package com.parlour.bookmyparlour.adapter.seller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.Model.SubServicemodel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.android.volley.toolbox.HttpHeaderParser.parseCacheHeaders;

/**
 * Created by Lakhwinder on 8/23/2017.
 */

public class ViewSubServices extends BaseActivity {
    RecyclerView view;
    private StringRequest jsonArrayRequest;
    ArrayList<SubServicemodel> servicemodelArrayList = new ArrayList<>();
    private SubserviceAdapter subserviceAdapter;
    private String serId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subservices);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sub-Services");
        serId = getIntent().getStringExtra("serviceid");
        view = (RecyclerView) findViewById(R.id.rec_sub);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewSubServices.this, LinearLayoutManager.VERTICAL, false);
        view.setLayoutManager(linearLayoutManager);
        subserviceAdapter = new SubserviceAdapter(ViewSubServices.this, servicemodelArrayList, new SubserviceAdapter.ondeleteListener() {
            @Override
            public void ondelete(final String sid, final String subid) {
                showOptionDialog("Do you really want to delete ?", new Helper.OnChoiceListener() {
                    @Override
                    public void onChoose(boolean isPositive) {
                        if (isPositive) {
                            deleteService(sid, subid);
                        } else {

                        }
                    }
                });
            }
        });
        view.setAdapter(subserviceAdapter);
        getServices();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public StringRequest deleteService(final String sid, String subid) {

        jsonArrayRequest = new StringRequest(WebServices.DeleteSubservice + "parlour_id=" + user.getSaloonId() + "&service_id=" + sid + "&sub_id=" + subid, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("service response", "---  " + response);
                Log.d("service response", "---  " + sid);
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        //Toast.makeText(AddService.this, "" + message, Toast.LENGTH_LONG).show();
                        getServices();
                    } else {

                        showInformationDialog(message);

                    }
                    getServices();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoading();

                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, parseCacheHeaders(response));
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        reqDetail.add(jsonArrayRequest);
        return jsonArrayRequest;
    }

    public StringRequest getServices() {

        showLoading();
        jsonArrayRequest = new StringRequest(WebServices.ViewSubServices + "parlour_id=" + user.getSaloonId() + "&service_id=" + serId, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideLoading();
                Log.d("service response", "---  " + response+"+++++++++++++"+serId+"--------"+user.getSaloonId());
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    servicemodelArrayList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            SubServicemodel adapterModel = new SubServicemodel();
                            adapterModel.setTitle(jsonArray.getJSONObject(i).getString("detail"));
                            adapterModel.setId(jsonArray.getJSONObject(i).getString("service_id"));
                            String ps_id = jsonArray.getJSONObject(i).getString("id");
                            adapterModel.setSubid(ps_id);
                            servicemodelArrayList.add(adapterModel);

                        }
                    } else {

                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });

                    }
                    subserviceAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoading();

                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, parseCacheHeaders(response));
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        reqDetail.add(jsonArrayRequest);
        return jsonArrayRequest;
    }

}

package com.parlour.bookmyparlour.adapter.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.SpecialOffersModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;
import com.parlour.bookmyparlour.utils.config.WebServices;

import java.util.ArrayList;

/**
 */
public class SpecialoffersAdapter extends GenericAdapter<SpecialOffersModel> {
onClick onClick;
    private final ArrayList<? extends SpecialOffersModel> itemList;


    public SpecialoffersAdapter(Context context, ArrayList<SpecialOffersModel> itemList,onClick onClick) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.onClick=onClick;

    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, desc, rates;
        ImageView image;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.offer_name);
            desc = (TextView) itemView.findViewById(R.id.offer_descri);
            rates = (TextView) itemView.findViewById(R.id.offer_price);
            image = (ImageView) itemView.findViewById(R.id.offer_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
onClick.onclick();
                }
            });
        }

        public void onBindHolder(final SpecialOffersModel hOmeUSerModel) {

            cname.setText(utils.capFirstChar(hOmeUSerModel.getTitle()));
            desc.setText(hOmeUSerModel.getDescription());
            rates.setText("Valid till: " + utils.getFormattedDate3(hOmeUSerModel.getDate()));

            String fullUrl = WebServices.DomainNameImage + hOmeUSerModel.getImage();
            Log.d("url image", "" + fullUrl);
            utils.setImagePicass(fullUrl, context, image);
        }
    }

    public interface onClick {
        public void onclick();
    }
}



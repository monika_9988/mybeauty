package com.parlour.bookmyparlour.adapter.seller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.SubServicemodel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class SubserviceAdapter extends GenericAdapter<SubServicemodel> {

    private final ArrayList<? extends SubServicemodel> itemList;
    ondeleteListener ondeletelistener;

    String title;
    public SubserviceAdapter(Context context, ArrayList<SubServicemodel> itemList, ondeleteListener ondeletelistener) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.ondeletelistener = ondeletelistener;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname;
        ImageView  delete;
        private ViewHolder(final View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.sername);
            delete = (ImageView) itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final SubServicemodel catlistingmodel) {

            //imageLoader.displayImage(fullUrl,featureimage);
            cname.setText(utils.capFirstChar(catlistingmodel.getTitle()));

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ondeletelistener.ondelete(catlistingmodel.getId(),catlistingmodel.getSubid());


                }
            });
        }
    }

       public interface ondeleteListener {
        void ondelete(String sid, String subid);
    }
}

package com.parlour.bookmyparlour.adapter.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.MyBookingModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class UserBookingsAdapter extends GenericAdapter<MyBookingModel> {

    private final ArrayList<? extends MyBookingModel> itemList;

    delete delete;
    cconfirm cconfirm;

    public UserBookingsAdapter(Context context, ArrayList<MyBookingModel> itemList, delete delete, cconfirm cconfirm) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.delete = delete;
        this.cconfirm = cconfirm;

    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, desc, time, suggestedtime, status, confirm, cancel, phone;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.sta_servicename);
            desc = (TextView) itemView.findViewById(R.id.sta_message);
            time = (TextView) itemView.findViewById(R.id.sta_bookingdate);
            suggestedtime = (TextView) itemView.findViewById(R.id.sta_newtimesuggested);
            status = (TextView) itemView.findViewById(R.id.sta_merchandresponse);
            confirm = (TextView) itemView.findViewById(R.id.sta_confirm_booking);
            phone = (TextView) itemView.findViewById(R.id.sta_number);
            cancel = (TextView) itemView.findViewById(R.id.sta_cancel_booking);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_fornewtimesuggested);

        }

        public void onBindHolder(final MyBookingModel myBookingModel) {

            String dayName = getdayName(utils.getFormattedDate(myBookingModel.getDate()));
//0 pending
            //1 ok
            //2 new time
            //3 cancel
            // 4 confirmed
            cname.setText(utils.capFirstChar(myBookingModel.getName()) + " - " + utils.capFirstChar(myBookingModel.getServicename()));
            switch (myBookingModel.getStatus()) {
                case "0":
                    linearLayout.setVisibility(View.GONE);

                    status.setText(utils.capFirstChar("Booking Status: " + "Pending"));
                    cancel.setVisibility(View.VISIBLE);
                    break;
                case "1":
                    linearLayout.setVisibility(View.GONE);

                    status.setText(utils.capFirstChar("Booking Status: " + "Confirmed"));
                    cancel.setVisibility(View.VISIBLE);
                    break;
                case "2":
                    linearLayout.setVisibility(View.VISIBLE);
                    suggestedtime.setText("Suggested DateTime :" + utils.getFormattedDate2(myBookingModel.getSugdate()) + " " + dayName + " , " + myBookingModel.getSugtime());
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cconfirm.onCOnfirm(myBookingModel.getId());
                        }
                    });
                    status.setText(utils.capFirstChar("Booking Status: " + "New Date-Time Suggested"));
                    cancel.setVisibility(View.VISIBLE);
                    break;
                case "3":
                    linearLayout.setVisibility(View.GONE);

                    status.setText(utils.capFirstChar("Booking Status: " + "Cancelled"));
                    cancel.setVisibility(View.GONE);
                    break;
                case "4":
                    linearLayout.setVisibility(View.GONE);

                    status.setText(utils.capFirstChar("Booking Status: " + "Confirmed"));
                    cancel.setVisibility(View.VISIBLE);
                    break;
                default:
            }

            try {
                time.setText("Booking on: " + utils.getFormattedDate2(myBookingModel.getDate()) + " " + dayName + " , " + myBookingModel.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
            phone.setText("Contact Number: " + myBookingModel.getPhone());
            desc.setText(utils.capFirstChar(myBookingModel.getDerc()));
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delete.onDelete(myBookingModel.getId());
                }
            });
        }
    }

    private String getdayName(String formattedDate) {
        String day = utils.getDayNamefromdate(formattedDate);
        return day;
    }

    public interface delete {
        public void onDelete(String id);
    }

    public interface cconfirm {
        public void onCOnfirm(String id);
    }

}



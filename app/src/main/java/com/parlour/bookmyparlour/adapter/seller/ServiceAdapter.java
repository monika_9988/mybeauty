package com.parlour.bookmyparlour.adapter.seller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.Servicemodel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;
import com.parlour.bookmyparlour.utils.config.WebServices;

import java.util.ArrayList;

/**
 */
public class ServiceAdapter extends GenericAdapter<Servicemodel> {

    private final ArrayList<? extends Servicemodel> itemList;
    ondeleteListener ondeletelistener;
    oneditListener oneditlistener;
    onAddSubServices onAddSubServices;

    String title;
    public ServiceAdapter(Context context, ArrayList<Servicemodel> itemList, ondeleteListener ondeletelistener, oneditListener oneditlistener, onAddSubServices onAddSubServices) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.ondeletelistener = ondeletelistener;
        this.oneditlistener = oneditlistener;
        this.onAddSubServices = onAddSubServices;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            try {
                ((ViewHolder) holder).onBindHolder(itemList.get(position));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname,description,sprice,o_text;
        ImageView edit, delete,featureimage;
        TextView _addSubService;
        LinearLayout ll_services;
        private ViewHolder(final View itemView) {
            super(itemView);

            featureimage = (ImageView) itemView.findViewById(R.id.thumbnail);
            cname = (TextView) itemView.findViewById(R.id.rowser_name);
            description = (TextView) itemView.findViewById(R.id.description);
            sprice = (TextView) itemView.findViewById(R.id.start_price);
            o_text = (TextView) itemView.findViewById(R.id.offer_text);

            ll_services=(LinearLayout) itemView.findViewById(R.id.ll_services);

            edit = (ImageView) itemView.findViewById(R.id.rowser_edit);
            delete = (ImageView) itemView.findViewById(R.id.rowser_delete);
            _addSubService = (TextView)itemView. findViewById(R.id.add_subservice);
            ll_services.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(context,ViewSubServices.class);
                    intent.putExtra("serviceid",itemList.get(getLayoutPosition()).getSerid());
                    context.startActivity(intent);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final Servicemodel catlistingmodel) throws Exception {
            String fullUrl = WebServices.DomainNameImage + catlistingmodel.getImage();
       utils.setImagePicass(fullUrl,context,featureimage);
            cname.setText(utils.capFirstChar(catlistingmodel.getTitle()));
            description.setText(catlistingmodel.getName());
            sprice.setText("From: "+utils.getCurrencySymbol()+catlistingmodel.getSprice()+" "+" To: "+utils.getCurrencySymbol()+catlistingmodel.getEprice());

            if(catlistingmodel.getOfferText().length()==0) {
                o_text.setVisibility(View.INVISIBLE);
            }
            else {
                o_text.setVisibility(View.VISIBLE);
                o_text.setText(utils.capFirstChar(catlistingmodel.getOfferText()));
            }
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    oneditlistener.onedit(catlistingmodel.getTitle(),catlistingmodel.getName(),catlistingmodel.getSprice(),catlistingmodel.getEprice(),catlistingmodel.getImage(),catlistingmodel.getId(),catlistingmodel.getOfferText());
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ondeletelistener.ondelete(catlistingmodel.getId());

                }
            });
            _addSubService.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAddSubServices.onAdd(catlistingmodel.getSerid(),itemList.get(getLayoutPosition()).getTitle());

                    //addSubServiceDialog(itemList.get(getLayoutPosition()).getId(),itemList.get(getLayoutPosition()).getTitle());
                }
            });
        }
    }

       public interface ondeleteListener {
        void ondelete(String sid);
    }

    public interface oneditListener {
        void onedit(String title,String des,String sprice,String eprice,String image, String id,String offer_text);
    }
    public interface onAddSubServices {
        void onAdd(String serviceid,String serviceTitlle);
    }
}

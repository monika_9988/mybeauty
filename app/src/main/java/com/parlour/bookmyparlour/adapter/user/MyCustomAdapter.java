package com.parlour.bookmyparlour.adapter.user;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.AdminServiceModel;
import com.parlour.bookmyparlour.R;

import java.util.ArrayList;

public class MyCustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<AdminServiceModel> objects;

    public MyCustomAdapter(Context context, ArrayList<AdminServiceModel> objects) {
        super();
        this.c = context;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public AdminServiceModel getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        AdminServiceModel cur_obj = objects.get(position);
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.all_services, parent, false);
        TextView label = (TextView) row.findViewById(R.id.text_list_view);
        label.setText(cur_obj.getName());

        return row;
    }
}
package com.parlour.bookmyparlour.adapter.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.GenericUSerModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.classes.user.ParlourDetail;
import com.parlour.bookmyparlour.generic.GenericAdapter;
import com.parlour.bookmyparlour.utils.TextUtils;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 */
public class HomeParloursAdapter extends GenericAdapter<GenericUSerModel> {

    private final ArrayList<? extends GenericUSerModel> itemList;
setopenMApLocation setopenMApLocation;

    public HomeParloursAdapter(Context context, ArrayList<GenericUSerModel> itemList,setopenMApLocation setopenMApLocation) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.setopenMApLocation=setopenMApLocation;

    }
    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, desc, distance;
        ImageView image;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.title_home);
            desc = (TextView) itemView.findViewById(R.id.detail_home);
            distance = (TextView) itemView.findViewById(R.id.distance_home);
            image = (ImageView) itemView.findViewById(R.id.image_home);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_rowitem);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Constants.Validator.isNetworkConnected(context)){
                        user.setParlourId(itemList.get(getLayoutPosition()).getId());
                        Log.d("parlour idddddddd", "" + itemList.get(getLayoutPosition()).getId());
                        Intent intent = new Intent(context, ParlourDetail.class);
                        context.startActivity(intent);
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getString(R.string.checkinternet));
                        builder.setCancelable(false);
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        // Create the AlertDialog object and return it
                        builder.create().show();

                    }

                }
            });

        }

        public void onBindHolder(final GenericUSerModel hOmeUSerModel) {

            cname.setText(utils.capFirstChar(hOmeUSerModel.getTitle()));
            desc.setText(utils.capFirstChar(hOmeUSerModel.getDescription()));
            try {
                Double aDouble = utils.getdistFrom(TextUtils.getDouble(hOmeUSerModel.getLat()), TextUtils.getDouble(hOmeUSerModel.getLng()), user.getUserLocation().getLatitude(), user.getUserLocation().getLongitude(), "km");

                distance.setText(String.valueOf(aDouble) + " kms away");
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            distance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setopenMApLocation.onMapLocationClick(hOmeUSerModel);
                }
            });
            String fullUrl = WebServices.DomainNameImage + hOmeUSerModel.getImage();
            Log.d("url image", "" + fullUrl);
            Picasso.with(context)
                    .load(fullUrl)
                    .placeholder(R.drawable.noimage)
                    .error(R.drawable.noimage).into(image);
        }
    }


    public int dpToPx(int dp, Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
public interface setopenMApLocation{
        public void onMapLocationClick(GenericUSerModel hOmeUSerModel);
    }
}



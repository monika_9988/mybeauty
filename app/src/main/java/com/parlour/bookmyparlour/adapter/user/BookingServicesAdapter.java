package com.parlour.bookmyparlour.adapter.user;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.ServicesModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;
import com.parlour.bookmyparlour.utils.config.WebServices;

import java.util.ArrayList;

/**
 */
public class BookingServicesAdapter extends GenericAdapter<ServicesModel> {

    private final ArrayList<? extends ServicesModel> itemList;
    bookNowClickListener bookNowClickListener;

    public BookingServicesAdapter(Context context, ArrayList<ServicesModel> itemList, bookNowClickListener bookNowClickListener) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.bookNowClickListener = bookNowClickListener;

    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            try {
                ((ViewHolder) holder).onBindHolder(itemList.get(position));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, desc, rates, book;
        ImageView image;
        LinearLayout linearLayout;

        private ViewHolder(final View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.sername);
            desc = (TextView) itemView.findViewById(R.id.serdetails);
            rates = (TextView) itemView.findViewById(R.id.serrates);
            book = (TextView) itemView.findViewById(R.id.booking);
            image = (ImageView) itemView.findViewById(R.id.serimage);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_rowitem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                    openViewDialogAnimation(itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final ServicesModel hOmeUSerModel) throws Exception {

            cname.setText(utils.capFirstChar(hOmeUSerModel.getTitle()));
            desc.setText(hOmeUSerModel.getDescription());
            rates.setText("Our Rates: " + utils.getCurrencySymbol() + hOmeUSerModel.getStartprice() + " - " + utils.getCurrencySymbol() + hOmeUSerModel.getEndprice());
            book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    openViewDialogAnimation(itemList.get(getLayoutPosition()));

                }
            });
            String fullUrl = WebServices.DomainNameImage + hOmeUSerModel.getImage();
            Log.d("url image", "" + fullUrl);
            utils.setImagePicass(fullUrl, context, image);
            fullUrl = fullUrl.substring(1, fullUrl.length() - 1);
//            imageLoader.displayImage(fullUrl, image);
        }
    }

    private void openViewDialogAnimation(final ServicesModel itemList) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_viewdetails);
        Window window = dialog.getWindow();
//        lp.copyFrom(window.getAttributes());
        int csc = dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //style id

//WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);

        TextView name, title, derc;
        ImageView image;
        title = (TextView) dialog.findViewById(R.id.dia_sername);
        name = (TextView) dialog.findViewById(R.id.dia_sertitle);
        derc = (TextView) dialog.findViewById(R.id.dia_serderc);
        image = (ImageView) dialog.findViewById(R.id.dia_serimage);

        title.setText(itemList.getTitle());
        derc.setText(itemList.getDescription());
        try {
            name.setText("Our Rates: " + utils.getCurrencySymbol() + itemList.getStartprice() + " - " + utils.getCurrencySymbol() + itemList.getEndprice());
        } catch (Exception e) {
            e.printStackTrace();
        }

        utils.setImagePicass(WebServices.DomainNameImage + itemList.getImage(), context, image);

        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookNowClickListener.setonbookNowClickListener(itemList);
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();


    }

    public interface bookNowClickListener {
        public void setonbookNowClickListener(ServicesModel id);

    }
}



package com.parlour.bookmyparlour.adapter.seller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.AdminServiceModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class AdminServiceAdapter extends GenericAdapter<AdminServiceModel>  {

    private final ArrayList<? extends AdminServiceModel> itemList;
    

    public AdminServiceAdapter(Context context, ArrayList<AdminServiceModel> itemList) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;

    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.text_list_view);


        }

        public void onBindHolder(final AdminServiceModel adminServiceModel) {

            cname.setText(adminServiceModel.getName());

        }
    }
}

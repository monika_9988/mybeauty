package com.parlour.bookmyparlour.adapter.seller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.OfferModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.classes.Merchant.UploadOffer;
import com.parlour.bookmyparlour.generic.GenericAdapter;
import com.parlour.bookmyparlour.utils.config.WebServices;

import java.util.ArrayList;

/**
 */
public class OfferAdapter extends GenericAdapter<OfferModel> {

    private final ArrayList<? extends OfferModel> offerList;
    ondeleteListener ondeletelistener;
    private String type;

    public OfferAdapter(UploadOffer context, ArrayList<OfferModel> offerList, ondeleteListener ondeletelistener) {
        super(context, offerList);
        this.offerList = offerList;
        this.context = context;
        this.ondeletelistener = ondeletelistener;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            try {
                ((ViewHolder) holder).onBindHolder(offerList.get(position));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, description, discount;
        ImageView delete, featureImage;

        private ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.offer_title);
            description = (TextView) itemView.findViewById(R.id.description);
            discount = (TextView) itemView.findViewById(R.id.discount);

            delete = (ImageView) itemView.findViewById(R.id.offer_delete);
            featureImage = (ImageView) itemView.findViewById(R.id.thumbnail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), offerList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final OfferModel offerModel) throws Exception {

            String fullUrl = WebServices.DomainNameImage + offerModel.getImage();
            Log.d("dxhwvdiuwd", "" + fullUrl);
        utils.setImagePicass(fullUrl,context,featureImage);
            title.setText(utils.capFirstChar(offerModel.getTitle()));
            description.setText(offerModel.getDesc());

            // same like that getting value from ePercent and parsing to double

            if (offerModel.getDiscountType().equals("2")) {
                type = "Flat";
                discount.setText("Discount: " +utils.getCurrencySymbol()+""+offerModel.getDiscount()+" /-");

            } else if (offerModel.getDiscountType().equals("1")) {
                type = "Percentage";
                discount.setText("Discount: " +offerModel.getDiscount() +"%");

            } else if (offerModel.getDiscountType().equals("0")) {
                type = "No Discount";
                discount.setText("No Discount");

            }
            //discount.setText("Discount: " + offerModel.getDiscount() + "  " + "Discount type: " + type);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ondeletelistener.ondelete(offerModel.getId());

                }
            });

        }
    }


    public interface ondeleteListener {
        void ondelete(String sid);
    }

}

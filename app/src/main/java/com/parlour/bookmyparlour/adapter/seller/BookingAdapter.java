package com.parlour.bookmyparlour.adapter.seller;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parlour.bookmyparlour.Models.Model.BookingModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class BookingAdapter extends GenericAdapter<BookingModel> {

    private final ArrayList<? extends BookingModel> itemList;
    onAcceptListener onAcceptListener;
    onDeclineListener onDeclineListener;
    onnewTimeListener onnewTimeListener;

    public BookingAdapter(Context context, ArrayList<? extends BookingModel> itemList,onAcceptListener onAcceptListener, onDeclineListener onDeclineListener,onnewTimeListener onnewTimeListener) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.onAcceptListener=onAcceptListener;
        this.onDeclineListener=onDeclineListener;
        this.onnewTimeListener=onnewTimeListener;

    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, message, aceept, decline,datetime,newtime,status;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.boo_name);
            message = (TextView) itemView.findViewById(R.id.boo_messgae);
            aceept = (TextView) itemView.findViewById(R.id.accept_booking);
            decline = (TextView) itemView.findViewById(R.id.cancel_booking);
            datetime = (TextView) itemView.findViewById(R.id.boo_time);
            newtime = (TextView) itemView.findViewById(R.id.req_newtime);
            status = (TextView) itemView.findViewById(R.id.boo_status);
            linearLayout=(LinearLayout) itemView.findViewById(R.id.ll_accdecline);

        }

        public void onBindHolder(final BookingModel adminServiceModel) {

            cname.setText(utils.capFirstChar(adminServiceModel.getName()));
            message.setText(adminServiceModel.getMessage());
//            utils.getFormattedDate() -use if we need to change date format
            datetime.setText("DateTime: "+adminServiceModel.getDateTime());
            if (adminServiceModel.getStatus().equals("0")){
                status.setText("Status: "+"Request is Pending");
                status.setTextColor(Color.BLACK);
                linearLayout.setVisibility(View.VISIBLE);
                newtime.setVisibility(View.VISIBLE);
                aceept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAcceptListener.onAccept(adminServiceModel.getId());
                    }
                });
                decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onDeclineListener.onDecline(adminServiceModel.getId());
                    }
                });
                newtime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onnewTimeListener.onnewtime(adminServiceModel.getId());
                    }
                });
            }else{
                //0 pending
                //1 ok
                //2 new time
                //3 cancel
                // 4 confirmed
                linearLayout.setVisibility(View.GONE);
                newtime.setVisibility(View.GONE);
                if (adminServiceModel.getStatus().equals("4")){
                    status.setText("Status: "+"Confirmed");
                    status.setTextColor(ContextCompat.getColor(context,R.color.confirm));
                }else if (adminServiceModel.getStatus().equals("2")){
                    status.setText("Status: "+"New Date-Time Suggested");
                    status.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));

                }else if (adminServiceModel.getStatus().equals("3")){
                    status.setText("Status: "+"Booking Cancelled");
                    status.setTextColor(ContextCompat.getColor(context,R.color.cancel));

                }else if (adminServiceModel.getStatus().equals("1")){
                    status.setText("Status: "+"Booking Approved");
                    status.setTextColor(ContextCompat.getColor(context,R.color.confirm));

                }
            }


        }
    }
    public interface onAcceptListener{
        public void onAccept(String id);
    }

    public interface onnewTimeListener{
        public void onnewtime(String id);
    }

    public interface onDeclineListener{
        public void onDecline(String id);
    }

}



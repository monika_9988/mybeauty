//package com.mybeautyplus.mybeautyplus.generic;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkResponse;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.HttpHeaderParser;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.mybeautyplus.mybeautyplus.utils.config.Constants;
//
//import java.io.UnsupportedEncodingException;
//
//import static com.android.volley.toolbox.HttpHeaderParser.parseCacheHeaders;
//
///**
// * Created by Lakhwinder on 9/2/2017.
// */
//
//public class RequestClass extends BaseActivity {
//    private static RequestClass mInstance;
//    private static Context mContext;
//    private static  reqDetail;
//
//    public static RequestClass getInstance() {
//        if (mInstance == null) {
//            mInstance = new RequestClass();
//            reqDetail = Volley.newRequestQueue(mInstance);
//
//        } else {
//            return mInstance;
//        }
//        return mInstance;
//    }
//
//    public String makeRequest(final String url, final String parameters) {
//        final String[] responseData = {""};
//        showLoading();
//        StringRequest jsonArrayRequest = new StringRequest(url + parameters, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                hideLoading();
//                responseData[0] =response;
//                Log.d("response = "+url+parameters, "  ---  " + response);
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        hideLoading();
//
//                    }
//                }) {
//            @Override
//            protected Response parseNetworkResponse(NetworkResponse response) {
//                String parsed;
//                try {
//                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//                } catch (UnsupportedEncodingException e) {
//                    parsed = new String(response.data);
//                }
//                return Response.success(parsed, parseCacheHeaders(response));
//            }
//
//        };
//        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
//                Constants.MY_SOCKET_TIMEOUT_MS,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        reqDetail.add(jsonArrayRequest);
//        return responseData[0];
//    }
//
//}

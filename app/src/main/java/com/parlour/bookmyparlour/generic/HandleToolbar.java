//package com.mybeautyplus.mybeautyplus.generic;
//
//import android.app.Activity;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.mybeautyplus.mybeautyplus.R;
//import com.mybeautyplus.mybeautyplus.utils.User;
//
//
///**
// * Created by Gurvinder rajpal on 30-11-16.
// */
//
//public class HandleToolbar {
//    private Activity activity;
//    private TextView toolbarTitle;
//    ImageView tool_back, tool_setting;
//    User user;
////    ImageLoader imageLoader;
//    OnPopupClickListener onPopupClickListener;
//    private ImageView tool_imageIcon;
//    private ImageView mDownloadButton;
//    private ImageView mStartChat;
//    private OnDownloadClickListener downloadClickListener;
//    private OnStartChatClickListener startChatClickListener;
//
//    public HandleToolbar(Activity activity) {
//        this.activity = activity;
//        user = User.getInstance();
////        imageLoader = ImageLoader.getInstance();
//        initToolbar();
//    }
//
//    private void initToolbar() {
//        toolbarTitle = (TextView) activity.findViewById(R.id.tool_text);
//        tool_back = (ImageView) activity.findViewById(R.id.tool_back);
//        tool_setting = (ImageView) activity.findViewById(R.id.tool_settings);
//        mDownloadButton = (ImageView) activity.findViewById(R.id.download_image_btn);
//        mStartChat = (ImageView) activity.findViewById(R.id.image_start_chat);
//
//        tool_back.setVisibility(View.INVISIBLE);
//        tool_setting.setVisibility(View.INVISIBLE);
//        toolbarTitle.setVisibility(View.INVISIBLE);
//        mDownloadButton.setVisibility(View.GONE);
//
//        handleClicks();
//    }
//
//
//    private void handleClicks() {
//        tool_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                activity.finish();
//            }
//        });
//        mDownloadButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (downloadClickListener != null)
//                    downloadClickListener.onClick();
//            }
//        }); mStartChat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (startChatClickListener != null)
//                    startChatClickListener.onClick();
//            }
//        });
//    }
//
//    public void showCenterText() {
//        toolbarTitle.setVisibility(View.VISIBLE);
//    }
//
//    public void showAddIcon() {
//
////        txt_addplan.setVisibility(View.VISIBLE);
//    }
//
//    public void showBackIcon() {
//        tool_back.setVisibility(View.VISIBLE);
//    }
//    public void showChatIcon() {
//        mStartChat.setVisibility(View.VISIBLE);
//    }
//
//    public void showCentreIcon() {
//        tool_imageIcon.setVisibility(View.VISIBLE);
//    }
//
//    public void setText(String s) {
//        toolbarTitle.setText(s);
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
////            toolbarTitle.setText(Html.fromHtml(s, Html.FROM_HTML_OPTION_USE_CSS_COLORS));
////        } else {
////        }
//
//    }
//
//    public void showDownloadButton() {
//        mDownloadButton.setVisibility(View.VISIBLE);
//    }
//
//    public interface OnNotificationItemClickListener<T> {
//        void onClick(int position, T item);
//    }
//
//    public interface OnEditClickListener {
//        void onEdit();
//    }
//
//    public interface OnDownloadClickListener {
//        void onClick();
//    }public interface OnStartChatClickListener {
//        void onClick();
//    }
//
//    public interface OnPopupClickListener {
//        void onPopupClick(int itemId);
//    }
//
//    public void setOnDownloadClickListener(OnDownloadClickListener downloadClickListener) {
//        this.downloadClickListener = downloadClickListener;
//    }
// public void setOnStartChatClickListener(OnStartChatClickListener startChatClickListener) {
//
//     this.startChatClickListener = startChatClickListener;
// }
//
//    public void setOnPopupClickListener(OnPopupClickListener onPopupClickListener) {
//        this.onPopupClickListener = onPopupClickListener;
//    }
//}

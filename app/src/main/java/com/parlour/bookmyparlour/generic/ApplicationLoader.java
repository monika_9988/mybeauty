package com.parlour.bookmyparlour.generic;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.parlour.bookmyparlour.utils.User;
import com.parlour.bookmyparlour.utils.Utils;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Macrew-PC-24 on 21-11-16.
 */

public class ApplicationLoader extends Application {
    public static volatile ApplicationLoader applicationContext;
    private RequestQueue reqDetail;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        applicationContext = this;
        MultiDex.install(this);
        User.init(getApplicationContext());
        //OpenRequest.init(getApplicationContext());
        reqDetail = Volley.newRequestQueue(getApplicationContext());
        Utils.init(getApplicationContext());
        getImageLoaderInstance(this);
    }

    public ImageLoader getImageLoaderInstance(Context context) {
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.ic_menu_report_image
                ) // resource or drawable
                .showImageForEmptyUri(android.R.drawable.ic_menu_report_image) // resource or drawable
                .showImageOnFail(android.R.drawable.ic_menu_report_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .considerExifParams(true) // default
                .displayer(new FadeInBitmapDisplayer(0)).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
        ImageLoader.getInstance().init(config);
        return ImageLoader.getInstance();
    }
}

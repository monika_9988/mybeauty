package com.parlour.bookmyparlour.utils.config;


public class WebServices {


    private static final String DomainName = "http://www.magnaopussoftware.in/parlour/";
    public static final String DomainNameImage = "http://www.magnaopussoftware.in/parlour/images/";

// public static final String DomainName = "http://10.0.2.2/parlourApp/";
//    public static final String DomainNameImage = "http://10.0.2.2/parlourApp/images/";

   // admin webservices

    public static final String RequestList = DomainName + "getListingRequests.php?";
    public static final String ApproveReq = DomainName + "approverequest.php?";
    public static final String DeclineReq = DomainName + "declinerequest.php?";
    public static final String AddService = DomainName + "addservice.php?";
    public static final String GetServices = DomainName + "getAllServices.php?";
    public static final String UPdateservice = DomainName + "updateservice.php?";
    public static final String DeleteServices = DomainName + "deleteservice.php?";
    public static final String AllBookings = DomainName + "getAllBookings.php?";
    public static final String AllAprrovedList = DomainName + "getapprovedsaloonlist.php?";
    public static final String AllREgisteredUsers = DomainName + "getregisteredusers.php?";

    public static final String ImageURl = DomainNameImage;
    public static final String LoginAdmin = DomainName + "loginAdmin.php?";
    public static final String add_client = DomainName + "add_client";
    public static final String get_goals = DomainName + "get_goals";


    //----- merchant webservices
    public static final String Register = DomainName + "registerUserOrg.php?";
    public static final String Login = DomainName + "loginUserOrg.php?";
    public static final String ForgetPassword = DomainName + "forgetpass.php?";
    public static final String FreeListing = DomainName + "freeListingParlour.php";
    public static final String ChangePassword = DomainName + "changepass.php?";
    public static final String AddServiceMerchant = DomainName + "addServiceMerchant.php";
    public static final String GetServicesMerchant = DomainName + "getAllServicesMerchant.php?";
    public static final String UPdateserviceMerchant = DomainName + "updateServicesMerchant.php";
    public static final String DeleteServicesMerchant = DomainName + "deleteServiceMerchant.php?";
    public static final String GetAdminServices = DomainName + "getAdminServices.php";
    public static final String GetAllBooking = DomainName + "getAllBookingsParlour.php?";
    public static final String AcceptRequestBookind = DomainName + "confirmUserBooking.php?";
    public static final String DeclineBookingBooking = DomainName + "cancelBookingUser.php?";
    public static final String NewTime = DomainName + "suggestNewTime.php?";
    public static final String AddOffers = DomainName + "addOffers.php?";

    public static final String GetAllOffers = DomainName + "getParlourOffer.php?";
    public static final String DeletOffer = DomainName + "deleteOffer.php?";
    public static final String GetDEtailsMerc = DomainName + "getMerchantDetails.php?";
    public static final String AddSubServiceMerchant = DomainName + "addSubServiceMerchant.php?";
    public static final String ViewSubServices = DomainName + "getSubServicesMerchant.php?";
    public static final String DeleteSubservice = DomainName + "delSubServiceMerchant.php?";
    public static final String UpdateDetails = DomainName + "updateMerchantDetails.php?";
    public static final String UpadateMerchantImage = DomainName + "updateMerchantImage.php?";


    // webservices user--------
    public static final String GetAllParlours = DomainName + "getAllParlourListingUser.php";
    public static final String GetAllParloursSearch = DomainName + "filterParlourList.php?";
    public static final String GetAllServices = DomainName + "getParlourServices.php?";
    public static final String BookmyParlour = DomainName + "bookParlour.php?";
    public static final String GetUSerBookings = DomainName + "getMyBookings.php?";
    public static final String GetListingStatus = DomainName + "listingStatusUser.php?";
    public static final String DeleteBooking = DomainName + "cancelBookingUser.php?";
    public static final String ConfirmBooking = DomainName + "confirmBookingByUser.php?";
    public static final String GetMainServices = DomainName + "getAdminServices.php?";
    public static final String FilterSearch = DomainName + "filterParlourByPrice.php?";
    public static final String GetCity = DomainName + "getAllCities.php?";
    public static final String GetTEmpUserid = DomainName + "checkMobile.php?";
    public static final String DeviceRegister = DomainName + "deviceRegister.php?";
    public static final String SendNotification=DomainName+"sendAdminNotification.php";
}




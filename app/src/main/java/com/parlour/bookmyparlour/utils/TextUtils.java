package com.parlour.bookmyparlour.utils;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Gurvinder rajpal on 29-11-16.
 */

public class TextUtils {
    public static double getDouble(String value) {
        double output = 0;
        if (value != null && value.length() > 0) {
            try {
                output = Double.valueOf(value.replaceAll("[^\\d.]", ""));
            } catch (Exception e) {
                Log.e("ignore this exception!!", "" + e.toString());
            }
        }
        return output;
    }

    public static String format(double value) {
        String output = "0.0";
        try {
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
            DecimalFormat df = (DecimalFormat) nf;
            df.applyPattern("0.0");
            output = df.format(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public static long getLong(String value) {
        long output = 0;
        if (value != null && value.length() > 0) {
            try {
                output = Long.valueOf(value.replaceAll("[^\\d.]", ""));
            } catch (Exception e) {
                Log.e("ignore this exception!!", "" + e.toString());
            }
        }
        return output;
    }

    public static int getInteger(String value) {
        int output = 0;
        if (value != null && value.length() > 0) {
            try {
                output = Integer.valueOf(value.replaceAll("[^\\d]", ""));
            } catch (Exception e) {
                Log.e("ignore this exception!!", "" + e.toString());
            }
        }
        return output;
    }

    public static float getFloat(String value) {
        float output = 0;
        if (value != null && value.length() > 0) {
            try {
                output = Float.valueOf(value.replaceAll("[^\\d]", ""));
            } catch (Exception e) {
                Log.e("ignore this exception!!", "" + e.toString());
            }
        }
        return output;
    }
}

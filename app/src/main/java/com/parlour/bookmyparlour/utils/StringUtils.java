package com.parlour.bookmyparlour.utils;

/**
 * Created by Gurvinder Rajpal on 11/12/2016.
 * Email: 47gurvindr@gmail.com
 */
public class StringUtils {
    public static boolean isEmpty(String fontAsset) {
        return fontAsset.length() <= 0;
    }

    public static String formatPoints(int integer) {
        return String.valueOf(integer) + " pts";
    }
}

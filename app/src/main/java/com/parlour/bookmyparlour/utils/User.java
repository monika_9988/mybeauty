package com.parlour.bookmyparlour.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class User {

    private static final String SharedPref = "userxx";
    private static final String Host = "host";
    private static final String Username = "username";
    private static final String Port = "port";
    private static final String Password = "password";
    private static final String SavedFTPServers = "SavedFTPServers";
    private static final String USER_LATITUDE = "_USER_LATITUDE";
    private static final String USER_LONGITUDE = "_USER_LONGITUDE";
    private static final String USER_LEVEL = "_USER_LEVEL";
    private static final String USER_CURRENT_WEEK = "_USER_CURRENT_WEEK";
    private static final String USER_CURRENT_DAY = "_USER_CURRENT_DAY";
    private static final String USER_PROFILE_IMAGE = "_USER_PROFILE_IMAGE";
    private static final String USER_EMAIL_ID = "_USER_EMAIL_ID";
    private static final String USER_Phone = "phone";
    private static final String USER_PASSWORD = "_USER_PASSWORD";
    private static final String SaloonId = "_GOAL_ID";
    private static final String SERVICEID = "_USER_PLAN_ID";
    private static final String ParlourId = "parlourid";
    private static final String BookingId = "bookingid";
    private static User mInstance;
    private static Context context;
    private static SharedPreferences pref;
    private static final String Goals_Data = "_Goals_Data";
    private final String USER_ID = "_userid";
    private final String GCM_ID = "_gcmid";
    private static final String USER_NAME = "_username";
    private static final String USER_NUMBER = "_usernumber";
    private static final String USER_TYPE = "_type_user";
    private final String IS_USER_LOGGED = "_IS_USER_LOGGED";
    Utils utils;

    public User() {
        utils = Utils.getInstance();

        pref = context.getSharedPreferences(SharedPref, Context.MODE_MULTI_PROCESS);
    }

    public static User getInstance() {

        if (mInstance == null) {
            mInstance = new User();

        } else {
            return mInstance;
        }
        return mInstance;
    }

    public static void init(Context context) {
        User.context = context;
    }

    public static String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
    /**
     * Clear all the preferences store in this {@link android.content.SharedPreferences.Editor}
     */
    public boolean clear() {
        try {
            SharedPreferences.Editor mEditor =  pref.edit();;

            mEditor.clear().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Removes preference entry for the given key.
     *
     * @param key
     */
    public void removeValue(String key) {
        SharedPreferences.Editor mEditor =  pref.edit();;
        if (mEditor != null) {
            mEditor.remove(key).commit();
        }
    }
    /**
     * To get data of String type from persistence storage.
     *
     * @param key key for data.
     * @param def Value to return if key is not match to anyone.
     * @return String data.
     */
    public String getString(String key, String def) {
        return pref.getString(key, def);
    }

    public String urlIncoder(String key) {
        try {
            key = URLEncoder.encode(key, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return key;
    }

    public String getString(String key) {
        return getString(key, "");
    }

    public static long getLong(String key) {
        return pref.getLong(key, 0);
    }

    public int getInteger(String key) {
        return getInteger(key, 0);
    }

    public int getInteger(String key, int def) {
        return pref.getInt(key, def);
    }

    /**
     * To get data of boolean type from persistence storage.
     *
     * @param key key for data.
     * @param def Value to return if key is not match to anyone.
     * @return boolean value.
     */
    public boolean getBoolean(String key, boolean def) {
        return pref.getBoolean(key, def);
    }

    /**
     * Set a String value in the preferences editor, to be written back once
     * {@link @commit} or {@link @apply} are called.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference. Supplying {@code null} as
     *              the value is equivalent to calling {@link @remove(String)}
     *              with this key.
     */
    public void setString(String key, String value) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(key, value);
        e.apply();
    }

    public void setInteger(String key, int value) {
        SharedPreferences.Editor e = pref.edit();
        e.putInt(key, value);
        e.apply();
    }

    public void setLong(String key, long value) {
        SharedPreferences.Editor e = pref.edit();
        e.putLong(key, value);
        e.apply();
    }

    /**
     * Set a boolean value in the preferences editor, to be written back once
     * {@link @commit} or {@link @apply} are called.
     *
     * @param key   The name of the preference to modify.
     * @param value The new value for the preference.
     */
    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor e = pref.edit();
        e.putBoolean(key, value);
        e.apply();
    }

    public void setUserId(String value) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_ID, value);
        e.apply();
    }

    public void setGcmId(String value) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(GCM_ID, value);
        e.apply();
    }

    public String getGcmId() {
        return pref.getString(GCM_ID, "");
    }

    public String getUserId() {
        return pref.getString(USER_ID, "");
    }

    public String getName() {
        return pref.getString(USER_NAME, "");
    }

    public void setName(String name) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_NAME, name);
        e.apply();
    }

    public String getNumber() {
        return pref.getString(USER_NUMBER, "");
    }

    public void setNumber(String number) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_NUMBER, number);
        e.apply();
    }

    public void setProfileImage(String profilePic) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_PROFILE_IMAGE, profilePic);
        e.apply();

    }

    public void showLog(Context context, String message) {
        Log.d(context + "----- ", "" + message);
    }

    public String getProfileImage() {
        return pref.getString(USER_PROFILE_IMAGE, "");
    }

    public void setUserType(String type) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_TYPE, type);
        e.apply();
    }

    /**
     *
     * @param myString
     * @return
     */
    public String capsFirstLetter(String myString) {
        String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1);
        return upperString;
    }

    public String getUserType() {
        return pref.getString(USER_TYPE, null);
    }

    public void setEmail(String email) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_EMAIL_ID, email);
        e.apply();
    }

    public String getPhone() {
        return pref.getString(USER_Phone, "");
    }

    public void setPhone(String phone) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_Phone, phone);
        e.apply();
    }

    public String getEmail() {
        return pref.getString(USER_EMAIL_ID, "");
    }

    public void setPassword(String password) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(USER_PASSWORD, password);
        e.apply();
    }

    public String getPassword() {
        return pref.getString(USER_PASSWORD, "");
    }


    public String getServiceId() {
        return pref.getString(SERVICEID, "");
    }

    public void setServiceId(String serviceId) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(SERVICEID, serviceId);
        e.apply();
    }

    public String getParlourId() {
        return pref.getString(ParlourId, "");
    }

    public void setParlourId(String Parlour) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(ParlourId, Parlour);
        e.apply();
    }

    /**
     *
     * @return
     */
    public String getBookingId() {
        return pref.getString(BookingId, "");
    }

    public void setBookingId(String Booking) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(BookingId, Booking);
        e.apply();
    }

    public String getSaloonId() {
        return pref.getString(SaloonId, "");
    }

    public void setSaloonId(String Saloon) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(SaloonId, Saloon);
        e.apply();
    }


    public int getCurrentDay() {
        return pref.getInt(USER_CURRENT_DAY, 1);
    }

    public void setCurrentDay(int day) {
        SharedPreferences.Editor e = pref.edit();
        e.putInt(USER_CURRENT_DAY, day);
        e.apply();
    }


    public void setCurrentWeek(int week) {
        SharedPreferences.Editor e = pref.edit();
        e.putInt(USER_CURRENT_WEEK, week);
        e.apply();
    }

    public int getCurrentWeek() {
        return pref.getInt(USER_CURRENT_WEEK, 1);
    }


    public String getAccessToken() {
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return md5(android_id).toUpperCase();

    }

    public boolean checkResponse(String response) {
        boolean status = false;
        try {
            JSONObject object = new JSONObject(response);

            if (object.getString("status").equals("valid")) {
                status = true;
            } else {
                status = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public boolean isLoggedIn() {
        return getBoolean(IS_USER_LOGGED, false);
    }

    public void loggedIn(boolean b) {
        setBoolean(IS_USER_LOGGED, b);
    }

    public void isShown(boolean b) {
        setBoolean("show", b);
    }
    public boolean ifShowed() {
        return getBoolean("show", false);
    }

    public void logout() {
        setBoolean(IS_USER_LOGGED, false);
        setUserId("");
        setName("");
        setProfileImage("");
        setEmail("");
        setPhone("");


    }

//    public String getTempProfileImageLocation() {
//        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/Android/data/" + mContext.getPackageName() + "/temp/");
//        if (!filelocation.exists())
//            filelocation.mkdirs();
//        return filelocation + "/profile.png";
//    }

    /**
     * @param locationManager
     * @return current gps state
     */
    public boolean isGPSEnable(LocationManager locationManager) {
        boolean isLocation = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isLocation;
    }

    /**
     *
     * @param key
     * @param value
     */
    public void setDouble(String key, double value) {
        SharedPreferences.Editor e = pref.edit();
        e.putString(key, String.valueOf(value));
        e.commit();
    }

    public double getDouble(String key, double defvalue) {
        return Double.valueOf(pref.getString(key, String.valueOf(defvalue)));
    }

    /**
     *
     * @param host
     * @param username
     * @param password
     * @param port
     */
    public void saveFtpServer(String host, String username, String password, int port) {
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(User.Host, host);
            jsonObject.put(User.Username, username);
            jsonObject.put(User.Password, password);
            jsonObject.put(User.Port, port);
            jsonArray.put(jsonObject);


            SharedPreferences.Editor e = pref.edit();
            e.putString(User.SavedFTPServers, jsonArray.toString());
            e.apply();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    public void saveGoalsList(JSONArray jsonArray) {

        SharedPreferences.Editor e = pref.edit();
        e.putString(Goals_Data, jsonArray.toString());
        e.apply();
    }


    public void getAddressFromLocation(final double latitude, final double longitude, Helper.OnRequestCompleteListener<String> onRequestCompleteListener) {
        new GetUserLocationAsync(latitude, longitude, onRequestCompleteListener).execute();
    }


    private class GetUserLocationAsync extends AsyncTask<Void, Void, Void> {
        private final double latitude;
        private final double longitude;
        private final Helper.OnRequestCompleteListener<String> onRequestCompleteListener;
        private String result;

        private GetUserLocationAsync(double latitude, double longitude, Helper.OnRequestCompleteListener<String> onRequestCompleteListener) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.onRequestCompleteListener = onRequestCompleteListener;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                if (addressList != null && addressList.size() > 0) {

                    Address address = addressList.get(0);

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                        sb.append(address.getAddressLine(i)).append(" ");
                    }


                    Log.e("_address_", "data: " + address.toString());

                 /*   sb.append(address.getLocality()).append("\n");
                    sb.append(address.getLocality()).append("\n");
                    sb.append(address.getLocality()).append("\n");
                    sb.append(address.getLocality()).append("\n");
                    sb.append(address.getLocality()).append("\n");
                    sb.append(address.getLocality()).append("\n");
                    sb.append(address.getCountryName());*/
                    result = sb.toString();
                } else {
                    result = latitude + ", " + longitude;
                }
            } catch (Exception e) {
                Log.e("", "Unable connect to Geocoder", e);
                result = latitude + ", " + longitude;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            onRequestCompleteListener.onComplete(result);
        }
    }


    public class CacheOutPut<T> {
        private ArrayList<T> data;
        private long lastUpdate;


        private CacheOutPut(ArrayList<T> data, long lastUpdate) {

            this.data = data;
            this.lastUpdate = lastUpdate;
        }

        private void setLastUpdate(long lastUpdate) {
            this.lastUpdate = lastUpdate;
        }

        private void setData(ArrayList<T> data) {
            this.data = data;
        }

        public long getLastUpdate() {
            return lastUpdate;
        }

        public ArrayList<T> getData() {
            return data;
        }
    }

    public void saveUserLocation(Location location) {
        setDouble(USER_LATITUDE, location.getLatitude());
        setDouble(USER_LONGITUDE, location.getLongitude());
    }

    public UserCurrentLocation getUserLocation() {
        double lat = getDouble(USER_LATITUDE, 0.0);
        double lng = getDouble(USER_LONGITUDE, 0.0);
        return new UserCurrentLocation(lat, lng);
    }

    public class UserCurrentLocation {
        private double latitude;
        private double longitude;

        public UserCurrentLocation(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

    }

}

package com.parlour.bookmyparlour.Models.Model;


import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public  class OfferModel implements ViewModel {

    private String id;
    private String name;
    private String sprice;
    private String discount,title,discountType;
    String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return name;
    }

    public void setDesc(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getSprice() {
        return sprice;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
    public String getDiscount() {
        return discount;
    }
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }
    public String getDiscountType() {
        return discountType;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    @Override
    public int layoutId() {
        return R.layout.offer_items;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

}
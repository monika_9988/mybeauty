package com.parlour.bookmyparlour.Models.Model;


import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public  class SubServicemodel implements ViewModel {

    private String id;
    private String title;
     String subid;

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int layoutId() {
        return R.layout.rowitem_subservices;
    }

    @Override
    public String emptyTitle(){
        return null;
    }

}
package com.parlour.bookmyparlour.Models;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public class HOmeModel implements ViewModel {

    private String id;
    private String name,derc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.rowitem_parlour;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

    public String getDerc() {
        return derc;
    }

    public void setDerc(String derc) {
        this.derc = derc;
    }

}
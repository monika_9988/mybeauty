package com.parlour.bookmyparlour.Models.Model;


import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public  class BookingModel implements ViewModel {

    private String id;
    private String name,message,dateTime,status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.rowitem_allbookings;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

}
package com.parlour.bookmyparlour.Models.Model;


import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public  class MerchantDetailsModel implements ViewModel {

    private String id;
    private String name;
    private String title;
    private String sprice;
    private String eprice;
    String image,email,address,address2,landline,details,phone,city,start_time,end_time,work_days;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetails() {
        return details;
    }
public void  setAdd1(String address)
    {this.address=address;}

    public String getAdd1() {
        return address;
    }
    public void  setAdd2(String address2)
    {this.address2=address2;}

    public String getAdd2() {
        return address2;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getCity()
    {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getLandline()
    {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }
    public String getStart_time() {
        return start_time;
    }
    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }
    public String getEnd_time() {
        return end_time;
    }
    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
    public String getWork_days() {
        return work_days;
    }
    public void setWork_days(String work_days) {
        this.work_days = work_days;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    @Override
    public int layoutId() {
        return R.layout.merchant_items;
    }

    @Override
    public String emptyTitle(){
        return null;
    }

}
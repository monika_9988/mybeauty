package com.parlour.bookmyparlour.Models.Model;


import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public class AdminServiceModel implements ViewModel {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.all_services;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

}
package com.parlour.bookmyparlour.Models.Model;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

/**
 * Created by Lakhwinder on 8/22/2017.
 */

public class SpecialOffersModel implements ViewModel {

    String id,title,description,image,startprice,endprice,date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEndprice() {
        return endprice;
    }

    public void setEndprice(String endprice) {
        this.endprice = endprice;
    }

    public String getStartprice() {
        return startprice;
    }

    public void setStartprice(String startprice) {
        this.startprice = startprice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    @Override
    public int layoutId() {
        return R.layout.rowitem_offers;
    }

    @Override
    public String emptyTitle() {
        return null;
    }
}

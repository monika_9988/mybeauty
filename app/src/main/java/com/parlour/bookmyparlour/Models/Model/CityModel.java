package com.parlour.bookmyparlour.Models.Model;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

/**
 * Created by Lakhwinder on 8/22/2017.
 */

public class CityModel implements ViewModel {

    String id,title;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int layoutId() {
        return R.layout.rowitem_spinnercity;
    }

    @Override
    public String emptyTitle() {
        return null;
    }
}

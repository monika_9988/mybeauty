package com.parlour.bookmyparlour.Models;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public class MyBookingModel implements ViewModel {

    private String id;
    private String name,derc,status,servicename,date,time,dayname,sugdate,sugtime,phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSugdate() {
        return sugdate;
    }

    public void setSugdate(String sugdate) {
        this.sugdate = sugdate;
    }

    public String getSugtime() {
        return sugtime;
    }

    public void setSugtime(String sugtime) {
        this.sugtime = sugtime;
    }

    public String getDayname() {
        return dayname;
    }

    public void setDayname(String dayname) {
        this.dayname = dayname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.rowitem_bookingstatus;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

    public String getDerc() {
        return derc;
    }

    public void setDerc(String derc) {
        this.derc = derc;
    }

}
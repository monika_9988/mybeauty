package com.parlour.bookmyparlour.Models.Model;


import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.ViewModel;

public  class Servicemodel implements ViewModel {

    private String id;
    private String name;
    private String title;
    private String sprice;
    private String eprice,serid;
     String image,subid,offerText;

    public String getSerid() {
        return serid;
    }

    public void setSerid(String serid) {
        this.serid = serid;
    }

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSprice() {
        return sprice;
    }

    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public String getEprice() {
        return eprice;
    }
    public void setOfferText(String offerText){this.offerText = offerText;}
    public String getOfferText(){return offerText;};

    public void setEprice(String eprice) {
        this.eprice = eprice;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    @Override
    public int layoutId() {
        return R.layout.services_items;
    }

    @Override
    public String emptyTitle(){
        return null;
    }

}
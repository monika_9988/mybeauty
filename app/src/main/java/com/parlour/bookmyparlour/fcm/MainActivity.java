///**
// * Copyright 2016 Google Inc. All Rights Reserved.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// * http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.parlour.bookmyparlour.fcm;
//
//import android.app.NotificationManager;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.google.firebase.messaging.FirebaseMessaging;
//import com.parlour.bookmyparlour.generic.BaseActivity;
//
//public class MainActivity extends BaseActivity {
//
//    private static final String TAG = "MainActivity";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//            // Create channel to show notifications.
////            String channelId  = getString(R.string.default_notification_channel_id);
////            String channelName = getString(R.string.default_notification_channel_name);
//            NotificationManager notificationManager =
//                    getSystemService(NotificationManager.class);
////            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
////                    channelName, NotificationManager.IMPORTANCE_LOW));
//
//        // If a notification message is tapped, any data accompanying the notification
//        // message is available in the intent extras. In this sample the launcher
//        // intent is fired when the notification is tapped, so any accompanying data would
//        // be handled here. If you want a different intent fired, set the click_action
//        // field of the notification message to the desired intent. The launcher intent
//        // is used when no click_action is specified.
//        //
//        // Handle possible data accompanying notification message.
//        // [START handle_data_extras]
//        if (getIntent().getExtras() != null) {
//            for (String key : getIntent().getExtras().keySet()) {
//                Object value = getIntent().getExtras().get(key);
//                Log.d(TAG, "Key: " + key + " Value: " + value);
//            }
//        }
//        // [END handle_data_extras]
//                FirebaseMessaging.getInstance().subscribeToTopic("book");
//                // [END subscribe_topics]
//    }
//
//}

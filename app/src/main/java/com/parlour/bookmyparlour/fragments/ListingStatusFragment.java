package com.parlour.bookmyparlour.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.classes.user.Login_register;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lakhwinder on 8/16/2017.
 */

public class ListingStatusFragment extends BaseFragment {
    TextView _loginSeller, message;
    private StringRequest jsonArrayRequest;
    private String listing_status="0";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_listingstatus, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _loginSeller = (TextView) view.findViewById(R.id.listing_loginasseller);
        message = (TextView) view.findViewById(R.id.listingmesaage);

        getstatus();

        try {
            if ( user.isLoggedIn() && user.getUserType().equals("user")) {
                if (listing_status.equals("1")) {
                    _loginSeller.setVisibility(View.VISIBLE);
                    _loginSeller.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showOptionDialog("Do you want to Login as Seller ?", new Helper.OnChoiceListener() {
                                @Override
                                public void onChoose(boolean isPositive) {
                                    if (isPositive) {
                                        user.logout();
                                        Intent intent = new Intent(getActivity(), Login_register.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
                                    } else {

                                    }
                                }
                            });
                        }
                    });
                }
            } else {
                _loginSeller.setVisibility(View.GONE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getstatus() {

        makeRequest(true, WebServices.GetListingStatus, "user_id=" + user.getUserId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject jsonObject = jsonObj.getJSONObject("result");
                    String mmessage = jsonObject.getString("message");
                    message.setText("" + mmessage);
                    if (jsonObject.has("resarray")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("resarray");

                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {

                                listing_status = jsonArray.getJSONObject(i).getString("listing_status");

                            }
                        } else {

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });
    }

}

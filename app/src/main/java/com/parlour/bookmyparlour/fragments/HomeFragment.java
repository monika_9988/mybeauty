package com.parlour.bookmyparlour.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.GenericUSerModel;
import com.parlour.bookmyparlour.Models.ServicesModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.user.BookingServicesAdapter;
import com.parlour.bookmyparlour.adapter.user.HomeParloursAdapter;
import com.parlour.bookmyparlour.classes.user.MapLocationUSer;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.TextUtils;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/16/2017.
 */

public class HomeFragment extends BaseFragment {
    TextView textView;
    RecyclerView recyclerView;
    private SearchView.SearchAutoComplete autoCompleteTextView;
    HomeParloursAdapter homeParloursAdapter;
    ArrayList<ServicesModel> servicesModelArrayList = new ArrayList<>();
    ArrayList<GenericUSerModel> hOmeUSerModelArrayList = new ArrayList<>();
    private StringRequest jsonArrayRequest;
    private BookingServicesAdapter serviceadapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.homeuser, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.rec_homeuser);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        homeParloursAdapter = new HomeParloursAdapter(getActivity(), hOmeUSerModelArrayList, new HomeParloursAdapter.setopenMApLocation() {
            @Override
            public void onMapLocationClick(GenericUSerModel hOmeUSerModel) {
                Intent intent=new Intent(getActivity(), MapLocationUSer.class);
                intent.putExtra("lat", TextUtils.getDouble(hOmeUSerModel.getLat()));
                intent.putExtra("lng",  TextUtils.getDouble(hOmeUSerModel.getLng()));
                intent.putExtra("name",hOmeUSerModel.getTitle());
                intent.putExtra("address",hOmeUSerModel.getAddress());
                startActivity(intent);
            }

        });
        recyclerView.setAdapter(homeParloursAdapter);
        if (Constants.Validator.isNetworkConnected(getActivity())) {
            getParloursList();

        } else {
            parseData(user.getString(Constants.AllParloursResponse));
//            showInformationDialog(getString(R.string.checkinternet));
        }

        if (user.ifShowed()) {
        }else{
            showAboutDialog();
        }
    }

    public void showAboutDialog() {
        user.isShown(true);

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.activity_aboutustxt);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView txt_about = (TextView) dialog.findViewById(R.id.txtabout);
        ImageView image_close = (ImageView) dialog.findViewById(R.id.ss);
        LinearLayout linearLayout = (LinearLayout) dialog.findViewById(R.id.ll_dia);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        String text = "Worried about your next visit to saloon ? No need to panic now ! All saloons in your city are only one click away from you. There are some listed saloons that provide appointment for their beauty services through this<font color=#de4688><i> BOOK MY PARLOUR</i></font> app. Get ready to explore saloons and their services. Also check whether they provide home services or not and you can fix the appointment <font color=#de4688><i>RIGHT NOW</i></font>. \n(No booking charges applicable)";
        txt_about.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        dialog.show();


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.home_user, menu);

    }


    public void getParloursList() {
        makeRequest(true, WebServices.GetAllParlours, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                Log.d("service response", "---  " + response);
                parseData(response);

            }
        });
    }

    public void parseData(String response) {

        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONObject result = jsonObj.getJSONObject("result");
            String message = result.getString("message");
            String status = result.getString("status");
            hOmeUSerModelArrayList.clear();

            if (status.equalsIgnoreCase("valid")) {
                JSONArray jsonArray = result.getJSONArray("resarray");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Log.d("arrayyyyyy", "" + i);
                    GenericUSerModel adapterModel = new GenericUSerModel();

                    adapterModel.setTitle(jsonArray.getJSONObject(i).getString("Title"));
                    adapterModel.setDescription(jsonArray.getJSONObject(i).getString("details"));
                    adapterModel.setId(jsonArray.getJSONObject(i).getString("saloon_id"));
                    adapterModel.setAddress(jsonArray.getJSONObject(i).getString("address_line1") + jsonArray.getJSONObject(i).getString("address_line2"));
                    adapterModel.setImage(jsonArray.getJSONObject(i).getString("feature_image"));
                    adapterModel.setLat(jsonArray.getJSONObject(i).getString("latitude"));
                    adapterModel.setLng(jsonArray.getJSONObject(i).getString("longitude"));
                    adapterModel.setEmail(jsonArray.getJSONObject(i).getString("email"));

                    hOmeUSerModelArrayList.add(adapterModel);

                }

            } else {

                showInformationDialog(message);

            }
            homeParloursAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
            hideLoading();

        }
    }

    public void parseDataSearch(String response) {

        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONObject result = jsonObj.getJSONObject("result");
            String message = result.getString("message");
            String status = result.getString("status");
            hOmeUSerModelArrayList.clear();

            if (status.equalsIgnoreCase("valid")) {
                JSONArray jsonArray = result.getJSONArray("resarray");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Log.d("arrayyyyyy", "" + i);
                    GenericUSerModel adapterModel = new GenericUSerModel();

                    adapterModel.setTitle(jsonArray.getJSONObject(i).getString("Title"));
                    adapterModel.setDescription(jsonArray.getJSONObject(i).getString("details"));
                    adapterModel.setId(jsonArray.getJSONObject(i).getString("saloon_id"));
                    adapterModel.setAddress(jsonArray.getJSONObject(i).getString("address_line1") + jsonArray.getJSONObject(i).getString("address_line2"));
                    adapterModel.setImage(jsonArray.getJSONObject(i).getString("feature_image"));
                    adapterModel.setLat(jsonArray.getJSONObject(i).getString("latitude"));
                    adapterModel.setLng(jsonArray.getJSONObject(i).getString("longitude"));
                    adapterModel.setEmail(jsonArray.getJSONObject(i).getString("email"));

                    hOmeUSerModelArrayList.add(adapterModel);

                }

            } else {
                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
            }
            homeParloursAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
            hideLoading();

        }
    }

    public void getParloursListSearch(String text) {
        makeRequest(false, WebServices.GetAllParloursSearch, "searchtext=" + text, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                Log.d("service response", "---  " + response);
                parseDataSearch(response);
            }
        });
    }


}

package com.parlour.bookmyparlour.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.ServicesModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.user.BookingServicesAdapter;
import com.parlour.bookmyparlour.classes.user.BookMyService;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/16/2017.
 */

public class AboutUsParlour extends BaseFragment {
    TextView txtbooking, messageservices;
    RecyclerView recyclerView;
    private BookingServicesAdapter serviceadapter;
    ArrayList<ServicesModel> genericUSerModelArrayList = new ArrayList<>();
    private StringRequest jsonArrayRequest;
    ImageView image;
    LinearLayout linearLayout;
    TextView name, address, phome, derc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.aboutus_parlour, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.rec_sub);
        image = (ImageView) view.findViewById(R.id.hfeatureImage);
        name = (TextView) view.findViewById(R.id.hdetailname);
        address = (TextView) view.findViewById(R.id.haddressnew);
        derc = (TextView) view.findViewById(R.id.h_descr);
        phome = (TextView) view.findViewById(R.id.hphonenew);
        messageservices = (TextView) view.findViewById(R.id.message_services);
        linearLayout = (LinearLayout) view.findViewById(R.id.ll_about);
        setupUI(linearLayout);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        serviceadapter = new BookingServicesAdapter(getActivity(), genericUSerModelArrayList, new BookingServicesAdapter.bookNowClickListener() {
            @Override
            public void setonbookNowClickListener(ServicesModel servicesModel) {
                    user.setServiceId(servicesModel.getId());
                    Intent intent = new Intent(getActivity(), BookMyService.class);
                    intent.putExtra("servicename", servicesModel.getTitle());
                    startActivity(intent);
                    getActivity().finish();
            }
        });
        recyclerView.setAdapter(serviceadapter);
        getMerchantDetails();
    }

    public void getMerchantDetails() {

        makeRequest(true, WebServices.GetDEtailsMerc, "parlour_id=" + user.getParlourId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                Log.d("service response", "---  " + response);
                Log.d("service response", "---  " + user.getUserId());
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");

                    if (status.equalsIgnoreCase("valid")) {

                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            utils.setImagePicass(WebServices.DomainNameImage + jsonArray.getJSONObject(i).getString("feature_image"), getActivity(), image);
                            name.setText(jsonArray.getJSONObject(i).getString("Title"));
                            address.setText(jsonArray.getJSONObject(i).getString("address_line1"));
                            phome.setText(jsonArray.getJSONObject(i).getString("phone"));
                            derc.setText(jsonArray.getJSONObject(i).getString("details"));
                        }
                        getServices();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });

    }

    private void getServices() {

        makeRequest(false, WebServices.GetAllServices, "parlour_id=" + user.getParlourId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    genericUSerModelArrayList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        messageservices.setText(getString(R.string.we_provide_following_services));
                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            ServicesModel adapterModel = new ServicesModel();

                            adapterModel.setTitle(jsonArray.getJSONObject(i).getString("service_title"));
                            adapterModel.setDescription(jsonArray.getJSONObject(i).getString("offer_text"));
                            adapterModel.setId(jsonArray.getJSONObject(i).getString("service_id"));
                            adapterModel.setImage(jsonArray.getJSONObject(i).getString("s_image"));
                            adapterModel.setStartprice(jsonArray.getJSONObject(i).getString("start_price"));
                            adapterModel.setEndprice(jsonArray.getJSONObject(i).getString("end_price"));

                            genericUSerModelArrayList.add(adapterModel);

                        }
                    } else {
                        messageservices.setText("No services provided !");

//                        showInformationDialog(message);

                    }
                    serviceadapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });

    }
}
package com.parlour.bookmyparlour.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.classes.user.Login_register;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lakhwinder on 8/16/2017.
 */

public class ChangePasswordFragment extends BaseFragment {
    private StringRequest jsonArrayRequest;
    EditText opass, newpass, conpass;
    TextView _reset;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup view, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_changepass, view, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        opass = (EditText) view.findViewById(R.id.old_pass);
        newpass = (EditText) view.findViewById(R.id.new_pass);
        conpass = (EditText) view.findViewById(R.id.confirm_pass);
        _reset = (TextView) view.findViewById(R.id.btn_changepass);

        _reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String _opass = opass.getText().toString().trim();
                String _newpass = newpass.getText().toString().trim();
                String _conpass = conpass.getText().toString().trim();

                if (_opass.isEmpty()) {
                    opass.setError(getString(R.string.error_empty_field));
                    opass.requestFocus();
                    newpass.clearFocus();
                    conpass.clearFocus();
                    return;
                }
                if (_newpass.isEmpty()) {
                    newpass.setError(getString(R.string.error_empty_field));
                    newpass.requestFocus();
                    opass.clearFocus();
                    conpass.clearFocus();

                    return;
                }
                if (_conpass.isEmpty()) {
                    conpass.setError(getString(R.string.error_empty_field));
                    conpass.requestFocus();
                    newpass.clearFocus();
                    opass.clearFocus();

                    return;
                }
                if (_newpass.equals(_conpass)) {
                    conpass.setError("Password not matched!");
                    conpass.requestFocus();
                    newpass.clearFocus();
                    opass.clearFocus();

                    return;
                }
                if (Constants.Validator.isNetworkConnected(getActivity())) {
                    changePassword(_opass, _newpass);

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });

    }

    private void changePassword(String opass, String _newpass) {
        makeRequest(true, WebServices.ChangePassword, "oldpassword=" + opass + "&newpassword=" + _newpass + "&user_id=" + user.getUserId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    if (status.equalsIgnoreCase("valid")) {

                        showOptionDialog(message, new Helper.OnChoiceListener() {
                            @Override
                            public void onChoose(boolean isPositive) {
                                if (isPositive) {
                                    Intent intent = new Intent(getActivity(), Login_register.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
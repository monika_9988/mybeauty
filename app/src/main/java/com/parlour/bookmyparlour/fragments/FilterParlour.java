package com.parlour.bookmyparlour.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.Model.AdminServiceModel;
import com.parlour.bookmyparlour.Models.ServicesModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.user.MyCustomAdapter;
import com.parlour.bookmyparlour.adapter.user.BookingServicesAdapter;
import com.parlour.bookmyparlour.classes.user.HomeUser;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/16/2017.
 */

public class FilterParlour extends BaseFragment {
    TextView txtbooking, messageservices;
    RecyclerView recyclerView;
    private BookingServicesAdapter serviceadapter;
    ArrayList<ServicesModel> genericUSerModelArrayList = new ArrayList<>();
    private StringRequest jsonArrayRequest;
    ImageView image;
    LinearLayout linearLayout;
    TextView name, address, phome, derc;
    private ArrayList<AdminServiceModel> servicemodelArrayList = new ArrayList<>();
    private String to_price;
    private String from_price;
    private String service_name;
    private String service_id;
    private ArrayList<ServicesModel> servicesModelArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.filterparlour, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.filterrec);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getservices();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                openFilterDialog();

            }
        }, 1000);

    }

    private void openFilterDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_filter);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        Spinner sp_ser = (Spinner) dialog.findViewById(R.id.sp_chooseser);
        Spinner sp_to = (Spinner) dialog.findViewById(R.id.sp_to);
        Spinner sp_from = (Spinner) dialog.findViewById(R.id.sp_from);


        sp_ser.setAdapter(new MyCustomAdapter(getActivity(), servicemodelArrayList));
        sp_to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                to_price = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                from_price = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_ser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> ad, View view, int position, long id) {
                AdminServiceModel adminServiceModel = (AdminServiceModel) ad.getItemAtPosition(position);
                service_name = adminServiceModel.getName();
                service_id = adminServiceModel.getId();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.Validator.isNetworkConnected(getActivity())) {
                    if (service_name.equals("Choose Service")) {
                        showInformationDialog("Please pick any service !");
                    } else {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        getParloursListFilter(service_id, from_price, to_price);
                    }
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Intent intent = new Intent(getActivity(), HomeUser.class);
                intent.putExtra(Constants.fromstatus, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().finish();
            }
        });

        dialog.show();
    }

    public void getParloursListFilter(String serviceid, String from, String to) {

        makeRequest(true, WebServices.FilterSearch, "service_id=" + serviceid + "&start_price=" + from + "&end_price=" + to, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    servicesModelArrayList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            ServicesModel adapterModel = new ServicesModel();

                            adapterModel.setTitle(jsonArray.getJSONObject(i).getString("service_title"));
                            adapterModel.setDescription(jsonArray.getJSONObject(i).getString("offer_text"));
                            adapterModel.setId(jsonArray.getJSONObject(i).getString("service_id"));
                            adapterModel.setImage(jsonArray.getJSONObject(i).getString("s_image"));
                            adapterModel.setStartprice(jsonArray.getJSONObject(i).getString("start_price"));
                            adapterModel.setEndprice(jsonArray.getJSONObject(i).getString("end_price"));

                            servicesModelArrayList.add(adapterModel);

                        }
                        serviceadapter = new BookingServicesAdapter(getActivity(), servicesModelArrayList, new BookingServicesAdapter.bookNowClickListener() {
                            @Override
                            public void setonbookNowClickListener(ServicesModel id) {

                            }
                        });
                        recyclerView.setAdapter(serviceadapter);
                    } else {

                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                Intent intent = new Intent(getActivity(), HomeUser.class);
                                intent.putExtra(Constants.fromstatus, true);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });

    }

    public void getservices() {
        makeRequest(false, WebServices.GetMainServices, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {

                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject jsonObj1 = jsonObj.getJSONObject("result");
                    String message = jsonObj1.getString("message");
                    String status = jsonObj1.getString("status");
                    servicemodelArrayList.clear();

                    AdminServiceModel categoryModel = new AdminServiceModel();
                    categoryModel.setId("0");
                    categoryModel.setName("Choose Service");
                    servicemodelArrayList.add(categoryModel);
                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = jsonObj1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            categoryModel = new AdminServiceModel();

                            categoryModel.setId(jsonArray.getJSONObject(i).getString("service_id"));
                            categoryModel.setName(jsonArray.getJSONObject(i).getString("s_title"));
                            servicemodelArrayList.add(categoryModel);

                        }
                    } else {
//                        showInformationDialog(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });
    }
}
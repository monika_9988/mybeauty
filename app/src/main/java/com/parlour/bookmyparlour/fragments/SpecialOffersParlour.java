package com.parlour.bookmyparlour.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parlour.bookmyparlour.Models.Model.SpecialOffersModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.user.SpecialoffersAdapter;
import com.parlour.bookmyparlour.classes.user.ParlourDetail;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/16/2017.
 */

public class SpecialOffersParlour extends BaseFragment {
    RecyclerView rec_offers;
    SpecialoffersAdapter specialoffersAdapter;
    ArrayList<SpecialOffersModel> offersModelArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_specialoffers, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rec_offers = (RecyclerView) view.findViewById(R.id.rec_offers);
        rec_offers.setLayoutManager(new LinearLayoutManager(getActivity()));
        specialoffersAdapter = new SpecialoffersAdapter(getActivity(), offersModelArrayList, new SpecialoffersAdapter.onClick() {
            @Override
            public void onclick() {
                Intent intent = new Intent(getActivity(), ParlourDetail.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        rec_offers.setAdapter(specialoffersAdapter);
        if (Constants.Validator.isNetworkConnected(getActivity())) {
            getAvailableOffers();
        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }

    public void getAvailableOffers() {
        makeRequest(true, WebServices.GetAllOffers, "user_id=" + user.getParlourId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    offersModelArrayList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            SpecialOffersModel offerModel = new SpecialOffersModel();
                            offerModel.setDescription(jsonArray.getJSONObject(i).getString("o_text"));
                            offerModel.setTitle(jsonArray.getJSONObject(i).getString("o_title"));
                            offerModel.setImage(jsonArray.getJSONObject(i).getString("o_image"));
                            offerModel.setId(jsonArray.getJSONObject(i).getString("o_id"));
                            offerModel.setDate(jsonArray.getJSONObject(i).getString("validity_date"));
                            offersModelArrayList.add(offerModel);

                        }

                    } else {

                        showInformationDialog(message);

                    }
                    specialoffersAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });
    }

}

package com.parlour.bookmyparlour.classes.user;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.CategoryModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Lakhwinder on 8/17/2017.
 */

public class BookMyService extends BaseActivity {
    EditText _name, _number, _message;
    TextView _time, _date, _book, _chooseCat, _showcat, _changetime,
            _changedate;
    LinearLayout ll_choosecat;
    ArrayList<CategoryModel> modelArrayList = new ArrayList<>();
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private Calendar calendar;
    private int CalendarMinute;
    private TimePickerDialog timepickerdialog;
    private String format;
    private int mYear;
    private int mMonth;
    private int mDay;
    private StringRequest jsonArrayRequest;
    int selecteditem;
    private String subService_ID = "0";
    private String dateToSend;
    private String timeToSend;
    private String serviceName;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        serviceName = getIntent().getStringExtra("servicename");
        _name = (EditText) findViewById(R.id.book_name);
        _number = (EditText) findViewById(R.id.book_phone);
        _message = (EditText) findViewById(R.id.book_message);
        _time = (TextView) findViewById(R.id.book_time);
        _date = (TextView) findViewById(R.id.book_date);
        _chooseCat = (TextView) findViewById(R.id.book_choosecategory);
        _showcat = (TextView) findViewById(R.id.book_choosencategory);
        _book = (TextView) findViewById(R.id.book_conbooking);
        _changetime = (TextView) findViewById(R.id.book_changetime);
        _changedate = (TextView) findViewById(R.id.book_changedate);
        ll_choosecat = (LinearLayout) findViewById(R.id.ll_choosecate);
        linearLayout = (LinearLayout) findViewById(R.id.ll_book);
        setupUI(linearLayout);

        _time.setText(utils.getCurrentTime());
        _date.setText(utils.getCurrentDate() + " " + utils.currentday());
        _chooseCat.setText("Choose" + " " + serviceName + " " + "Type");

        dateToSend = utils.getCurrentDateforWebservice();
        timeToSend = utils.getCurrentTime();

        _date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdate();
            }
        });
        _time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime();
            }
        });
        _name.setText(user.getName());
        _number.setText(user.getPhone());
        _book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myname = _name.getText().toString().trim();
                String mymessage = _message.getText().toString().trim();
                String mynumber = _number.getText().toString().trim();
                if (myname.isEmpty()) {
                    _name.setError("Enter name");
                    _name.requestFocus();
                    _number.clearFocus();
                    return;
                }
                if (mynumber.isEmpty()) {
                    _number.setError("Enter Contact Number");
                    _number.requestFocus();
                    _name.clearFocus();
                    return;
                }
                if (mynumber.length() != 10) {
                    _number.setError("Enter valid number");
                    _number.requestFocus();
                    _name.clearFocus();
                    return;
                }
                if (subService_ID.equals("0")) {
                    showInformationDialog("Please specify sub-service");
                    return;
                }

                if (Constants.Validator.isNetworkConnected(BookMyService.this)) {
                    if (user.isLoggedIn()) {
                        uploaddetails(myname, mymessage, mynumber, user.getUserId());
                    } else {
                        getTEmpUserID(myname, mymessage, mynumber);
                    }
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        _chooseCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDay();
            }
        });
        getServices();
        hideSoftKeyboard();
    }

    public void getTEmpUserID(final String myname, final String mymessage, final String mynumber) {
        makeRequest(false, WebServices.GetTEmpUserid, "phone=" + mynumber, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            user.setUserId(jsonArray.getJSONObject(i).getString("id"));
                            uploaddetails(myname, mymessage, mynumber, user.getUserId());
                        }
                    } else {

                        showInformationDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void getServices() {
        makeRequest(false, WebServices.ViewSubServices, "parlour_id=" + user.getParlourId() + "&service_id=" + user.getServiceId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    modelArrayList.clear();
                    if (status.equalsIgnoreCase("valid")) {
                        ll_choosecat.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            CategoryModel categoryModel = new CategoryModel();
                            categoryModel.setName(jsonArray.getJSONObject(i).getString("detail"));
                            categoryModel.setId(jsonArray.getJSONObject(i).getString("id"));
                            modelArrayList.add(categoryModel);

                        }

                    } else {
                        subService_ID = "-1";
                        ll_choosecat.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    ll_choosecat.setVisibility(View.GONE);


                }
            }
        });

    }

    public void uploaddetails(String myname, String mymessage, final String mynumber, String userId) {

        mymessage = user.urlIncoder(mymessage);
        myname = user.urlIncoder(myname);
        timeToSend = user.urlIncoder(timeToSend);

        makeRequest(true, WebServices.BookmyParlour, "name=" + myname + "&phone=" + mynumber + "&preferabletime=" + timeToSend + "&date=" + dateToSend + "&subcat_id=" + subService_ID + "&message=" + mymessage + "&user_id=" + userId + "&service_id=" + user.getServiceId() + "&saloon_id=" + user.getParlourId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {

                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    final String status = jsonObj.getString("status");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(getString(R.string.thanksforchoosingservice) + message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                Intent intent = new Intent(BookMyService.this, BookingStatus.class);
                                startActivity(intent);
                                finish();
                            }
                        });


                    } else {

                        showInformationDialog(message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gohome, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_gohome:
                Intent intent = new Intent(BookMyService.this, HomeUser.class);
                intent.putExtra(Constants.fromstatus, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getdate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        long selectedtime = calendar.getTimeInMillis();

                        if (utils.compareDates(selectedtime)) {
                            showInformationDialog(getString(R.string.previousdateerror));
                        } else {
                            String day = utils.getDayNamefromdate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            _date.setText(dayOfMonth + "-" + MONTHS[(monthOfYear)] + " " + day);
                            dateToSend = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    public void setTime() {
        calendar = Calendar.getInstance();
        int CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(BookMyService.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String minuteOfDay;
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = getString(R.string.am);
                        } else if (hourOfDay == 12) {

                            format = getString(R.string.pm);

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = getString(R.string.pm);

                        } else {

                            format = getString(R.string.am);
                        }

                        if (minute < 10) {
                            minuteOfDay = "0" + minute;
                        } else {
                            minuteOfDay = "" + minute;
                        }
                        _time.setText(hourOfDay + ":" + minuteOfDay + " " + format);
                        timeToSend = hourOfDay + ":" + minuteOfDay + " " + format;

                    }
                }, CalendarHour, CalendarMinute, false);
        timepickerdialog.show();

    }

    private void chooseDay() {

        final ArrayList<CategoryModel> listingmodel = new ArrayList<CategoryModel>();
        ArrayList<String> listing = new ArrayList<String>();
        for (int i = 0; i < modelArrayList.size(); i++) {
            String name = modelArrayList.get(i).getName();
            listingmodel.add(modelArrayList.get(i));
            listing.add(name);
        }
        CharSequence[] dayList = listing.toArray(new CharSequence[listing.size()]);

        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setTitle(R.string.selectservices);

        alt_bld.setSingleChoiceItems(dayList, selecteditem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selecteditem = which;
            }
        }).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ListView list = ((AlertDialog) dialog).getListView();
//                Integer selected = (Integer)list.getItemAtPosition();
                String s = (String) list.getAdapter().getItem(selecteditem);
                CategoryModel checkedItem = modelArrayList.get(selecteditem);
                subService_ID = checkedItem.getId();
                Log.d("checkedItem", checkedItem.getId() + " --- ");
                Log.d("checkedItem", s + " --- ");
                _chooseCat.setText("Selected Service: " + checkedItem.getName());

            }
        }).setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = alt_bld.create();
        alert.show();
    }

}

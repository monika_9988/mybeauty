package com.parlour.bookmyparlour.classes.Merchant;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import static com.parlour.bookmyparlour.utils.config.Constants.REQCODE;

public class MerchantMainActivity extends BaseFragment implements View.OnClickListener {
    TextView _addViewServices, _UploadOffer, _viewBookings;
    EditText address, title, phone, detailss;
    ImageView imageView, editImage, done, edit;
    private static final int REQUEST_PERMISSION_IMAGECHOOSER = 121;
    Uri selectedImageUri;
    private String encodedImage;
    private JSONObject jsonObject = new JSONObject();
    private StringRequest jsonArrayRequest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_merchant_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _addViewServices = (TextView) view.findViewById(R.id.m_addviewService);
        _UploadOffer = (TextView) view.findViewById(R.id.m_uploadOffer);
        _viewBookings = (TextView) view.findViewById(R.id.m_bookingRequests);
        address = (EditText) view.findViewById(R.id.m_address);
        title = (EditText) view.findViewById(R.id.m_title);
        phone = (EditText) view.findViewById(R.id.m_phone);
        detailss = (EditText) view.findViewById(R.id.m_details);
        imageView = (ImageView) view.findViewById(R.id.hfeatureImage);
        editImage = (ImageView) view.findViewById(R.id.hedit);
        edit = (ImageView) view.findViewById(R.id.edit);
        done = (ImageView) view.findViewById(R.id.done);
        _addViewServices.setOnClickListener(this);
        _UploadOffer.setOnClickListener(this);
        _viewBookings.setOnClickListener(this);
        disableFields();

        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean permissionGranted = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionGranted) {
                    showImageChooser();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGECHOOSER);
                    }
                }
            }
        });
        edit.setOnClickListener(this);
        done.setOnClickListener(this);
        if (Constants.Validator.isNetworkConnected(getActivity())) {
            getMerchantDetails();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }

    private void disableFields() {

        address.setBackground(null);
        title.setBackground(null);
        phone.setBackground(null);
        detailss.setBackground(null);

        address.setPadding(0, 0, 0, 0);
        title.setPadding(0, 0, 0, 0);
        phone.setPadding(0, 0, 0, 0);
        detailss.setPadding(0, 0, 0, 0);

        address.setEnabled(false);
        title.setEnabled(false);
        phone.setEnabled(false);
        detailss.setEnabled(false);
    }

    private void editDetails(String address1, String title1, String phone1, String detailss1) {

        try {
            jsonObject.put("saloon_id", user.getSaloonId());
            jsonObject.put("title", title1);
            jsonObject.put("address_line1", address1);
            jsonObject.put("details", detailss1);
            jsonObject.put("phone", phone1);
            Log.d("logggg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        makeRequestPost(WebServices.UpdateDetails, jsonObject, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    Log.d("result", response.toString());
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equals("valid")) {
                        showInformationDialog(message);
                        disableFields();
                        done.setVisibility(View.GONE);
                        edit.setVisibility(View.VISIBLE);
                        getMerchantDetails();

                    } else {
                        showInformationDialog(message);
                    }
                } catch (JSONException e1) {
                    hideLoading();

                    e1.printStackTrace();
                }
            }
        });

    }

    private void showImageChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQCODE);

    }

    /*

     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQCODE && resultCode == getActivity().RESULT_OK && data != null) {
            selectedImageUri = data.getData();
            imageView.setImageURI(selectedImageUri);
            askForChangeImage();

        }

    }

    /*ask for change
     */
    public void askForChangeImage() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.change_image_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        ImageView featureImage = (ImageView) dialog.findViewById(R.id.chooseImage);
        featureImage.setImageURI(selectedImageUri);
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (Constants.Validator.isNetworkConnected(getActivity())) {
                    changeImage();

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (Constants.Validator.isNetworkConnected(getActivity())) {
                    getMerchantDetails();

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });

        dialog.show();

    }

    private void changeImage() {
        encodeImage();
        try {
            jsonObject.put("saloon_id", user.getSaloonId());
            jsonObject.put("image", encodedImage);
            Log.d("logggg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        makeRequestPost(WebServices.UpadateMerchantImage, jsonObject, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    Log.d("result", response.toString());
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equals("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getMerchantDetails();

                            }
                        });

                    } else {
                        showInformationDialog(message);
                    }
                } catch (JSONException e1) {

                    e1.printStackTrace();
                }
            }
        });
    }

    private void encodeImage() {
        Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        }
    }


    @Override
    public void onClick(View v) {
        if (v == _addViewServices) {

            Intent intent = new Intent(getActivity(), AddService.class);
            startActivity(intent);
            // finish();

        } else if (v == _UploadOffer) {
            Intent intent = new Intent(getActivity(), UploadOffer.class);
            startActivity(intent);
            // finish();

        } else if (v == _viewBookings) {
            Intent intent = new Intent(getActivity(), ViewBookings.class);
            startActivity(intent);
            //finish();

        } else if (v == edit) {
            address.setEnabled(true);
            title.setEnabled(true);
            phone.setEnabled(true);
            detailss.setEnabled(true);
            address.setBackgroundResource(android.R.drawable.editbox_background_normal);
            title.setBackgroundResource(android.R.drawable.editbox_background_normal);
            phone.setBackgroundResource(android.R.drawable.editbox_background_normal);
            detailss.setBackgroundResource(android.R.drawable.editbox_background_normal);

            edit.setVisibility(View.GONE);
            done.setVisibility(View.VISIBLE);

        } else if (v == done) {
            String address1 = address.getText().toString();
            String title1 = title.getText().toString();
            String phone1 = phone.getText().toString();
            String detailss1 = detailss.getText().toString();
            if (Constants.Validator.isNetworkConnected(getActivity())) {
                editDetails(address1, title1, phone1, detailss1);

            } else {
                showInformationDialog(getString(R.string.checkinternet));
            }

        }
    }

    @Nullable
    public void getMerchantDetails() {
        makeRequest(true, WebServices.GetDEtailsMerc, "parlour_id=" + user.getSaloonId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    String ImageUrl = "";

                    if (status.equalsIgnoreCase("valid")) {

                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ImageUrl = WebServices.DomainNameImage + jsonArray.getJSONObject(i).getString("feature_image");
                            utils.setImagePicass(ImageUrl, getActivity(), imageView);

                            Log.d("service imageurl", "" + ImageUrl);
                            title.setText("" + jsonArray.getJSONObject(i).getString("Title"));
                            address.setText(jsonArray.getJSONObject(i).getString("address_line1"));
                            phone.setText("" + jsonArray.getJSONObject(i).getString("phone"));
                            detailss.setText("" + jsonArray.getJSONObject(i).getString("details"));
                        }

                    } else {

                        showInformationDialog(message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        });

    }
}


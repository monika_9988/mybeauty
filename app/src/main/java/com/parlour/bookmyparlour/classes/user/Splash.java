package com.parlour.bookmyparlour.classes.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

public class Splash extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();
        final ProgressBar mProgress = (ProgressBar) findViewById(R.id.splash_screen_progress_bar);
        mProgress.showContextMenu();
        getParloursList();

     new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                mProgress.destroyDrawingCache();
                Intent i = new Intent(Splash.this, HomeUser.class);
                startActivity(i);
                finish();

            }
        }, SPLASH_TIME_OUT);
    }

    public void getParloursList() {
        makeRequest(false, WebServices.GetAllParlours, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    user.setString(Constants.AllParloursResponse, response);
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });
    }


}
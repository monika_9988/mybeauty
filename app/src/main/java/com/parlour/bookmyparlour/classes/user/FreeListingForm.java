package com.parlour.bookmyparlour.classes.user;

import android.Manifest;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.parlour.bookmyparlour.Models.Model.AdminServiceModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.user.MyCustomAdapter;
import com.parlour.bookmyparlour.generic.BaseFragment;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;
import static com.parlour.bookmyparlour.utils.config.Constants.REQCODE;

public class FreeListingForm extends BaseFragment {
    ImageView _chooseImage;
    //    @SerializedName("image")
    String encodedImage = "";
    private JSONObject jsonObject;

    private static final int REQUEST_PERMISSION_IMAGECHOOSER = 121;
    EditText _bussinessTitle;
    EditText _contactPerson;
    EditText _email;
    EditText _landline;
    EditText _phone;
    EditText _addressline1;
    EditText _addressline2;
    EditText _details;
    Spinner _city;
    TextView _starttime, _endtime, _selectDay;
    TextView _applyForListing;
    ArrayList<AdminServiceModel> cityModelArrayList = new ArrayList<>();
    private int CalendarHour, CalendarMinute;
    String format;
    Calendar calendar;
    TimePickerDialog timepickerdialog;
    private Uri selectedImageUri;
    String Stime, Etime;
    LinearLayout linearLayout;
    private String cityNme;
    private String name="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_free_listing_form, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _bussinessTitle = (EditText) view.findViewById(R.id.apply_businessTitle);
        _contactPerson = (EditText) view.findViewById(R.id.apply_contactPerson);
        _email = (EditText) view.findViewById(R.id.apply_email);
        _landline = (EditText) view.findViewById(R.id.apply_landline);
        _phone = (EditText) view.findViewById(R.id.apply_phone);
        _city = (Spinner) view.findViewById(R.id.sp_choosecity);

        linearLayout = (LinearLayout) view.findViewById(R.id.apply_activity_login);
        setupUI(linearLayout);
        _addressline1 = (EditText) view.findViewById(R.id.apply_addressline1);
        _addressline2 = (EditText) view.findViewById(R.id.apply_addressline2);
        _details = (EditText) view.findViewById(R.id.apply_details);
        _starttime = (TextView) view.findViewById(R.id.apply_starttime);
        _endtime = (TextView) view.findViewById(R.id.apply_endtime);

        _starttime.setText("Opening time : " + utils.getCurrentTime());
        _endtime.setText("Closing time : " + utils.getCurrentTime());
        _selectDay = (TextView) view.findViewById(R.id.apply_chooseday);
        _chooseImage = (ImageView) view.findViewById(R.id.chooseImage);
        _applyForListing = (TextView) view.findViewById(R.id.btn_applyForListing);
        Stime = utils.getCurrentTime();
        Etime = utils.getCurrentTime();
        _city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AdminServiceModel adminServiceModel = (AdminServiceModel) parent.getItemAtPosition(position);
                 name = adminServiceModel.getName();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getCity();
        _chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean permissionGranted = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionGranted) {
                    showImageChooser();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGECHOOSER);
                    }
                }
            }
        });

        _starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime("start");
            }
        });
        _endtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime("end");

            }
        });
        _selectDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDay();
            }


        });
        _applyForListing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strBtitle = _bussinessTitle.getText().toString();
                String strContactPerson = _contactPerson.getText().toString().trim();
                String strEmail = _email.getText().toString().trim();
                String strLandLine = _landline.getText().toString().trim();
                String strPhone = _phone.getText().toString().trim();
//                String strCity = _city.getText().toString().trim();
                String strAddress1 = _addressline1.getText().toString().trim();
                String strAddress2 = _addressline2.getText().toString().trim();
                String strDetails = _details.getText().toString().trim();
                String strStartT = _starttime.getText().toString().trim();
                String strEndT = _endtime.getText().toString();
                String strWorkDay = _selectDay.getText().toString();

                if (strBtitle.isEmpty()) {
                    _bussinessTitle.setError(getString(R.string.error_empty_field));
                    _bussinessTitle.requestFocus();
                    return;
                }
                if (strContactPerson.isEmpty()) {
                    _contactPerson.setError(getString(R.string.error_empty_field));
                    _contactPerson.requestFocus();
                    return;
                }
                if (strEmail.isEmpty()) {
                    _email.setError(getString(R.string.error_empty_field));
                    _email.requestFocus();
                    return;
                }
//                if (strLandLine.isEmpty()) {
//                    _landline.setError(getString(R.string.error_empty_field));
//                    _landline.requestFocus();
//                    return;
//                }
                if (strPhone.isEmpty()) {
                    _phone.setError(getString(R.string.error_empty_field));
                    _phone.requestFocus();
                    return;
                }

                if (strAddress1.isEmpty()) {
                    _addressline1.setError(getString(R.string.error_empty_field));
                    _addressline1.requestFocus();
                    return;
                }
                if (strAddress2.isEmpty()) {
                    _addressline2.setError(getString(R.string.error_empty_field));
                    _addressline2.requestFocus();
                    return;
                }
                if (strDetails.isEmpty()) {
                    _details.setError(getString(R.string.error_empty_field));
                    _details.requestFocus();
                    return;
                }

                if (selectedImageUri == null) {
                    showInformationDialog(getString(R.string.chooseimage));
                    return;
                }
                if (name.equals("Choose City")) {
                    showInformationDialog("Please Choose city");
                } else {
                    cityNme = name;
                }
                UploadFreeListingDetail(strBtitle, strContactPerson, strEmail, strLandLine, strPhone, cityNme, strAddress1, strAddress2, strDetails, strStartT, strEndT, strWorkDay);
            }
        });
        jsonObject = new JSONObject();

    }

    private void UploadFreeListingDetail(String strBtitle, String strContactPerson, String strEmail, String strLandLine, String strPhone, String strCity, String strAddress1, String strAddress2, String strDetails, String strStartT, String strEndT, String strWorkDay) {
        encodeImage();
        try {
            jsonObject.put("userId", user.getUserId());
            jsonObject.put("businessTitle", strBtitle);
            jsonObject.put("contactPerson", strContactPerson);
            jsonObject.put("email", strEmail);
            jsonObject.put("landline", strLandLine);
            jsonObject.put("phone", strPhone);
            jsonObject.put("city", strCity);
            jsonObject.put("addressLine1", strAddress1);
            jsonObject.put("addressLine2", strAddress2);
            jsonObject.put("lat", user.getUserLocation().getLatitude());
            jsonObject.put("long", user.getUserLocation().getLongitude());
            jsonObject.put("details", strDetails);
            jsonObject.put("startTime", Stime);
            jsonObject.put("endTime", Etime);
            jsonObject.put("workingDays", strWorkDay);
            jsonObject.put("image", encodedImage);
            Log.d("logggg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        makeRequestPost(WebServices.FreeListing, jsonObject, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equals("valid")) {
                        notifyAdmin();
                        showInformationDialog("Your free listing application is submitted to admin and pending for approval", new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {

                                Intent intent = new Intent(getActivity(), HomeUser.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                getActivity().finish();

                            }
                        });

                    } else {
                        showInformationDialog(message);
                    }
                } catch (JSONException e1) {
                    hideLoading();

                    e1.printStackTrace();
                }

            }
        });
    }

    public void notifyAdmin() {

        makeRequest(false, WebServices.SendNotification, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                Log.d("MyFirebaseIIDService response   id ", "---  " + user.getUserId());
            }
        });
    }

    private void chooseDay() {
        final CharSequence[] dayList = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "sunday"};

        AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
        alt_bld.setTitle(R.string.selectdays);
        alt_bld.setMultiChoiceItems(dayList, new boolean[]{false, false,
                        false, false, false, false, false},
                new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton, boolean isChecked) {

                    }
                });
        alt_bld.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ListView list = ((AlertDialog) dialog).getListView();

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < list.getCount(); i++) {
                    boolean checked = list.isItemChecked(i);

                    if (checked) {
                        if (sb.length() > 0) sb.append(", ");
                        sb.append(list.getItemAtPosition(i));
                    }
                }
                _selectDay.setText(getString(R.string.workingdays) + sb.toString());
//                Toast.makeText(getApplicationContext(),"Selected days: " + sb.toString(),
//                        Toast.LENGTH_SHORT).show();

            }
        });
        alt_bld.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog alert = alt_bld.create();
        alert.show();
    }

    public void getCity() {

        makeRequest(false, WebServices.GetCity, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    cityModelArrayList.clear();

                    AdminServiceModel categoryModel = new AdminServiceModel();
                    categoryModel.setName("Choose City");
                    categoryModel.setId("0");
                    cityModelArrayList.add(categoryModel);


                    if (status.equalsIgnoreCase("valid")) {

                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            categoryModel = new AdminServiceModel();
                            categoryModel.setName(jsonArray.getJSONObject(i).getString("city"));
                            categoryModel.setId(jsonArray.getJSONObject(i).getString("id"));
                            cityModelArrayList.add(categoryModel);

                        }

                    } else {

                    }
                    _city.setAdapter(new MyCustomAdapter(getActivity(), cityModelArrayList));

                } catch (JSONException e) {
                    e.printStackTrace();


                }
            }
        });

    }

    public void setTime(final String time) {
        String gettime = "";
        calendar = Calendar.getInstance();
        CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String minuteOfDay;
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = getString(R.string.am);
                        } else if (hourOfDay == 12) {

                            format = getString(R.string.pm);

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = getString(R.string.pm);

                        } else {

                            format = getString(R.string.am);
                        }

                        if (minute < 10) {
                            minuteOfDay = "0" + minute;
                        } else {
                            minuteOfDay = "" + minute;
                        }
                        if (time.equals("start")) {
                            Stime = hourOfDay + ":" + minuteOfDay + format;
                            _starttime.setText(hourOfDay + ":" + minuteOfDay + format);

                        } else {
                            Etime = hourOfDay + ":" + minuteOfDay + format;
                            _endtime.setText(hourOfDay + ":" + minuteOfDay + format);
                        }
                    }
                }, CalendarHour, CalendarMinute, false);
        timepickerdialog.show();

    }

    private void showImageChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQCODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQCODE && resultCode == RESULT_OK && data != null) {
            selectedImageUri = data.getData();
            _chooseImage.setImageURI(selectedImageUri);
        }
    }

    private void encodeImage() {
        Bitmap image = ((BitmapDrawable) _chooseImage.getDrawable()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        }
    }

}

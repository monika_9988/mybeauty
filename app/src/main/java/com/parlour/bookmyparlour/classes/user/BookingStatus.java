package com.parlour.bookmyparlour.classes.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.MyBookingModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.user.UserBookingsAdapter;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/17/2017.
 */

public class BookingStatus extends BaseActivity {
    private TextView newbooking;
    private RecyclerView rec_list;
    private String dateToSend, timeToSend;
    ArrayList<MyBookingModel> myBookingModelArrayList = new ArrayList<>();
    UserBookingsAdapter myBookingsAdapter;
    private StringRequest jsonArrayRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookinstatus);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        newbooking = (TextView) findViewById(R.id.sta_newbooking);
        rec_list = (RecyclerView) findViewById(R.id.rec_allbookings);
        LinearLayoutManager layoutManager = new LinearLayoutManager(BookingStatus.this, LinearLayoutManager.VERTICAL, false);
        rec_list.setLayoutManager(layoutManager);

        myBookingsAdapter = new UserBookingsAdapter(BookingStatus.this, myBookingModelArrayList, new UserBookingsAdapter.delete() {
            @Override
            public void onDelete(final String id) {

                showOptionDialog("Do you want to cancel booking ?", new Helper.OnChoiceListener() {
                    @Override
                    public void onChoose(boolean isPositive) {
                        if (isPositive) {
                            deletBooking(id);

                        } else {

                        }
                    }
                });
            }
        }, new UserBookingsAdapter.cconfirm() {
            @Override
            public void onCOnfirm(String id) {
                confirmbooking(id);
            }
        });
        rec_list.setAdapter(myBookingsAdapter);
        dateToSend = utils.getCurrentDateforWebservice();
        timeToSend = utils.getCurrentTime();
        Log.i("time    ", dateToSend + " --- " + timeToSend);
        newbooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingStatus.this, HomeUser.class);
                intent.putExtra(Constants.fromstatus, true);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        if (Constants.Validator.isNetworkConnected(BookingStatus.this)) {
            getbookings();
        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }

    public void deletBooking(final String id) {
makeRequest(true, WebServices.DeleteBooking, "user_id=" + user.getUserId() + "&booking_id=" + id, new onRequestComplete() {
    @Override
    public void onComplete(String response) {
        try {
            JSONObject jsonObj = new JSONObject(response);
            String message = jsonObj.getString("message");
            String status = jsonObj.getString("status");
            if (status.equalsIgnoreCase("valid")) {
                getbookings();
                showInformationDialog(message);

            } else {
                showInformationDialog(message);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            hideLoading();

        }

    }
});
    }

    public void confirmbooking(final String id) {
        makeRequest(true, WebServices.ConfirmBooking, "user_id=" + user.getUserId() + "&booking_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");
                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message);
                        getbookings();
                    } else {
                        showInformationDialog(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        });

    }

    public void getbookings() {
        makeRequest(true, WebServices.GetUSerBookings, "user_id=" + user.getUserId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");
                    myBookingModelArrayList.clear();
                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = jsonObj.getJSONArray("rows");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            MyBookingModel categoryModel = new MyBookingModel();
                            categoryModel.setServicename(jsonArray.getJSONObject(i).getString("service_title") + "(" + jsonArray.getJSONObject(i).getString("sub_service_title") + ")");
                            categoryModel.setId(jsonArray.getJSONObject(i).getString("booking_no"));
                            categoryModel.setName(jsonArray.getJSONObject(i).getString("name"));
                            categoryModel.setDate(jsonArray.getJSONObject(i).getString("a_date"));
                            categoryModel.setTime(jsonArray.getJSONObject(i).getString("a_time"));
                            categoryModel.setPhone(jsonArray.getJSONObject(i).getString("phone"));
                            categoryModel.setDerc(jsonArray.getJSONObject(i).getString("message"));
                            categoryModel.setStatus(jsonArray.getJSONObject(i).getString("booking_status"));
                            categoryModel.setSugdate(jsonArray.getJSONObject(i).getString("new_date"));
                            categoryModel.setSugtime(jsonArray.getJSONObject(i).getString("new_time"));
                            myBookingModelArrayList.add(categoryModel);

                        }
                    } else {
                        showInformationDialog(message);
                    }
                    myBookingsAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gohome, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_gohome:
                Intent intent = new Intent(BookingStatus.this, HomeUser.class);
                intent.putExtra(Constants.fromstatus, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

package com.parlour.bookmyparlour.classes.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseActivity;

import static com.parlour.bookmyparlour.R.id.map;

public class MapLocationUSer extends BaseActivity implements OnMapReadyCallback {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    private double userLat;
    private double useLng;
    @Nullable
    private String name;
    @Nullable
    private String address;
    TextView mapaddresss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maploactionuser);


        userLat = getIntent().getDoubleExtra("lat", 0.0);
        useLng = getIntent().getDoubleExtra("lng", 0.0);
        name = getIntent().getStringExtra("name");
        address = getIntent().getStringExtra("address");
        mapaddresss=(TextView) findViewById(R.id.mapaddress);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(""+name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(userLat, useLng);
        googleMap.addMarker(new MarkerOptions().position(sydney)
                .title(name));
        googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(sydney , 14.0f) );
        mapaddresss.setText(""+name+"\n"+ address);
    }
}
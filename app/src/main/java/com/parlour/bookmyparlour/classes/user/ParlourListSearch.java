package com.parlour.bookmyparlour.classes.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.parlour.bookmyparlour.R;

/**
 * Created by Lakhwinder on 8/18/2017.
 */

class ParlourListSearch extends Activity {
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rowitem_parlour);

        linearLayout = (LinearLayout)findViewById(R.id.ll_rowitem);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(ParlourListSearch.this,ParlourDetail.class);
                startActivity(intent);
            }
        });
    }
}

package com.parlour.bookmyparlour.classes.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Login_register extends BaseActivity {
    TextView _signin, _signup, _btnsignUp, _btnsignIn;
    LinearLayout _llLogin, _llSignUp;
    private CheckBox _ch_rememberme;
    private TextView _forgetpassword;
    private EditText _email;
    EditText _password, _signupEmail, _signupPass, _signupName, _signupPhone, _signupCpass;
    private String emailStr;
    private String passwordStr;
    String s_name, s_email, s_phome, s_pass, s_cpass;
    private StringRequest jsonArrayRequestReg;
    private boolean fromListing;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        fromListing = getIntent().getBooleanExtra(Constants.fromListing, false);
        _llLogin = (LinearLayout) findViewById(R.id.layoutLogin);
        _llSignUp = (LinearLayout) findViewById(R.id.layoutSignUp);
        _btnsignIn = (TextView) findViewById(R.id.sin);
        _btnsignUp = (TextView) findViewById(R.id.sup);
        linearLayout = (LinearLayout) findViewById(R.id.ll);
        setupUI(linearLayout);
        _ch_rememberme = (CheckBox) findViewById(R.id.login_rememberme);
        _forgetpassword = (TextView) findViewById(R.id.forgetpassword);
        _email = (EditText) findViewById(R.id.emailLogin);
        _password = (EditText) findViewById(R.id.passLogin);
        //---sign up parameteres-----
        _signupEmail = (EditText) findViewById(R.id.emailsignup);
        _signupName = (EditText) findViewById(R.id.nameSignup);
        _signupPhone = (EditText) findViewById(R.id.phonesignup);
        _signupPass = (EditText) findViewById(R.id.passSignup);
        _signupCpass = (EditText) findViewById(R.id.conpassSignup);

        rememberSettings();

        _signin = (TextView) findViewById(R.id.signin);
        _signup = (TextView) findViewById(R.id.signup);

        _btnsignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                _llSignUp.setVisibility(View.GONE);
                _llLogin.setVisibility(View.VISIBLE);
                _btnsignIn.setBackgroundColor(Color.parseColor("#e97cad"));
                _btnsignUp.setBackgroundColor(Color.parseColor("#80e24392"));
                _btnsignIn.setTextColor(Color.BLACK);
                _btnsignUp.setTextColor(Color.WHITE);

//                Intent i = new Intent(Login_register.this, FreeListingForm.class);
//                startActivity(i);

                // close this activity
            }
        });
        _btnsignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                _llSignUp.setVisibility(View.VISIBLE);
                _llLogin.setVisibility(View.GONE);
                _btnsignUp.setBackgroundColor(Color.parseColor("#e97cad"));
                _btnsignIn.setBackgroundColor(Color.parseColor("#80e24392"));
                _btnsignIn.setTextColor(Color.WHITE);
                _btnsignUp.setTextColor(Color.BLACK);

            }
        });
        _signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailStr = _email.getText().toString().trim();
                passwordStr = _password.getText().toString();

                if (emailStr.isEmpty()) {
                    _email.setError(getString(R.string.error_empty_field));
                    _email.requestFocus();
                    _password.clearFocus();

                    return;
                }
                if (!Constants.Validator.isValidEmail(emailStr)) {
                    _email.setError(getString(R.string.email_not_valid));
                    _email.requestFocus();
                    _password.clearFocus();
                    return;
                }
                if (passwordStr.isEmpty()) {
                    _password.setError(getString(R.string.error_empty_field));
                    _password.requestFocus();
                    _email.clearFocus();
                    return;
                }
                if (Constants.Validator.isNetworkConnected(Login_register.this)) {
                    login();
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        _signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_email = _signupEmail.getText().toString().trim();
                s_name = _signupName.getText().toString().trim();
                s_phome = _signupPhone.getText().toString().trim();
                s_pass = _signupPass.getText().toString().trim();
                s_cpass = _signupCpass.getText().toString().trim();
                if (s_name.isEmpty()) {
                    _signupName.setError(getString(R.string.error_empty_field));
                    _signupName.requestFocus();
                    _signupPass.clearFocus();
                    _signupEmail.clearFocus();
                    _signupPhone.clearFocus();

                    return;
                }
                if (s_email.isEmpty()) {
                    _signupEmail.setError(getString(R.string.error_empty_field));
                    _signupEmail.requestFocus();
                    _signupPass.clearFocus();
                    _signupName.clearFocus();
                    _signupPhone.clearFocus();

                    return;
                }
                if (!Constants.Validator.isValidEmail(s_email)) {
                    _signupEmail.setError(getString(R.string.email_not_valid));
                    _signupEmail.requestFocus();
                    _signupPass.clearFocus();
                    _signupName.clearFocus();
                    _signupPhone.clearFocus();
                    return;
                }

                if (s_phome.isEmpty()) {
                    _signupPhone.setError(getString(R.string.error_empty_field));
                    _signupPhone.requestFocus();
                    _signupPass.clearFocus();
                    _signupEmail.clearFocus();
                    _signupName.clearFocus();

                    return;
                }
                if (s_pass.isEmpty()) {
                    _signupPass.setError(getString(R.string.error_empty_field));
                    _signupPass.requestFocus();
                    _signupEmail.clearFocus();
                    _signupName.clearFocus();
                    _signupEmail.clearFocus();
                    return;
                }
                if (!s_cpass.equals(s_pass)) {
                    _signupCpass.setError(getString(R.string.passwordnotmatched));
                    _signupCpass.requestFocus();
                    _signupPass.clearFocus();
                    _signupName.clearFocus();
                    _signupEmail.clearFocus();
                    return;
                }
                if (s_phome.length() != 10) {
                    _signupPhone.setError("Enter valid number");
                    _signupPhone.requestFocus();
                    _signupEmail.clearFocus();
                    _signupName.clearFocus();
                    _signupPass.clearFocus();
                    return;
                }
                if (Constants.Validator.isNetworkConnected(Login_register.this)) {
                    register();
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });

        _ch_rememberme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                      @Override
                                                      public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                                                          String email = _email.getText().toString().trim();
                                                          String pass = _password.getText().toString().trim();
                                                          if (isChecked) {
                                                              user.setBoolean(Constants.Ischecked, true);
                                                              user.setString(Constants.Email, email);
                                                              user.setString(Constants.Password, pass);
                                                          } else {
                                                              user.setBoolean(Constants.Ischecked, false);
                                                              user.setString(Constants.Email, "");
                                                              user.setString(Constants.Password, "");
                                                              _email.setText("");
                                                              _password.setText("");
                                                          }
                                                      }

                                                  }

        );

        _forgetpassword.setOnClickListener(new View.OnClickListener()

                                           {
                                               @Override
                                               public void onClick(View view) {
                                                   Intent intent = new Intent(Login_register.this, Forgetpassword.class);
                                                   startActivity(intent);
                                               }
                                           }
        );

    }

    private void rememberSettings() {

        String userEmail = user.getString(Constants.Email);
//        String userPass = user.getString(Constants.Password);
        boolean check = user.getBoolean(Constants.Ischecked, false);
        manageVisibility();
        if (check) {
            _ch_rememberme.setChecked(true);
            _email.setText(userEmail);
//            _password.setText(userPass);
            _email.setSelection(_email.getText().length());
        }

    }

    private void manageVisibility() {
        _llSignUp.setVisibility(View.GONE);
        _llLogin.setVisibility(View.VISIBLE);
        _btnsignIn.setBackgroundColor(Color.parseColor("#e97cad"));
        _btnsignUp.setBackgroundColor(Color.parseColor("#80e24392"));
        _btnsignIn.setTextColor(Color.BLACK);
        _btnsignUp.setTextColor(Color.WHITE);
    }

    private void register() {
        makeRequest(true, WebServices.Register, "email=" + s_email + "&name=" + s_name + "&phone=" + s_phome + "&password=" + s_pass, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (user.checkResponse(response)) {
                        showInformationDialog(jsonObject.getString("message"), new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                _signupEmail.setText("");
                                _signupPass.setText("");
                                _signupName.setText("");
                                _signupPhone.setText("");
                                _signupCpass.setText("");
                                manageVisibility();

                            }
                        });
                    } else {
                        showInformationDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void login() {
        makeRequest(true, WebServices.Login, "email=" + emailStr + "&password=" + passwordStr, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");
                    if (jsonObject1.getString("status").equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = jsonObject1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            user.setUserId(jsonArray.getJSONObject(i).getString("id"));
                            user.setName(jsonArray.getJSONObject(i).getString("name"));
                            user.setEmail(jsonArray.getJSONObject(i).getString("email"));
                            user.setPhone(jsonArray.getJSONObject(i).getString("phone"));
                            user.setUserType(jsonArray.getJSONObject(i).getString("type"));
                            if (jsonArray.getJSONObject(i).getString("type").equals("user")) {

                            } else {
                                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                savedetails(refreshedToken, user.getUserId());
                                user.setSaloonId(jsonArray.getJSONObject(i).getString("parlour_id"));
                            }
                            user.loggedIn(true);

                            showInformationDialog(jsonObject1.getString("message"), new Helper.OnButtonClickListener() {
                                @Override
                                public void onClick() {
                                    if (fromListing) {
                                        Intent intent = new Intent(Login_register.this, FreeListingForm.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    if (user.getUserType().equals("user")) {

                                        Intent intent = new Intent(Login_register.this, HomeUser.class);
                                        startActivity(intent);
                                        finish();
                                    } else {

                                        Intent intent = new Intent(Login_register.this, HomeUser.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        }
                    } else {

                        showInformationDialog(jsonObject1.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void savedetails(final String token, String id) {

        String android_id = Settings.Secure.getString(Login_register.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        makeRequest(false, WebServices.DeviceRegister, "device_token=" + token + "&id=" + android_id+ "&seller_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                Log.d("MyFirebaseIIDService response", "---  " + response);
                Log.d("MyFirebaseIIDService response   id ", "---  " + user.getUserId());
                Log.d("MyFirebaseIIDService response   token ", "---  " + token);
            }
        });
    }

}

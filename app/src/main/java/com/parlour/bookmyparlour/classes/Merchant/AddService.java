package com.parlour.bookmyparlour.classes.Merchant;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.Model.AdminServiceModel;
import com.parlour.bookmyparlour.Models.Model.Servicemodel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.seller.AdminServiceAdapter;
import com.parlour.bookmyparlour.adapter.seller.ServiceAdapter;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static com.parlour.bookmyparlour.utils.config.Constants.REQCODE;
import static com.parlour.bookmyparlour.utils.config.Constants.REQCODE_UP;

public class AddService extends BaseActivity {
    RecyclerView _recyclerViewService, _adminServices;
    ImageView _addService;
    private StringRequest jsonArrayRequest;
    String service_id;
    // RecyclerView listView;
    //ring[] allserviceList;
    private JSONObject jsonObject = new JSONObject();

    private static final int REQUEST_PERMISSION_IMAGECHOOSER = 121;
    ImageView featureImage;
    private ArrayList<Servicemodel> serlist = new ArrayList<Servicemodel>();
    private ArrayList<AdminServiceModel> allserviceList = new ArrayList<AdminServiceModel>();
    View ChildView;
    String service_name;
    int GetItemPosition;
    String ps_id = "";
    TextView _addSubService;
    ImageView featureImageupdate;
    ServiceAdapter serviceAdapter;
    String encodedImage;
    String s_title = "";
    private RecyclerView offerText;
    TextView cancel;
    Uri selectedImageUri;
    TextView sstatus, _ser_title;
    private AdminServiceAdapter adminServiceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Service");
        _ser_title = (TextView) findViewById(R.id.servicetitle);

        _recyclerViewService = (RecyclerView) findViewById(R.id.rec_services);
        _recyclerViewService.setLayoutManager(new LinearLayoutManager(AddService.this));

        _addService = (ImageView) findViewById(R.id.addservices);
        sstatus = (TextView) findViewById(R.id.servicestatuc);

        serviceAdapter = new ServiceAdapter(AddService.this, serlist, new ServiceAdapter.ondeleteListener() {
            @Override
            public void ondelete(final String sid) {
                showOptionDialog("Do you really want to Delete ?", new Helper.OnChoiceListener() {
                    @Override
                    public void onChoose(boolean isPositive) {
                        if (isPositive) {
                            if (Constants.Validator.isNetworkConnected(AddService.this)) {
                                deleteService(sid);

                            } else {
                                showInformationDialog(getString(R.string.checkinternet));
                            }
                        } else {

                        }
                    }
                });
            }
        }, new ServiceAdapter.oneditListener() {
            @Override
            public void onedit(String title, String des, String sprice, String eprice, String image, String id, String offer_text) {
                updateDialog(title, des, sprice, eprice, image, id, offer_text);

            }
        }, new ServiceAdapter.onAddSubServices() {
            @Override
            public void onAdd(String serviceid, String title) {
                addSubServiceDialog(serviceid, title);
            }
        });
        _recyclerViewService.setAdapter(serviceAdapter);

        if (Constants.Validator.isNetworkConnected(AddService.this)) {
            getServices();

        } else {
            showInformationDialog(getString(R.string.checkinternet), new Helper.OnButtonClickListener() {
                @Override
                public void onClick() {
                    finish();
                }
            });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_services, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_addaervice:


                addServiceDialog();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addSubServiceDialog(final String sid, String title) {

        final Dialog dialog = new Dialog(AddService.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_sub_services);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final TextView text = (TextView) dialog.findViewById(R.id.edt_dia);
        final EditText sdescription = (EditText) dialog.findViewById(R.id.edt_description);


        text.setText(title);

        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = sdescription.getText().toString();
                if (des.isEmpty()) {
                    sdescription.setError(getString(R.string.error_empty_field));
                    sdescription.requestFocus();
                    return;
                }

                if (Constants.Validator.isNetworkConnected(AddService.this)) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    addSubService(sid, des);
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void addSubService(String sid, String des) {
        showLoading();
        hideSoftKeyboard();
        des = user.urlIncoder(des);
        makeRequest(true, WebServices.AddSubServiceMerchant, "service_id=" + sid + "&detail=" + des + "&parlour_id=" + user.getSaloonId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    if (status.equalsIgnoreCase("valid")) {

                        showInformationDialog(message);
                    } else {
                        showInformationDialog(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void updateDialog(String title, String des, String sprice1, String endprice1, String image, final String id, String offer_text1) {

        final Dialog dialog = new Dialog(AddService.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_edit);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final TextView text = (TextView) dialog.findViewById(R.id.edt_dia);
        final EditText sdescription = (EditText) dialog.findViewById(R.id.edt_description);

        final EditText sparice = (EditText) dialog.findViewById(R.id.edt_price);
        final EditText eprice = (EditText) dialog.findViewById(R.id.edt_eprice);
        final EditText offerText = (EditText) dialog.findViewById(R.id.edt_offertext);
        featureImageupdate = (ImageView) dialog.findViewById(R.id.chooseImage);
        featureImageupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean permissionGranted = ActivityCompat.checkSelfPermission(AddService.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionGranted) {
                    showImageChooser(false);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGECHOOSER);
                    }
                }
            }
        });
        String fullUrl = WebServices.DomainNameImage + image;
        utils.setImagePicass(fullUrl, AddService.this, featureImageupdate);

        text.setText(title);
        sdescription.setText(des);
        sparice.setText(sprice1);
        eprice.setText(endprice1);
        offerText.setText(offer_text1);
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str_des = sdescription.getText().toString().trim();
                String str_sprice = sparice.getText().toString().trim();
                String str_eprice = eprice.getText().toString().trim();
                String str_otext = offerText.getText().toString().trim();

                if (str_des.isEmpty()) {
                    sdescription.setError(getString(R.string.error_empty_field));
                    sdescription.requestFocus();
                    eprice.clearFocus();
                    offerText.clearFocus();
                    return;
                }
                if (str_sprice.isEmpty()) {
                    sparice.setError(getString(R.string.error_empty_field));
                    sparice.requestFocus();
                    eprice.clearFocus();
                    offerText.clearFocus();
                    return;
                }
                if (str_eprice.isEmpty()) {
                    eprice.setError(getString(R.string.error_empty_field));
                    eprice.requestFocus();
                    sdescription.clearFocus();
                    offerText.clearFocus();

                    return;
                }
//                if (str_otext.isEmpty()) {
//                    offerText.setError(getString(R.string.error_empty_field));
//                    sparice.requestFocus();
//                    eprice.clearFocus();
//                    sdescription.clearFocus();
//                    return;
//                }

//                if (featureImage.getDrawable() == null) {
//                    showInformationDialog("Choose Image");
//                    return;
//                }

                dialog.dismiss();
                if (Constants.Validator.isNetworkConnected(AddService.this)) {
                    hideSoftKeyboard();

                    editService(str_des, str_sprice, str_eprice, str_otext, id);


                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();


    }

    private void addServiceDialog() {
        final Dialog dialog = new Dialog(AddService.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_sellerallservices);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        offerText = (RecyclerView) dialog.findViewById(R.id.rec_addservice);
        cancel = (TextView) dialog.findViewById(R.id.cancel);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        offerText.setLayoutManager(new LinearLayoutManager(AddService.this));

        adminServiceAdapter = new AdminServiceAdapter(AddService.this, allserviceList);
        offerText.setAdapter(adminServiceAdapter);
        getAdminServices();
        offerText.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(AddService.this, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent motionEvent) {

                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {

                    GetItemPosition = Recyclerview.getChildAdapterPosition(ChildView);
                    service_id = allserviceList.get(GetItemPosition).getId();
                    service_name = allserviceList.get(GetItemPosition).getName();
                    addDialog(service_id, service_name);
                    dialog.dismiss();
                    // Toast.makeText(AddService.this, allserviceList.get(GetItemPosition).getId(), Toast.LENGTH_LONG).show();
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        dialog.show();
    }

    private void addDialog(final String service_id, final String service_name) {

        final Dialog dialog = new Dialog(AddService.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_edit);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final TextView text = (TextView) dialog.findViewById(R.id.edt_dia);
        final EditText sdescription = (EditText) dialog.findViewById(R.id.edt_description);

        final EditText sparice = (EditText) dialog.findViewById(R.id.edt_price);
        final EditText eprice = (EditText) dialog.findViewById(R.id.edt_eprice);
        final EditText offerText = (EditText) dialog.findViewById(R.id.edt_offertext);
        featureImage = (ImageView) dialog.findViewById(R.id.chooseImage);
        featureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean permissionGranted = ActivityCompat.checkSelfPermission(AddService.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionGranted) {
                    showImageChooser(true);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGECHOOSER);
                    }
                }
            }
        });

        text.setText(service_name);
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sdescription.getText().toString().trim().isEmpty()) {
                    sdescription.setError(getString(R.string.error_empty_field));
                    sparice.requestFocus();
                    eprice.clearFocus();
                    offerText.clearFocus();
                    return;
                }
                if (sparice.getText().toString().trim().isEmpty()) {
                    sparice.setError(getString(R.string.error_empty_field));
                    sdescription.requestFocus();
                    eprice.clearFocus();
                    offerText.clearFocus();
                    return;
                }
                if (eprice.getText().toString().trim().isEmpty()) {
                    eprice.setError(getString(R.string.error_empty_field));
                    sparice.requestFocus();
                    sdescription.clearFocus();
                    offerText.clearFocus();

                    return;
                }
//                if (offerText.getText().toString().trim().isEmpty()) {
//                    offerText.setError(getString(R.string.error_empty_field));
//                    sparice.requestFocus();
//                    eprice.clearFocus();
//                    sdescription.clearFocus();
//                    return;
//                }

                if (selectedImageUri == null) {
                    showInformationDialog("Choose Image");
                    return;
                }
                String des = sdescription.getText().toString();
                String sp = sparice.getText().toString();
                String ep = eprice.getText().toString();
                String ot = offerText.getText().toString();
                dialog.dismiss();
                if (Constants.Validator.isNetworkConnected(AddService.this)) {
                    hideSoftKeyboard();
                    addService(service_id, service_name, des, sp, ep, ot);
                    // Toast.makeText(AddService.this, ""+des+sp+ep+"VVVV"+item, Toast.LENGTH_LONG).show();
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void showImageChooser(boolean b) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (b) {
            startActivityForResult(intent, REQCODE);

        } else {
            startActivityForResult(intent, REQCODE_UP);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQCODE:
                if (resultCode == RESULT_OK && data != null) {
                    selectedImageUri = data.getData();
                    featureImage.setImageURI(selectedImageUri);
                }
                break;
            case REQCODE_UP:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImageUri1 = data.getData();
                    featureImageupdate.setImageURI(selectedImageUri1);
                }
                break;
            default:

        }


    }

    private void encodeImage(boolean b) {
        Bitmap image;
        if (b) {
            image = ((BitmapDrawable) featureImage.getDrawable()).getBitmap();
        } else {
            image = ((BitmapDrawable) featureImageupdate.getDrawable()).getBitmap();
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        }
    }

    public void getAdminServices() {
        makeRequest(false, WebServices.GetAdminServices, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    allserviceList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = result.getJSONArray("resarray");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            AdminServiceModel adminServiceModel = new AdminServiceModel();
                            service_id = jsonArray.getJSONObject(i).getString("service_id");
                            s_title = jsonArray.getJSONObject(i).getString("s_title");
                            adminServiceModel.setName(s_title);
                            adminServiceModel.setId(service_id);
                            allserviceList.add(adminServiceModel);

                        }
                        if (allserviceList.size() > 0) {
                            adminServiceAdapter.notifyDataSetChanged();

                        } else {
                            _ser_title.setText("No Service Added By Admin");
                        }

                    } else {

                        showInformationDialog(message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });
    }

    public void addService(String service_id, String title, String des, String sprice, String eprice, String offerText) {
        encodeImage(true);
        try {
            jsonObject.put("service_id", service_id.trim());
            jsonObject.put("service_title", title.trim());
            jsonObject.put("description", des.trim());
            jsonObject.put("start_price", sprice.trim());
            jsonObject.put("end_price", eprice.trim());
            jsonObject.put("parlour_id", user.getSaloonId());
            jsonObject.put("offerText", offerText.trim());
            jsonObject.put("image", encodedImage);
            Log.d("logggg", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
//
        makeRequestPost(WebServices.AddServiceMerchant, jsonObject, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("valid")) {
                        showInformationDialog(jsonObject.getString("message"), new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getServices();
                            }
                        });
                    } else {
                        showInformationDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {

                }
            }
        });

    }

    public void getServices() {
        makeRequest(true, WebServices.GetServicesMerchant, "parlour_id=" + user.getSaloonId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    serlist.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            Servicemodel adapterModel = new Servicemodel();
                            adapterModel.setName(jsonArray.getJSONObject(i).getString("description"));
                            adapterModel.setTitle(jsonArray.getJSONObject(i).getString("service_title"));
                            adapterModel.setSprice(jsonArray.getJSONObject(i).getString("start_price"));
                            adapterModel.setEprice(jsonArray.getJSONObject(i).getString("end_price"));
                            adapterModel.setImage(jsonArray.getJSONObject(i).getString("s_image"));
                            adapterModel.setSerid(jsonArray.getJSONObject(i).getString("service_id"));
                            adapterModel.setOfferText(jsonArray.getJSONObject(i).getString("offer_text"));
                            ps_id = jsonArray.getJSONObject(i).getString("id");
                            adapterModel.setId(ps_id);
                            serlist.add(adapterModel);

                        }
                        sstatus.setText("Services");
                    } else {
                        sstatus.setText("Currently no services added by You");

                    }
                    serviceAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });
    }

    public void deleteService(final String sid) {

        makeRequest(true, WebServices.DeleteServicesMerchant, "ps_id=" + sid, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        //Toast.makeText(AddService.this, "" + message, Toast.LENGTH_LONG).show();
                        getServices();
                    } else {

                        showInformationDialog(message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });

    }

    public void editService(String des, String sp, String ep, String ot, String id) {
        encodeImage(false);

        try {
            jsonObject.put("ps_id", id.trim());
            jsonObject.put("description", des.trim());
            jsonObject.put("sprice", sp.trim());
            jsonObject.put("eprice", ep.trim());
            jsonObject.put("offerText", ot.trim());
            jsonObject.put("image", encodedImage);
            Log.d("logggg", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
//
        makeRequestPost(WebServices.UPdateserviceMerchant, jsonObject, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    Log.d("result", response.toString());
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equals("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {

                            @Override
                            public void onClick() {
                                getServices();
                            }
                        });

                    } else {
                        showInformationDialog(message);
                    }
                } catch (JSONException e1) {
                    hideLoading();

                    e1.printStackTrace();
                }
            }
        });

    }
}
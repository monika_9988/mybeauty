package com.parlour.bookmyparlour.classes.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.generic.BaseActivity;

public class AbourUsTExt extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    private TextView txt_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutustxt);
        getSupportActionBar().hide();
        txt_about = (TextView) findViewById(R.id.txtabout);

        String text = "Worried about your next visit to saloon ? No need to panic now ! All saloons in your city are only one click away from you. There are some listed saloons that provide appointment for their beauty services through this BOOK MY PARLOUR app. Get ready to explore saloons and their services. Also check whether they provide home services or not and you can fix the appointment right <font color=\"#E72A02\"><bold>NOW</bold></font> \n(No booking charges applicable)";
        txt_about.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        txt_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AbourUsTExt.this, HomeUser.class);
                startActivity(i);
                finish();
            }
        });


//
    }

}
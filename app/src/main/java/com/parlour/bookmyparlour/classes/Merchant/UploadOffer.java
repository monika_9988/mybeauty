package com.parlour.bookmyparlour.classes.Merchant;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.Models.Model.OfferModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.seller.OfferAdapter;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.User;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import static com.parlour.bookmyparlour.utils.config.Constants.REQCODE;

public class UploadOffer extends BaseActivity {
    RecyclerView _recyclerViewOffers;
    ImageView _addOffers;
    private StringRequest jsonArrayRequest;

    ImageView featureImage;
    private JSONObject jsonObject = new JSONObject();

    private static final int REQUEST_PERMISSION_IMAGECHOOSER = 121;

    private ArrayList<OfferModel> offerList = new ArrayList<OfferModel>();
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private int mYear;
    private int mMonth;
    private int mDay;
    String service_name;
    OfferAdapter offerAdapter;
    String encodedImage = "";
    String discount = "1";
    private TextView _date;
    private String dateToSEnd;
    Uri selectedImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_offer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Upload Offers");

        user = new User();
        offerList = new ArrayList<>();

        _recyclerViewOffers = (RecyclerView) findViewById(R.id.rec_offers);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UploadOffer.this, LinearLayoutManager.VERTICAL, false);
        _recyclerViewOffers.setLayoutManager(linearLayoutManager);


        _addOffers = (ImageView) findViewById(R.id.add_offer);
        _addOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOfferDialog();
            }
        });
        offerAdapter = new OfferAdapter(UploadOffer.this, offerList, new OfferAdapter.ondeleteListener() {
            @Override
            public void ondelete(final String sid) {
                showOptionDialog("Do you really want to Delete this offer ?", new Helper.OnChoiceListener() {
                    @Override
                    public void onChoose(boolean isPositive) {
                        if (isPositive) {
                            if (Constants.Validator.isNetworkConnected(UploadOffer.this)) {
                                deleteService(sid);

                            } else {
                                showInformationDialog(getString(R.string.checkinternet));
                            }
                        } else {

                        }
                    }
                });
            }
        });
        _recyclerViewOffers.setAdapter(offerAdapter);

        if (Constants.Validator.isNetworkConnected(UploadOffer.this)) {
            getAvailableOffers();
        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_services, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_addaervice:


                addOfferDialog();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addOfferDialog() {
        final Dialog dialog = new Dialog(UploadOffer.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_upload_offer);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView _changedate = (TextView) dialog.findViewById(R.id.book_changedate);

        _date = (TextView) dialog.findViewById(R.id.book_date);
        dateToSEnd = utils.getCurrentDateforWebservice();
        _date.setText(utils.getCurrentDate() + " " + utils.currentday());

        _date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdate();
            }
        });
        final EditText text = (EditText) dialog.findViewById(R.id.upload_title);
        final EditText sdescription = (EditText) dialog.findViewById(R.id.upload_description);
        RadioButton upload_flatDiscount = (RadioButton) findViewById(R.id.upload_flatDiscount);
        RadioButton upload_perDiscount = (RadioButton) findViewById(R.id.upload_perDiscount);
        final RadioGroup diacountType = (RadioGroup) dialog.findViewById(R.id.radGroup);
        final EditText discountAmount = (EditText) dialog.findViewById(R.id.upload_price);
        featureImage = (ImageView) dialog.findViewById(R.id.chooseImage);
        text.setText(service_name);

        featureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean permissionGranted = ActivityCompat.checkSelfPermission(UploadOffer.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                if (permissionGranted) {
                    showImageChooser();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGECHOOSER);
                    }
                }
            }
        });
        diacountType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                discount = "";
                switch (diacountType.getCheckedRadioButtonId()) {
                    case R.id.upload_flatDiscount:
                        discount = "2";
                        break;
                    case R.id.upload_perDiscount:
                        discount = "1";
                        break;
                    default:
                        discount = "0";

                }
            }

        });
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = text.getText().toString();
                String des = sdescription.getText().toString();
                String disAmount = discountAmount.getText().toString();
                if (title.isEmpty()) {
                    text.setError(getString(R.string.error_empty_field));
                    text.requestFocus();
                    sdescription.clearFocus();
                    discountAmount.clearFocus();
                    return;
                }
                if (des.isEmpty()) {
                    sdescription.setError(getString(R.string.error_empty_field));
                    sdescription.requestFocus();
                    text.clearFocus();
                    discountAmount.clearFocus();
                    return;
                }
                if (disAmount.isEmpty()) {
                    discountAmount.setError(getString(R.string.error_empty_field));
                    discountAmount.requestFocus();
                    sdescription.clearFocus();
                    text.clearFocus();
                    return;
                }
                if (selectedImageUri == null) {
                    showInformationDialog("Choose Image");
                    return;
                }


                if (Constants.Validator.isNetworkConnected(UploadOffer.this)) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    hideSoftKeyboard();
                    addOffer(title, des, discount, disAmount, dateToSEnd);
                    // Toast.makeText(UploadOffer.this, ""+des+sp+ep+"VVVV"+item, Toast.LENGTH_LONG).show();
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    public void getdate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        long selectedtime = calendar.getTimeInMillis();
                        Calendar c = Calendar.getInstance();
                        long currenttime = c.getTimeInMillis();

                        Log.d("booking date", "" + selectedtime);
                        Log.d("booking date", "" + currenttime);

                        if (selectedtime < currenttime) {
                            showInformationDialog(getString(R.string.previousdateerror));

                        } else {
                            String day = utils.getDayNamefromdate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            _date.setText(dayOfMonth + "-" + MONTHS[(monthOfYear)] + " " + day);
                            dateToSEnd = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
//                            showInformationDialog("Sorry,No booking applicable for previous dates.");
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    private void addOffer(String title, String des, String discount, String disAmount, String date) {
        encodeImage();
        try {
            jsonObject.put("o_title", title);
            jsonObject.put("o_text", des);
            jsonObject.put("discount_type", discount);
            jsonObject.put("discount_amount", disAmount);
            jsonObject.put("vdate", date);
            jsonObject.put("user_id", user.getSaloonId());
            jsonObject.put("o_image", encodedImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//
makeRequestPost(WebServices.AddOffers, jsonObject, new onRequestComplete() {
    @Override
    public void onComplete(String response) {
        try {
            Log.d("result", response.toString());
            JSONObject jsonObj = new JSONObject(response.toString());
            String status = jsonObj.getString("status");
            String message = jsonObj.getString("message");

            if (status.equals("valid")) {
                showInformationDialog(message, new Helper.OnButtonClickListener() {
                    @Override
                    public void onClick() {
                        getAvailableOffers();
                    }
                });

            } else {
                showInformationDialog(message, new Helper.OnButtonClickListener() {
                    @Override
                    public void onClick() {
                        finish();
                    }
                });
            }
        } catch (JSONException e1) {
            hideLoading();

            e1.printStackTrace();
        }
    }
});

    }

    private void showImageChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQCODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQCODE && resultCode == RESULT_OK && data != null) {
            selectedImageUri = data.getData();
            featureImage.setImageURI(selectedImageUri);
        }
    }

    private void encodeImage() {
        Bitmap image = ((BitmapDrawable) featureImage.getDrawable()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        }
    }


    public void getAvailableOffers() {
        makeRequest(true, WebServices.GetAllOffers, "user_id=" + user.getSaloonId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject result = jsonObj.getJSONObject("result");
                    String message = result.getString("message");
                    String status = result.getString("status");
                    offerList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = result.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            OfferModel offerModel = new OfferModel();
                            offerModel.setDesc(jsonArray.getJSONObject(i).getString("o_text"));
                            offerModel.setTitle(jsonArray.getJSONObject(i).getString("o_title"));
                            offerModel.setDiscountType(jsonArray.getJSONObject(i).getString("discount_type"));
                            offerModel.setDiscount(jsonArray.getJSONObject(i).getString("discount_amount"));
                            offerModel.setImage(jsonArray.getJSONObject(i).getString("o_image"));
                            offerModel.setId(jsonArray.getJSONObject(i).getString("o_id"));
                            offerList.add(offerModel);

                        }

                    } else {

showOptionDialog(message, "Add Offer", "Cancel", new Helper.OnChoiceListener() {
    @Override
    public void onChoose(boolean isPositive) {
        if (isPositive){
            addOfferDialog();
        }else{

        }
    }
});

                    }
                    offerAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        });

    }

    public void deleteService(final String sid) {
        makeRequest(true, WebServices.DeletOffer, "o_id=" + sid + "&user_id=" + user.getSaloonId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getAvailableOffers();

                            }
                        });
                    } else {

                        showInformationDialog(message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });

    }


}

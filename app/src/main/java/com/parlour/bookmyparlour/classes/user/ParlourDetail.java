package com.parlour.bookmyparlour.classes.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.fragments.AboutUsParlour;
import com.parlour.bookmyparlour.fragments.SpecialOffersParlour;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.config.Constants;

public class ParlourDetail extends BaseActivity {

    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parlour_detail);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("About Us"));
        tabLayout.addTab(tabLayout.newTab().setText("Special Offers"));

        setFragment(0);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d("onTabSelected== ", "" + tab.getPosition());
                tab.getPosition();
                setFragment(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public void setFragment(int position) {
        Fragment currentFragment = getItem(position);
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.framelayout, currentFragment, "").commit();
    }

    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                AboutUsParlour tab1 = new AboutUsParlour();
                return tab1;
            case 1:
                SpecialOffersParlour tab2 = new SpecialOffersParlour();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gohome, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_gohome:
                Intent intent = new Intent(ParlourDetail.this, HomeUser.class);
                intent.putExtra(Constants.fromstatus,true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
               finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

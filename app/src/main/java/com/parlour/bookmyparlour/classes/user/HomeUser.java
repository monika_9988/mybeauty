package com.parlour.bookmyparlour.classes.user;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.parlour.bookmyparlour.LocationAwareActivity;
import com.parlour.bookmyparlour.Models.Model.AdminServiceModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.seller.AdminServiceAdapter;
import com.parlour.bookmyparlour.classes.Merchant.MerchantMainActivity;
import com.parlour.bookmyparlour.fragments.BookingStatusFragment;
import com.parlour.bookmyparlour.fragments.ChangePasswordFragment;
import com.parlour.bookmyparlour.fragments.FilterParlour;
import com.parlour.bookmyparlour.fragments.HomeFragment;
import com.parlour.bookmyparlour.fragments.ListingStatusFragment;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.TextUtils;
import com.parlour.bookmyparlour.utils.config.Constants;

import java.util.ArrayList;
import java.util.List;
//App ID: ca-app-pub-7472264333207144~9925273634
//        Ad unit ID: ca-app-pub-7472264333207144/9682209981

public class HomeUser extends LocationAwareActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private SearchView.SearchAutoComplete autoCompleteTextView;
    private DrawerLayout drawer;

    private static final String TAG_HOME = "home";
    private static final String TAG_Booking = "book";
    private static final String TAG_Listing = "list";
    private static final String TAG_ChangePass = "change";
    private static final String TAG_Filter = "filter";
    private static final String TAG_Login = "login";
    private static final String TAG_Logout = "logout";
    public static String CURRENT_TAG = TAG_HOME;
    private int navItemIndex;
    private Handler mHandler = new Handler();
    private NavigationView navigationView;
    private String[] activityTitles;
    private View navHeader;
    private Location lastLocation;
    private double longitude;
    private double latitude;
    private static final String TAG_Free = "free";
    private static final String TAG_Dash = "dash";
    private HomeFragment homeFragment;
    private StringRequest jsonArrayRequest;
    ArrayList<AdminServiceModel> servicemodelArrayList = new ArrayList<>();
    AdminServiceAdapter adminServiceAdapter;
    private String to_price;
    private String from_price;
    private String service_id;
    private MerchantMainActivity freeListingForm;
    private String service_name;
    private TextView header_name, header_email;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        startLocation();
        drawerSettings();
        googleAddSetup();


        boolean fromStatus = getIntent().getBooleanExtra(Constants.fromstatus, false);
        manageNavigationMenuvisibility();
    }

    private void googleAddSetup() {
         mAdView = (AdView) findViewById(R.id.adVieww);
        mAdView.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build());

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                //user returns to the app after tapping on an ad.
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                //Ad request fails.
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                //User left the app.
            }
            @Override

            public void onAdOpened() {
                super.onAdOpened();
                //Ad displayed on the screen.
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                //Ad finishes loading.
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
    private void manageNavigationMenuvisibility() {
        navItemIndex = 0;
        CURRENT_TAG = TAG_HOME;
        loadHomeFragment();
        if (user.isLoggedIn()) {

            if (user.getUserType().equals("user")) {
                navigationView.getMenu().findItem(R.id.nav_freelisting).setVisible(true);
                navigationView.getMenu().findItem(R.id.nav_gotomerchand).setVisible(false);
            } else {
                navigationView.getMenu().findItem(R.id.nav_freelisting).setVisible(false);
                navigationView.getMenu().findItem(R.id.nav_gotomerchand).setVisible(true);
            }
            navigationView.getMenu().findItem(R.id.nav_login).setTitle("Logout");
        } else {

            navigationView.getMenu().findItem(R.id.nav_login).setTitle("Login");
            navigationView.getMenu().findItem(R.id.nav_gotomerchand).setVisible(false);

        }

    }

    private void drawerSettings() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        headerSettings();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void headerSettings() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        header_name = (TextView) navHeader.findViewById(R.id.head_name);
        header_email = (TextView) navHeader.findViewById(R.id.head_email);
        if (user.getName().isEmpty()) {
            header_name.setText("Book My Parlour");
            header_email.setText("");
        } else {
            header_name.setText(user.getName());
            header_email.setText(user.getEmail());
        }
    }

    @Override
    protected void onUserLocationChanged(Location location) {
        lastLocation = location;
        user.getAddressFromLocation(location.getLatitude(), location.getLongitude(), new Helper.OnRequestCompleteListener<String>() {

            @Override
            public void onComplete(String object) {
                Log.d("location", "" + lastLocation);
                Log.d("location anme", "" + object);
                user.saveUserLocation(lastLocation);
                latitude = TextUtils.getDouble(String.valueOf(user.getUserLocation().getLatitude()));
                longitude = TextUtils.getDouble(String.valueOf(user.getUserLocation().getLongitude()));
            }
        });

        stopLocation();
    }

    @Override
    protected void onDataChanged() {

    }

    @Override
    protected void onShutDownCalled(List<String> stringArrayExtra) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.home_user, menu);

            MenuItem searchItem = menu.findItem(R.id.action_search);
            MenuItem about = menu.findItem(R.id.about);
            about.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    homeFragment.showAboutDialog();
                    return true;
                }
            });
            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {

                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    if (Constants.Validator.isNetworkConnected(HomeUser.this)) {
                        homeFragment.getParloursList();
                    } else {
                        homeFragment.parseData(user.getString(Constants.AllParloursResponse));
                    }
                    return true;
                }
            });

            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) searchItem.getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    if (Constants.Validator.isNetworkConnected(HomeUser.this)) {
                        homeFragment.getParloursList();

                    } else {
                        homeFragment.parseData(user.getString(Constants.AllParloursResponse));
                    }
                    return true;
                }
            });
            searchView.setOnQueryTextListener(this);

        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void goHome() {
        navigationView.invalidate();
        navItemIndex = 0;
        CURRENT_TAG = TAG_HOME;
        loadHomeFragment();
        invalidateOptionsMenu();
    }

    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        try {
            selectNavMenu();

            // set toolbar title
            setToolbarTitle();

            // if user select the current navigation menu again, don't do anything
            // just close the navigation drawer
            if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
                drawer.closeDrawers();

                // show or hide the fab button
                return;
            }

            // Sometimes, when fragment has huge data, screen seems hanging
            // when switching between navigation menus
            // So using runnable, the fragment is loaded with cross fade effect
            // This effect can be seen in GMail app
            Runnable mPendingRunnable = new Runnable() {
                @Override
                public void run() {
                    // update the main content by replacing fragments
                    if (getHomeFragment() != null) {

                        Fragment fragment = getHomeFragment();
                        FragmentManager fragmentManager = getSupportFragmentManager();

                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                android.R.anim.fade_out);
                        fragmentTransaction.replace(R.id.frame, fragment,CURRENT_TAG);
    //                    fragmentTransaction.addToBackStack(HomeUser.class.getName());
                        fragmentTransaction.commit();

    //                    FragmentTransaction transaction = fragmentManager.beginTransaction();
    //                    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    //                    transaction.replace(R.id.frame,sellerRegisteration).addToBackStack(null).commit();

                    }
                }
            };
            // If mPendingRunnable is not null, then add to the message queue
            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
            }
//        getSupportFragmentManager().addOnBackStackChangedListener(
//                new FragmentManager.OnBackStackChangedListener() {
//                    public void onBackStackChanged() {
//
//                    }
//                });
            //Closing drawer on item click
            drawer.closeDrawers();

            // refresh toolbar menu
            invalidateOptionsMenu();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                homeFragment = new HomeFragment();

                return homeFragment;
            case 1:
                if (user.isLoggedIn()) {
                    BookingStatusFragment photosFragment = new BookingStatusFragment();
                    return photosFragment;
                } else {
                    if (user.getUserId() != "") {
                        BookingStatusFragment photosFragment = new BookingStatusFragment();
                        return photosFragment;
                    } else {
                        showOptionDialog("⚠\nUser must need to login before viewing booking status. ", new Helper.OnChoiceListener() {
                            @Override
                            public void onChoose(boolean isPositive) {
                                if (isPositive) {
                                    Intent intent = new Intent(HomeUser.this, Login_register.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    goHome();

                                }
                            }
                        });
                        return null;
                    }

                }
            case 2:
                //  fragment
                if (user.isLoggedIn()) {
                    ListingStatusFragment moviesFragment = new ListingStatusFragment();
                    return moviesFragment;
                } else {
                    try {
                        showOptionDialog("⚠\nUser must need to login before viewing listing status. ", new Helper.OnChoiceListener() {
                            @Override
                            public void onChoose(boolean isPositive) {
                                if (isPositive) {
                                    Intent intent = new Intent(HomeUser.this, Login_register.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    goHome();


                                }
                            }
                        });
                        return null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            case 3:
                FilterParlour filterParlour = new FilterParlour();
                return filterParlour;
            case 4:
                if (user.isLoggedIn()) {

                    ChangePasswordFragment notificationsFragment = new ChangePasswordFragment();
                    return notificationsFragment;


                } else {
                    showOptionDialog("⚠\nUser must need to login before changing password. ", new Helper.OnChoiceListener() {
                        @Override
                        public void onChoose(boolean isPositive) {
                            if (isPositive) {
                                Intent intent = new Intent(HomeUser.this, Login_register.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                goHome();


                            }
                        }
                    });
                    return null;

                }
            case 5:
                // settings fragment
                String title = navigationView.getMenu().findItem(R.id.nav_login).getTitle().toString();
                if (title.equals("Login")) {

                    Intent intent = new Intent(HomeUser.this, Login_register.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                    return null;

                } else {

                    showOptionDialog("Do you want to Logout ?", new Helper.OnChoiceListener() {
                        @Override
                        public void onChoose(boolean isPositive) {
                            if (isPositive) {
                                user.logout();
                                header_name.setText("Book My Parlour");
                                header_email.setText("");
                                navigationView.getMenu().findItem(R.id.nav_login).setTitle("Login");
                                navigationView.getMenu().findItem(R.id.nav_freelisting).setVisible(true);
                                navigationView.getMenu().findItem(R.id.nav_gotomerchand).setVisible(false);
                                goHome();


                            } else {
                                goHome();


                            }
                        }
                    });
                    return null;

                }
            case 6:
                if (user.isLoggedIn()) {

                    FreeListingForm freeListingForm = new FreeListingForm();
                    return freeListingForm;

                } else {
                    showOptionDialog("Alert!!\nUser must need to login before applying for free listing. ", new Helper.OnChoiceListener() {
                        @Override
                        public void onChoose(boolean isPositive) {
                            if (isPositive) {
                                Intent intent = new Intent(HomeUser.this, Login_register.class);
                                intent.putExtra(Constants.fromListing, true);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                goHome();

                            }
                        }
                    });
                    return null;

                }
            case 7:
                freeListingForm = new MerchantMainActivity();
                return freeListingForm;

            default:
                return new HomeFragment();
        }
    }



    void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_home:
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                break;
            case R.id.nav_booking:
                navItemIndex = 1;
                CURRENT_TAG = TAG_Booking;
                break;
            case R.id.nav_lstatus:
                navItemIndex = 2;
                CURRENT_TAG = TAG_Listing;
                break;
            case R.id.nav_filter:
                navItemIndex = 3;
                CURRENT_TAG = TAG_Filter;
                break;
            case R.id.nav_changepass:
                navItemIndex = 4;
                CURRENT_TAG = TAG_ChangePass;
                break;
            case R.id.nav_login:
                navItemIndex = 5;
                CURRENT_TAG = TAG_Login;
                break;
            case R.id.nav_freelisting:
                navItemIndex = 6;

                CURRENT_TAG = TAG_Free;
                break;
            case R.id.nav_gotomerchand:
                navItemIndex = 7;

                CURRENT_TAG = TAG_Dash;
                break;
            default:
                navItemIndex = 0;
        }
        loadHomeFragment();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("Searching for: ", "" + query + "...");
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("Searching for: ", "" + newText + "...");

//        newText = newText.isEmpty() ? "" : "Query so far: " + newText;
        if (newText.isEmpty()) {

        } else {
            if (Constants.Validator.isNetworkConnected(HomeUser.this)) {
                homeFragment.getParloursListSearch(newText);

            } else {
                showInformationDialog(getString(R.string.checkinternet));
            }

        }
//        mSearchText.setText(newText);
        return true;
    }
}

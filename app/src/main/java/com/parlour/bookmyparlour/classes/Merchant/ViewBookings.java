package com.parlour.bookmyparlour.classes.Merchant;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.parlour.bookmyparlour.Models.Model.BookingModel;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.adapter.seller.BookingAdapter;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class ViewBookings extends BaseActivity {
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private RecyclerView rec_bookin;
    ArrayList<BookingModel> bookingModels = new ArrayList<>();
    private Calendar calendar;
    private int CalendarMinute;
    private TimePickerDialog timepickerdialog;
    private String format;
    private int mYear;
    private int mMonth;
    private int mDay;
    private
    @Nullable
    TextView _time, _date;
    private int monthInt;
    private String dateToSEnd;
    private String timeTosend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bookings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Manage Bookings");

        rec_bookin = (RecyclerView) findViewById(R.id.rec_viewbookings);

        rec_bookin.setLayoutManager(new LinearLayoutManager(ViewBookings.this));
        if (Constants.Validator.isNetworkConnected(ViewBookings.this)) {
            getBookings();

        } else {
            showInformationDialog(getString(R.string.checkinternet), new Helper.OnButtonClickListener() {
                @Override
                public void onClick() {
                    finish();
                }
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getBookings() {
        makeRequest(true, WebServices.GetAllBooking, "user_id=" + user.getSaloonId(), new onRequestComplete() {
                @Override
                public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");
                    bookingModels.clear();

                    if (status.equals("valid")) {
                        JSONArray jsonArray = jsonObj.getJSONArray("rows");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            BookingModel bookingModel = new BookingModel();

                            bookingModel.setName(jsonArray.getJSONObject(i).getString("name") + " - " + jsonArray.getJSONObject(i).getString("service_title") + "(" + jsonArray.getJSONObject(i).getString("sub_service_title") + ")");
                            bookingModel.setId(jsonArray.getJSONObject(i).getString("booking_no"));
                            bookingModel.setMessage(jsonArray.getJSONObject(i).getString("message"));
                            bookingModel.setStatus(jsonArray.getJSONObject(i).getString("booking_status"));
                            bookingModel.setDateTime(jsonArray.getJSONObject(i).getString("a_date") + " " + jsonArray.getJSONObject(i).getString("a_time"));
                        bookingModels.add(bookingModel);
                    }
                        BookingAdapter adminServiceAdapter = new BookingAdapter(ViewBookings.this, bookingModels, new BookingAdapter.onAcceptListener() {
                            @Override
                            public void onAccept(String id) {
                                acceptRequest(id);
                            }
                        }, new BookingAdapter.onDeclineListener() {
                            @Override
                            public void onDecline(String id) {
                                declineRequest(id);
                            }
                        }, new BookingAdapter.onnewTimeListener() {
                            @Override
                            public void onnewtime(String id) {
                                suggestNewTimeDialog(id);

                            }
                        });
                        rec_bookin.setAdapter(adminServiceAdapter);
                    } else {

                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();
                }
            }
        });

    }

    private void suggestNewTimeDialog(final String id) {
        final Dialog dialog = new Dialog(ViewBookings.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_newtime);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dateToSEnd = utils.getCurrentDateforWebservice();
        timeTosend = utils.getCurrentTime();
        final EditText mesaage = (EditText) dialog.findViewById(R.id.edt_messgae);
        _time = (TextView) dialog.findViewById(R.id.book_time);
        _date = (TextView) dialog.findViewById(R.id.book_date);
        TextView send = (TextView) dialog.findViewById(R.id.sendrequest);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancelrequest);

        _time.setText(utils.getCurrentTime());
        _date.setText(utils.getCurrentDate() + " " + utils.currentday());
        _time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime();
            }
        });
        _date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdate();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (mesaage.getText().toString().trim().isEmpty()){
                    mesaage.setError(getString(R.string.error_empty_field));
                    return;
                }
                if (Constants.Validator.isNetworkConnected(ViewBookings.this)) {

                    newtimeRequest(mesaage.getText().toString().trim(), id);
                } else {

                    showInformationDialog(getString(R.string.checkinternet), new Helper.OnButtonClickListener() {
                        @Override
                        public void onClick() {
                            finish();
                        }
                    });
                }
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }
        });
        dialog.show();
    }

    public void getdate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String day = utils.getDayNamefromdate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        long selectedtime = calendar.getTimeInMillis();
                        Calendar c = Calendar.getInstance();
                        long currenttime = c.getTimeInMillis();

                        Log.d("booking date", "" + selectedtime);
                        Log.d("booking date", "" + currenttime);

                        if (selectedtime < currenttime) {
                            showInformationDialog("Sorry,No booking applicable for previous dates.");

                        } else {

                            _date.setText(dayOfMonth + "-" + MONTHS[(monthOfYear)] + " " + day);
                            dateToSEnd = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    public void setTime() {
        String gettime = "";
        calendar = Calendar.getInstance();
        int CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = calendar.get(Calendar.MINUTE);


        timepickerdialog = new TimePickerDialog(ViewBookings.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String minuteOfDay;
                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        if (minute < 10) {
                            minuteOfDay = "0" + minute;
                        } else {
                            minuteOfDay = "" + minute;
                        }
                        timeTosend = hourOfDay + ":" + minuteOfDay + " " + format;
                        _time.setText(hourOfDay + ":" + minuteOfDay + " " + format);
                    }
                }, CalendarHour, CalendarMinute, false);
        timepickerdialog.show();

    }

    private void newtimeRequest(String message, final String id) {
        hideSoftKeyboard();
        timeTosend = user.urlIncoder(timeTosend);
        message = user.urlIncoder(message);
        makeRequest(true, WebServices.NewTime, "uid=" + user.getUserId() + "&suid=" + user.getSaloonId() + "&a_time=" + timeTosend + "&a_date=" + dateToSEnd + "&message=" + message + "&booking_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getBookings();
                            }
                        });
                    } else {

                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        });

    }


    private void declineRequest(final String id) {
        makeRequest(true, WebServices.DeclineBookingBooking, "booking_id=" + id + "&user_id=" + user.getUserId(), new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getBookings();
                            }
                        });
                    } else {

                        showInformationDialog(message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });

    }

    private void acceptRequest(final String id) {
        makeRequest(true, WebServices.AcceptRequestBookind, "uid=" + user.getUserId() + "&suid=" + user.getSaloonId() + "&booking_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getBookings();

                            }
                        });
                    } else {
                        showInformationDialog(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        });
    }
}

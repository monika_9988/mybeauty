package com.parlour.bookmyparlour.classes.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.parlour.bookmyparlour.R;
import com.parlour.bookmyparlour.utils.config.Constants;
import com.parlour.bookmyparlour.utils.config.WebServices;
import com.parlour.bookmyparlour.generic.BaseActivity;
import com.parlour.bookmyparlour.utils.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.android.volley.toolbox.HttpHeaderParser.parseCacheHeaders;

/**
 * Created by HP on 16-12-2016.
 */
public class Forgetpassword extends BaseActivity {
    EditText _email;
    TextView _reset;
    private StringRequest jsonArrayRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpass);
        getSupportActionBar().hide();
        _email = (EditText) findViewById(R.id.forget_email);
        _reset = (TextView) findViewById(R.id.btn_forgetpass);

        _reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailStr = _email.getText().toString().trim();

                if (emailStr.isEmpty()) {
                    _email.setError(getString(R.string.error_empty_field));
                    _email.requestFocus();
                    return;
                }
                if (!Constants.Validator.isValidEmail(emailStr)) {
                    _email.setError(getString(R.string.email_not_valid));
                    _email.requestFocus();

                    return;
                }
                if (Constants.Validator.isNetworkConnected(Forgetpassword.this)) {
                    ForgetPasswordprocess(emailStr);
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
    }

    private StringRequest ForgetPasswordprocess(String email) {

showLoading();
        jsonArrayRequest = new StringRequest(WebServices.ForgetPassword + "email=" + email, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideLoading();

                Log.d("login response", "---  " + response);
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {


                        showOptionDialog(message, new Helper.OnChoiceListener() {
                            @Override
                            public void onChoose(boolean isPositive) {
                                if (isPositive) {
                                    Intent intent = new Intent(Forgetpassword.this, Login_register.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                    finish();

                                }
                            }
                        });
                    } else {
                        showInformationDialog(message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoading();

                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, parseCacheHeaders(response));
            }

        };  jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        reqDetail.add(jsonArrayRequest);
        return jsonArrayRequest;

    }
}
package com.parlour.adminparlour.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parlour.adminparlour.Models.BookingRequestModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class BookingAdapterr extends GenericAdapter<BookingRequestModel> {

    private final ArrayList<? extends BookingRequestModel> itemList;


    public BookingAdapterr(Context context, ArrayList<? extends BookingRequestModel> itemList) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;


    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, message, aceept, decline,datetime,newtime,status;
        LinearLayout linearLayout;
        private ViewHolder(View itemView) {
            super(itemView);


            cname = (TextView) itemView.findViewById(R.id.boo_name);
            message = (TextView) itemView.findViewById(R.id.boo_messgae);
            datetime = (TextView) itemView.findViewById(R.id.boo_time);
           // status = (TextView) itemView.findViewById(R.id.boo_status);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final BookingRequestModel adminServiceModel) {

            cname.setText(adminServiceModel.getName());
            message.setText("Today's Bookings: "+adminServiceModel.getToday_booking());
//            utils.getFormattedDate() -use if we need to change date format
            datetime.setText("Total Bookings: "+adminServiceModel.getTot_booking());
//            if (adminServiceModel.getStatus().equals("0")){
//                status.setText("Status: "+"Request is Pending");
//
//            }else{
//                //0 pending
//                //1 ok
//                //2 new time
//                //3 cancel
//                // 4 confirmed
//                if (adminServiceModel.getStatus().equals("4")){
//                    status.setText("Status: "+"Confirmed");
//                }else if (adminServiceModel.getStatus().equals("2")){
//                    status.setText("Status: "+"New Date-Time Suggested");
//                }else if (adminServiceModel.getStatus().equals("3")){
//                    status.setText("Status: "+"Booking Cancelled");
//                }else if (adminServiceModel.getStatus().equals("1")){
//                    status.setText("Status: "+"Booking Approved");
//                }
//            }



        }
    }



}

package com.parlour.adminparlour.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parlour.adminparlour.Models.RequestModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.GenericAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 */
public class RequestAdapterr extends GenericAdapter<RequestModel> {

    private final ArrayList<? extends RequestModel> itemList;

    onViewMoreListner onViewMore;
    public RequestAdapterr(Context context, ArrayList<? extends RequestModel> itemList, onViewMoreListner onViewMore){
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.onViewMore= onViewMore;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, cderc;
        TextView approve, delete,_viewMore;
        ImageView image;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.rowser_name);
            cderc = (TextView) itemView.findViewById(R.id.derc);
            _viewMore = (TextView) itemView.findViewById(R.id.view_more);

            image = (ImageView) itemView.findViewById(R.id.profile_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final RequestModel catlistingmodel) {
            cname.setText(utils.capFirstChar(catlistingmodel.getName()));
            cderc.setText(catlistingmodel.getDerc());
            Double aDouble = utils.getdistFrom(Double.parseDouble(catlistingmodel.getLatitude()), Double.parseDouble(catlistingmodel.getLongitude()), user.getUserLocation().getLatitude(), user.getUserLocation().getLongitude(), "km");
            final String distance=String.valueOf(aDouble) + " kms away";
            Picasso.with(context)
                    .load(WebServices.DomainNameImage+catlistingmodel.getImage())
                    .placeholder(android.R.drawable.ic_dialog_info)
                    .error(android.R.drawable.ic_dialog_info).into(image);
            _viewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onViewMore.onview(catlistingmodel.getName(),catlistingmodel.getId(),catlistingmodel.getImage(),catlistingmodel.getDerc(),catlistingmodel.getEmail(),catlistingmodel.getAddressline1(),catlistingmodel.getAddressline2(),catlistingmodel.getCity(),catlistingmodel.getLandline(),catlistingmodel.getPhone(),catlistingmodel.getListing_status(),catlistingmodel.getListing_type(),catlistingmodel.getStart_time(),catlistingmodel.getEnd_time(),catlistingmodel.getWork_days(),distance);
                }
            });

        }
    }



    public interface onViewMoreListner{
        void onview(String catlistingmodelName, String catlistingmodelId,String image,String details,String email,String addressline1,String addressline2, String city, String landline, String phone, String listing_status, String listing_type, String start_time, String end_time, String work_days,String distance);
    }    }
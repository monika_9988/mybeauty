package com.parlour.adminparlour.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parlour.adminparlour.Models.ServiceModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class ServiceAdapterr extends GenericAdapter<ServiceModel> {

    private final ArrayList<? extends ServiceModel> itemList;
    ondeleteListener ondeletelistener;
    onapproveListener oneditlistener;

    public ServiceAdapterr(Context context, ArrayList<? extends ServiceModel> itemList, ondeleteListener ondeletelistener, onapproveListener oneditlistener) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.ondeletelistener = ondeletelistener;
        this.oneditlistener = oneditlistener;

    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, cderc;
        ImageView _delete,_edit;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.rowser_name);
            cderc = (TextView) itemView.findViewById(R.id.derc);
            _delete = (ImageView) itemView.findViewById(R.id.delete);
            _edit = (ImageView) itemView.findViewById(R.id.edit);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final ServiceModel catlistingmodel) {

            cname.setText(utils.capFirstChar(catlistingmodel.getName()));


            _edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    oneditlistener.onapprove(catlistingmodel.getName(), catlistingmodel.getId());
                }
            });
            _delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ondeletelistener.ondelete(catlistingmodel.getId());

                }
            });

        }
    }


    public interface ondeleteListener {
        void ondelete(String sid);
    }

    public interface onapproveListener {
        void onapprove(String name, String id);
    }
}

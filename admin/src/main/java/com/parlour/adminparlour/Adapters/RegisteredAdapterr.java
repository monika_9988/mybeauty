package com.parlour.adminparlour.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parlour.adminparlour.Models.registeredUserModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class RegisteredAdapterr extends GenericAdapter<registeredUserModel> {

    private final ArrayList<? extends registeredUserModel> itemList;


    public RegisteredAdapterr(Context context, ArrayList<? extends registeredUserModel> itemList) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;


    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname,email, cderc,phone;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.rowser_name);
            email = (TextView) itemView.findViewById(R.id.rowser_email);
            phone = (TextView) itemView.findViewById(R.id.phone);
            cderc = (TextView) itemView.findViewById(R.id.derc);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final registeredUserModel catlistingmodel) {

            cname.setText(catlistingmodel.getName());
            email.setText(catlistingmodel.getEmail());
            phone.setText("Ph: "+catlistingmodel.getPhone());
            if(((catlistingmodel.getDerc()).equals(""))||(catlistingmodel.getDerc()).equals("null"))
            {
                cderc.setVisibility(View.GONE);

            }
            else {
                cderc.setText("Profession : "+catlistingmodel.getDerc());


            }
           }

    }



}

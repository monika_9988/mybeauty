package com.parlour.adminparlour.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parlour.adminparlour.Models.BookingModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.GenericAdapter;

import java.util.ArrayList;

/**
 */
public class ApprovedAdapterr extends GenericAdapter<BookingModel> {

    private final ArrayList<? extends BookingModel> itemList;
    ondeleteListener ondeletelistener;
    onapproveListener oneditlistener;
    onUpdate onUpdate;
    onImageClick onImageClick;

    public ApprovedAdapterr(Context context, ArrayList<? extends BookingModel> itemList, ondeleteListener ondeletelistener, onapproveListener oneditlistener, onUpdate onUpdate,onImageClick onImageClick) {
        super(context, itemList);
        this.itemList = itemList;
        this.context = context;
        this.ondeletelistener = ondeletelistener;
        this.oneditlistener = oneditlistener;
        this.onUpdate = onUpdate;
        this.onImageClick = onImageClick;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId) {
        View v = LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    protected void onBindView(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBindHolder(itemList.get(position));
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView cname, cderc, _enable, _disable, phone, updatetime;
        ImageView image;

        private ViewHolder(View itemView) {
            super(itemView);

            cname = (TextView) itemView.findViewById(R.id.rowser_name);
            cderc = (TextView) itemView.findViewById(R.id.derc);
            phone = (TextView) itemView.findViewById(R.id.phone);
            updatetime = (TextView) itemView.findViewById(R.id.update_time);

            image = (ImageView) itemView.findViewById(R.id.profile_image);
            _enable = (TextView) itemView.findViewById(R.id.enable);
            _disable = (TextView) itemView.findViewById(R.id.disable);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(getLayoutPosition(), itemList.get(getLayoutPosition()));
                }
            });
        }

        public void onBindHolder(final BookingModel catlistingmodel) {

            cname.setText(utils.capFirstChar(catlistingmodel.getName()));
            cderc.setText("Email: " + catlistingmodel.getEmail());
            phone.setText("Tel: " + catlistingmodel.getLandline() + " " + "\n" + "Mob: " + catlistingmodel.getPhone());
            final String fullUrl = WebServices.DomainNameImage +catlistingmodel.getImage();

            utils.setImagePicass(fullUrl,context,image);
//            Picasso.with(context)
//                    .load(WebServices.DomainNameImage + catlistingmodel.getImage())
//                    .placeholder(android.R.drawable.ic_dialog_info)
//                    .error(android.R.drawable.ic_dialog_info).into(image);
            String status = catlistingmodel.getStatus();
            if (status.equals("0")) {
                _disable.setVisibility(View.VISIBLE);
                _enable.setVisibility(View.GONE);
            } else if (status.equals("1")) {
                _disable.setVisibility(View.GONE);
                _enable.setVisibility(View.VISIBLE);
            }
            _enable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    oneditlistener.onapprove(catlistingmodel.getId(), 0);
                }
            });
            _disable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ondeletelistener.ondelete(catlistingmodel.getId(), 1);

                }
            });
            updatetime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onUpdate.onupd(catlistingmodel);
                }
            });
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImageClick.onimageclick(fullUrl);
                }
            });

        }
    }

    public interface ondeleteListener {
        void ondelete(String sid, int i);
    }

    public interface onUpdate {
        void onupd(BookingModel catlistingmodel);
    }

    public interface onapproveListener {
        void onapprove(String id, int i);
    }
    public interface onImageClick {
        void onimageclick(String imgurl);
    }


}

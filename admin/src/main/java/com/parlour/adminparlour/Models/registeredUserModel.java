package com.parlour.adminparlour.Models;

import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.ViewModel;

public class registeredUserModel implements ViewModel {

    private String id;
    private String name,derc,image,email,phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int layoutId() {
        return R.layout.register_user_items;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

    public String getDerc() {
        return derc;
    }

    public void setDerc(String derc) {
        this.derc = derc;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
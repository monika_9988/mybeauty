package com.parlour.adminparlour.Models;

import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.ViewModel;

/**
 * Created by CORE i5 on 08/26/17.
 */

public class BookingRequestModel implements ViewModel{


    private String id;
    private String name,message,dateTime,status,tot_booking,today_booking;

    public void setToday_booking(String today_booking) {
        this.today_booking = today_booking;
    }

    public String getToday_booking() {
        return today_booking;
    }

    public void setTot_booking(String tot_booking) {
        this.tot_booking = tot_booking;
    }

    public String getTot_booking() {
        return tot_booking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.rowitem_allbookings;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

}
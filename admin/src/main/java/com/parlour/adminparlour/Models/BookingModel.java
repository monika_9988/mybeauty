package com.parlour.adminparlour.Models;

import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.ViewModel;

public class BookingModel implements ViewModel {

    private String id;
    private String name,derc,image,status,email,phone,landline,starttime,endtime,position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEndtime() {
        return endtime;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLandline() {
        return landline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.rowitem_booking;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

    public String getDerc() {
        return derc;
    }

    public void setDerc(String derc) {
        this.derc = derc;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
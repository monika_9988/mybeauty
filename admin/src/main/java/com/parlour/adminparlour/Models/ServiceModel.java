package com.parlour.adminparlour.Models;

import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.ViewModel;

public class ServiceModel implements ViewModel {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int layoutId() {
        return R.layout.rowitem_services;
    }

    @Override
    public String emptyTitle() {
        return null;
    }


}
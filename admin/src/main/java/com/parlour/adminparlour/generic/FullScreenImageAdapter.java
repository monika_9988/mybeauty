package com.parlour.adminparlour.generic;//package macrew.teamnewstart.generic;
//
//import android.app.Activity;
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.LinearLayout;
//
//import com.nostra13.universalimageloader.core.DisplayImageOptions;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.assist.ImageScaleType;
//import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
//import com.reviewbyme.review.R;
//import com.reviewbyme.utils.Utils;
//
//import java.util.ArrayList;
//
//
//public class FullScreenImageAdapter extends PagerAdapter {
//
//    private final int scheight;
//    private final int scwidth;
//    Utils utils;
//    private Activity _activity;
//    private ArrayList<String> _imagePaths;
//    private LayoutInflater inflater;
//    private ArrayList<Uri> list;
//    private DisplayImageOptions options;
//    ImageLoader imageLoader;
//
//    // constructor
//    public FullScreenImageAdapter(Activity activity,
//                                  ArrayList<String> arrayList) {
//        this._activity = activity;
//        this._imagePaths = arrayList;
//
//
//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        _activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        scheight = displaymetrics.heightPixels;
//        scwidth = displaymetrics.widthPixels;
//        utils = Utils.getInstance();
//        imageLoader = ImageLoader.getInstance();
//    }
//
//    @Override
//    public int getCount() {
//        return this._imagePaths.size();
//    }
//
//    @Override
//    public boolean isViewFromObject(View view, Object object) {
//        return view == ((LinearLayout) object);
//    }
//
//    @Override
//    public Object instantiateItem(ViewGroup container, final int position) {
//        TouchImageView imgDisplay;
//        Button btnClose;
//
//        inflater = (LayoutInflater) _activity
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
//                false);
//
//        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
//
//        imgDisplay.setTag(position);
//        /*BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inSampleSize = 2;*/
//        options = new DisplayImageOptions.Builder()
//                .resetViewBeforeLoading(true)
//                .cacheOnDisk(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .considerExifParams(true)
//                .displayer(new FadeInBitmapDisplayer(300))
//                .build();
////        Bitmap bitmap = BitmapFactory.decodeFile(Dataset.Grid_Image_path.get(position), options);
////        Log.d("path click in adapter", "full screen adapter" + position);
////        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,
////                scwidth, scheight, true);
////        imgDisplay.setImageBitmap(scaledBitmap);
//
//        Log.d("images", String.valueOf(_imagePaths.get(position)));
//        imageLoader.displayImage(_imagePaths.get(position), imgDisplay);
//
//        // close button click event
//
//        ((ViewPager) container).addView(viewLayout);
//        return viewLayout;
//    }
//
//    @Override
//    public void destroyItem(View container, int position, Object object) {
//        ((ViewPager) container).removeView((View) object);
//    }
//
//
//}

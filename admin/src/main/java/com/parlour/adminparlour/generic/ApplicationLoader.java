package com.parlour.adminparlour.generic;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.parlour.adminparlour.utils.User;
import com.parlour.adminparlour.utils.Utils;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Macrew-PC-24 on 21-11-16.
 */

public class ApplicationLoader extends Application {
    public static volatile ApplicationLoader applicationContext;
    private RequestQueue reqDetail;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        applicationContext = this;

        User.init(getApplicationContext());
        //OpenRequest.init(getApplicationContext());
        reqDetail = Volley.newRequestQueue(getApplicationContext());
        Utils.init(getApplicationContext());
//        getImageLoaderInstance(this);
    }

//    public ImageLoader getImageLoaderInstance(Context context) {
//        // UNIVERSAL IMAGE LOADER SETUP
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.no_image_found) // resource or drawable
//                .showImageForEmptyUri(R.drawable.no_image_found) // resource or drawable
//                .showImageOnFail(R.drawable.no_image_found)
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
//                .cacheOnDisc(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .considerExifParams(true) // default
//                .displayer(new FadeInBitmapDisplayer(0)).build();
//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
//                .defaultDisplayImageOptions(defaultOptions)
//                .memoryCache(new WeakMemoryCache())
//                .discCacheSize(100 * 1024 * 1024).build();
//        ImageLoader.getInstance().init(config);
//        return ImageLoader.getInstance();
//    }
}

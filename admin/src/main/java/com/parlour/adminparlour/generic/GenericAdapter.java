package com.parlour.adminparlour.generic;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.utils.User;
import com.parlour.adminparlour.utils.Utils;

import java.util.ArrayList;


public abstract class GenericAdapter<T extends ViewModel> extends RecyclerView.Adapter {
    public Context context;
    public User user;
    public Utils utils;
    private ArrayList<? extends ViewModel> itemList;
    private OnLoadMoreListener loadMoreListener;
    private boolean isLoading = false, isMoreDataAvailable = true;
    public OnItemClickListener<T> onItemClickListener;

    public void updateList(ArrayList<? extends T> itemList) {
        this.itemList = itemList;
    }

    public GenericAdapter(Context context, ArrayList<? extends ViewModel> itemList) {
        this.context = context;
        user = User.getInstance();
        utils = Utils.getInstance();
        this.itemList = itemList;
    }

    @Override
    public int getItemCount() {
        if (itemList != null) {
            return itemList.size();
        }
        return 0;
    }


    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).layoutId();
    }


    private void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ArrayList<? extends ViewModel> getItemList() {
        return itemList;
    }


    public interface OnItemClickListener<T> {
        void onItemClick(int position, T item);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.Layouts.error) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.error_item, parent, false);
            return new ErrorViewHolder(v);
        } else if (viewType == Constants.Layouts.progressLayout) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            return new ProgressViewHolder(v);
        } else if (viewType == Constants.Layouts.emptyLayout) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item, parent, false);
            return new EmptyViewHolder(v);
        }

        return onCreateView(context, parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }
        if (holder instanceof EmptyViewHolder) {
            ((EmptyViewHolder) holder).onBindView(itemList.get(position));
        } else {
            onBindView(holder, position);
        }
    }

    protected abstract RecyclerView.ViewHolder onCreateView(Context context, ViewGroup viewGroup, int layoutId);

    protected abstract void onBindView(RecyclerView.ViewHolder holder, int position);

    public Context getContext() {
        return context;
    }

    /* notifyDataSetChanged is final method so we can't override it
               call adapter.notifyDataChanged(); after update the list
               */
    public void notifyRefreshDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        private ProgressViewHolder(View v) {
            super(v);
        }
    }

    private class EmptyViewHolder<VH> extends RecyclerView.ViewHolder {
        private final TextView itemTitle;

        private EmptyViewHolder(View v) {
            super(v);
            itemTitle = (TextView) v.findViewById(R.id.title_empty);

        }

        private void onBindView(T t) {
            if (t.emptyTitle() != null) {
                itemTitle.setText(t.emptyTitle());
            }
        }

    }

    private class ErrorViewHolder extends RecyclerView.ViewHolder {
        private ErrorViewHolder(View v) {
            super(v);
        }
    }
}
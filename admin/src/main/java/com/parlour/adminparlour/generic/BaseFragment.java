package com.parlour.adminparlour.generic;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.utils.Helper;
import com.parlour.adminparlour.utils.User;
import com.parlour.adminparlour.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.android.volley.toolbox.HttpHeaderParser.parseCacheHeaders;


/**
 * Created by Gurvinder rajpal on 29-11-16.
 */
public abstract class BaseFragment extends Fragment {
    private ProgressDialog dialog;
    public User user;
    public Utils utils;
    public RequestQueue reqDetail;
    public ImageLoader imageLoader;
    Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = User.getInstance();
        utils = Utils.getInstance();
        reqDetail = Volley.newRequestQueue(getActivity());
        imageLoader = ImageLoader.getInstance();

    }

    public void hideLoading() {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLoading() {
        try {
            if (dialog == null)
                dialog = new ProgressDialog(getActivity());
            dialog.setCancelable(false);
            dialog.setMessage("Loading...");
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showOptionDialog(String message, final Helper.OnChoiceListener onChoiceListener) {
        showOptionDialog(message, null, null, onChoiceListener);
    }

    public void showInformationDialog(String information) {
        showInformationDialog(information, null);
    }

    public void showOptionDialog(String message, String optionPositive, String optionNegative, final Helper.OnChoiceListener onChoiceListener) {
        try {
            // showOptionDialog(message,optionPositive,optionNegative);
            if (optionPositive == null) {
                optionPositive = getString(android.R.string.yes);
            }
            if (optionNegative == null) {
                optionNegative = getString(android.R.string.no);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(message);
            builder.setPositiveButton(optionPositive, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    onChoiceListener.onChoose(true);
                }
            });
            builder.setNegativeButton(optionNegative, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    onChoiceListener.onChoose(false);
                }
            });
            // Create the AlertDialog object and return it
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }

    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void showInformationDialog(String information, final Helper.OnButtonClickListener clickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(information);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (clickListener != null) {
                    clickListener.onClick();
                }
            }
        });
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    public String makeRequestPost(final String url, final JSONObject parameters, final onRequestComplete onRequestComplete) {
        final String[] responseData = {""};
        showLoading();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject result) {
                hideLoading();
                responseData[0] = result.toString();
                Log.d("response = url ", "  ---  " + url);
                Log.d("response = param", "  ---  " + parameters);
                Log.d("response = data", "  ---  " + result.toString());
                onRequestComplete.onComplete(result.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getActivity(),"" + message,Toast.LENGTH_SHORT).show();                hideLoading();


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.MY_SOCKET_TIMEOUT_MS
                , Constants.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        reqDetail.add(jsonObjectRequest);

        return responseData[0];
    }

    public String makeRequest(final boolean b, final String url, final String parameters, final onRequestComplete onRequestComplete) {
        final String[] responseData = {""};
        if (b) {
            showLoading();
        }
        StringRequest jsonArrayRequest = new StringRequest(url + parameters, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (b) {
                    hideLoading();
                }
                responseData[0] = response;

                Log.d("response = url ", "  ---  " + url);
                Log.d("response = param", "  ---  " + parameters);
                Log.d("response = data", "  ---  " + response);
                onRequestComplete.onComplete(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(),"" + message,Toast.LENGTH_SHORT).show();
                        if (b) {
                            hideLoading();
                        }
                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, parseCacheHeaders(response));
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                Constants.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jsonArrayRequest);
        return responseData[0];
    }

    public interface onRequestComplete {
        public void onComplete(String response);
    }

}
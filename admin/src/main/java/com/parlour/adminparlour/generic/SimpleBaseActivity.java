package com.parlour.adminparlour.generic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.parlour.adminparlour.utils.Helper;
import com.parlour.adminparlour.utils.User;
import com.parlour.adminparlour.utils.Utils;

import java.util.List;


public abstract class SimpleBaseActivity extends AppCompatActivity {
    public User user;
    public Utils utils;
    private ProgressDialog dialog;

    private static final int REQUEST_APP_SETTINGS = 1212;
    public RequestQueue reqDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        utils = Utils.getInstance();
        user = User.getInstance();
        reqDetail = Volley.newRequestQueue(getApplicationContext());




    }

    protected abstract void onDataChanged();


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }

    }
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    protected abstract void onNetworkChange(boolean isConnected);

    public String getClassName(Class aClass) {
        return aClass.getSimpleName();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected abstract void onShutDownCalled(List<String> stringArrayExtra);

    protected abstract void onActivityRefresh();

    public void refreshActivity() {
        onActivityRefresh();
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    public void showLoading() {
        try {
            if (dialog == null)
                dialog = new ProgressDialog(SimpleBaseActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage("Loading");
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoading() {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showInformationDialog(String information) {
        showInformationDialog(information, null, null);
    }

    public void showInformationDialog(String information, String buttonText, final Helper.OnButtonClickListener clickListener) {

        try {
            if (buttonText == null) {
                buttonText = getString(android.R.string.ok);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(SimpleBaseActivity.this);
            builder.setMessage(information);
            builder.setCancelable(false);
            builder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    if (clickListener != null) {
                        clickListener.onClick();
                    }
                }
            });
            // Create the AlertDialog object and return it
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showInformationDialog(String information, final Helper.OnButtonClickListener clickListener) {
        showInformationDialog(information, null, clickListener);
    }

    public static void openPermissionScreen(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivityForResult(i, REQUEST_APP_SETTINGS);

    }

    public void showOptionDialog(String message, String optionPositive, String optionNegative, final Helper.OnChoiceListener onChoiceListener) {
        try {
            // showOptionDialog(message,optionPositive,optionNegative);
            if (optionPositive == null) {
                optionPositive = getString(android.R.string.yes);
            }
            if (optionNegative == null) {
                optionNegative = getString(android.R.string.no);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(SimpleBaseActivity.this);
            builder.setMessage(message);
            builder.setPositiveButton(optionPositive, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    onChoiceListener.onChoose(true);
                }
            });
            builder.setNegativeButton(optionNegative, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    onChoiceListener.onChoose(false);
                }
            });
            // Create the AlertDialog object and return it
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showOptionDialog(String message, final Helper.OnChoiceListener onChoiceListener) {
        showOptionDialog(message, null, null, onChoiceListener);
    }

    public String removeLastComma(String s) {
        if (s != null && !s.isEmpty()) {
            String last = s.substring(s.length() - 1, s.length());
            if (last.equalsIgnoreCase(",")) {
                return s.substring(0, s.length() - 1);
            }
            return s;
        } else return "";
    }


}

package com.parlour.adminparlour.generic;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.parlour.adminparlour.config.Constants;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static com.android.volley.toolbox.HttpHeaderParser.parseCacheHeaders;

public class BaseActivity extends SimpleBaseActivity {
    onRequestComplete onRequestComplete;

    @Override
    protected void onDataChanged() {

    }

    @Override
    protected void onNetworkChange(boolean isConnected) {

    }

    @Override
    protected void onShutDownCalled(List<String> stringArrayExtra) {

    }

    @Override
    protected void onActivityRefresh() {

    }

    public String makeRequest(final boolean b, final String url, final String parameters, final onRequestComplete onRequestComplete) {
        final String[] responseData = {""};
if (b){
    showLoading();
}
        StringRequest jsonArrayRequest = new StringRequest(url + parameters, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (b){
                    hideLoading();
                }
                responseData[0] = response;
                Log.d("response = url ", "  ---  " + url);
                Log.d("response = param", "  ---  " + parameters);
                Log.d("response = data", "  ---  " + response);
                onRequestComplete.onComplete(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (b){
                            hideLoading();
                        }
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(),"" + message,Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, parseCacheHeaders(response));
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                Constants.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        reqDetail.add(jsonArrayRequest);
        return responseData[0];
    }


    public String makeRequestPost(final String url, final JSONObject parameters, final onRequestComplete onRequestComplete) {
        final String[] responseData = {""};
        showLoading();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject result) {
                hideLoading();
                responseData[0] = result.toString();
                Log.d("response = url ", "  ---  " + url);
                Log.d("response = param", "  ---  " + parameters);
                Log.d("response = data", "  ---  " + result.toString());
                onRequestComplete.onComplete(result.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplicationContext(),"" + message,Toast.LENGTH_SHORT).show();                hideLoading();


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.MY_SOCKET_TIMEOUT_MS
                , Constants.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);

        return responseData[0];
    }

    public interface onRequestComplete {
        public void onComplete(String response);
    }

}

package com.parlour.adminparlour.config;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Constants {


    public static final boolean DEBUG = true;
    public static final float MAP_ZOOM_LEVEL = 16;
    public static final long REST_TIME_BETWEEN_PROGRAMS = 30;// in secs
    public static final long MESSAGE_RECEIVE_SERVICE_DELAY = 10 * 1000;//10-secs
    public static final int MAX_DAY_COUNT = 6;
    public static final int MAX_COMPARE_IMAGES = 2;
    public static final String name = "_username";
    public static final String Email = "email";
    public static final String Password = "password";
    public static final String ischecked = "isc";
    public static final String clientModelExtra = "_clientModelExtra";
    public static final int REQCODE = 100;

    public static final String workoutModelExtra = "_workoutModelExtra";
    public static final String planIdExtra = "_planIdExtra";
    public static final String planModelExtra = "_planModelExtra";
    public static final String programModelListExtra = "_programModelListExtra";
    public static final String weekModelExtra = "_weekModelExtra";
    public static final String programModelExtra = "_programModelExtra";
    public static final String videoIdExtra = "_videoIdExtra";
    public static final String imagesArrayExtra = "_imagesArrayExtra";
    public static final String canUploadImages = "_canUploadImages";
    public static final String MIN_PER_SET= " mins/set";
    public static final String SEC_PER_SET = " secs/set";
    public static final float MAX_WEIGHT_CHART = 50;
    public static final String DOCUMENT_BASE_URL = "https://docs.google.com/viewer?url=";
    public static final String CONTACT_EMAIL = "info@teamnewstart.com";
    public static final String Ischecked = "ischecked";
    public static final int DEFAULT_MAX_RETRIES =0 ;
    public static int MY_SOCKET_TIMEOUT_MS=2000;
//    public static final String DOCUMENT_BASE_URL = "https://drive.google.com/viewerng/viewer?embedded=true&url=";

    public static class Validator {
        public static boolean isValidEmail(String emailInput) {
            String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(emailInput);
            return matcher.matches();
        }

        public static boolean isNetworkConnected(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        }

        public static boolean isValidUsername(String s) {
            return Pattern.compile("^[a-z0-9_-]{3,15}$").matcher(s).matches();
        }
    }

    public static class Unit {
        public static final String Mile = "mile";
        public static final String Meter = "m";
        public static final String Km = "km";
    }

    public static class Formatter {
        public static final String dateTime = "yyyy-MM-dd HH:mm:ss";
        public static final String dateTimeDotNet = "yyyy-MM-dd'T'hh:mm:ss";
        public static final String time = "HH:mm:ss";
        public static final String date = "yyyy-MM-dd";
        public static final String ProgressPhotos = "d MMM-yyyy";
    }

    public static class TimeZone {
        public static final String UTC = "UTC";
        public static final String GMT = "GMT";

    }


    public static class Layouts {
        public static final int error = -1;
        public static final int progressLayout = -2;
        public static final int emptyLayout = -3;
//        public static final int ChatItem= R.layout.row_item_chat;
    }


    public static class IntentFilter {
        public static final String Activity_Finish = "Activity_Finish";
        public static final String NOTIFY_DATA_CHANGED = "_NOTIFY_DATA_CHANGED";
    }
}

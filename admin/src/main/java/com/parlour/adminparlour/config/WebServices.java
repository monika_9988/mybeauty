package com.parlour.adminparlour.config;


public class WebServices {
    public static final String DomainName = "http://www.magnaopussoftware.in/parlour/";
    public static final String DomainNameImage = "http://www.magnaopussoftware.in/parlour/images/";
//    public static final String DomainName = "http://10.0.2.2/parlourApp/";
//    public static final String DomainNameImage = "http://10.0.2.2/parlourApp/images/";
    public static final String RequestList = DomainName + "getListingRequests.php?";
    public static final String ApproveReq = DomainName + "approverequest.php?";
    public static final String DeclineReq = DomainName + "declinerequest.php?";
    public static final String AddService = DomainName + "addservice.php?";
    public static final String GetServices = DomainName + "getAllServices.php?";
    public static final String UPdateservice = DomainName + "updateservice.php?";
    public static final String DeleteServices = DomainName + "deleteservice.php?";
   // public static final String AllBookings = DomainName + "getAllBookings.php?";
    public static final String getBookingNumber = DomainName + "getBookingNumber.php";

    public static final String AllAprrovedList = DomainName + "getapprovedsaloonlist.php?";
    public static final String AllREgisteredUsers = DomainName + "getregisteredusers.php";
    public static final String EnableDisableparlour = DomainName + "enableDisableParlour.php?";

    public static final String ImageURl = DomainNameImage;
    public static final String LoginAdmin = DomainName + "loginAdmin.php?";
    public static final String ForgetPassword = DomainName + "forgot_password";
    public static final String add_client = DomainName + "add_client";
    public static final String get_goals = DomainName + "get_goals";
    public static final String SendEmail = DomainName + "sendEmail.php?";
    public static final String SendNotification = DomainName + "sendnotification.php?";
    public static final String UPdate = DomainName + "updateListingDate.php?";


    public static final String DeviceRegister=DomainName+"adminDeviceRegister.php?";
}

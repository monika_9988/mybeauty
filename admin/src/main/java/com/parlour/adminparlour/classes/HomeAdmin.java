package com.parlour.adminparlour.classes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.parlour.adminparlour.Adapters.ApprovedAdapterr;
import com.parlour.adminparlour.LocationAwareActivity;
import com.parlour.adminparlour.Models.BookingModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.utils.Helper;
import com.parlour.adminparlour.utils.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static com.android.volley.toolbox.HttpHeaderParser.parseCacheHeaders;
import static com.parlour.adminparlour.config.WebServices.AddService;

public class HomeAdmin extends LocationAwareActivity {
    TextView _btnRequest, _btnAddservice, _btnAllServices, btnAllbookin, btnRegisterusers, btnApproved, adminmessage;
    private Location lastLocation;
    private double longitude;
    private double latitude;
    ArrayList<BookingModel> requestModelArrayList = new ArrayList<>();
    private ApprovedAdapterr requestAdapter;
    private StringRequest jsonArrayRequest;
    int selecteditem;
    private String subService_ID = "0";
    private String selectedIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getSupportActionBar().setIcon(android.R.drawable.ic_lock_power_off);
        setContentView(R.layout.activity_home_admin);
        _btnRequest = (TextView) findViewById(R.id.req_list);
        _btnAddservice = (TextView) findViewById(R.id.add_service);
        _btnAllServices = (TextView) findViewById(R.id.allservices);
        btnAllbookin = (TextView) findViewById(R.id.allbooking);
        btnRegisterusers = (TextView) findViewById(R.id.registeresuser);
        adminmessage = (TextView) findViewById(R.id.admin_message);
        btnApproved = (TextView) findViewById(R.id.parlourlist);
        String bmp = "Book My Parlour ";
        adminmessage.setText("Welcome " + utils.capFirstChar(user.getName()) + " to \n " + bmp);
        _btnAllServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAdmin.this, AllServices.class));

            }
        });
        _btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAdmin.this, RequestList.class));
            }
        });
        _btnAddservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddDialog();
            }
        });

        btnAllbookin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAdmin.this, AllBookingList.class));

            }
        });
        btnRegisterusers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAdmin.this, AllRegUsers.class));

            }
        });
        btnApproved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAdmin.this, AllAproved.class));

            }
        });
        if (Constants.Validator.isNetworkConnected(HomeAdmin.this)) {
            getApprovedList();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }

    private void getApprovedList() {
makeRequest(true, WebServices.AllAprrovedList, "", new onRequestComplete() {
    @Override
    public void onComplete(String response) {
        BookingModel requestModel = null;
        JSONArray jsonArray = null;
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("result");

            String status = jsonObject1.getString("status");
            String message = jsonObject1.getString("message");
            requestModelArrayList.clear();

            if (status.equalsIgnoreCase("valid")) {
                jsonArray = jsonObject1.getJSONArray("resarray");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject json = null;
                    try {
                        requestModel = new BookingModel();
                        json = jsonArray.getJSONObject(i);
                        requestModel.setId(json.getString("saloon_id"));
                        requestModel.setName(json.getString("Title"));
                        requestModel.setEmail(json.getString("email"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    requestModelArrayList.add(requestModel);
                }

            } else {
                showInformationDialog(message, new Helper.OnButtonClickListener() {
                    @Override
                    public void onClick() {
                        finish();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
});

    }

    @Override
    protected void onDataChanged() {

    }

    @Override
    protected void onShutDownCalled(List<String> stringArrayExtra) {

    }

    @Override
    protected void onUserLocationChanged(Location location) {
        lastLocation = location;
        user.getAddressFromLocation(location.getLatitude(), location.getLongitude(), new Helper.OnRequestCompleteListener<String>() {

            @Override
            public void onComplete(String object) {
                Log.d("location", "" + lastLocation);
                Log.d("location anme", "" + object);
                user.saveUserLocation(lastLocation);
                latitude = TextUtils.getDouble(String.valueOf(user.getUserLocation().getLatitude()));
                longitude = TextUtils.getDouble(String.valueOf(user.getUserLocation().getLongitude()));
            }
        });

        stopLocation();
    }

    private void openAddDialog() {

        final Dialog dialog = new Dialog(HomeAdmin.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_addservice);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final EditText text = (EditText) dialog.findViewById(R.id.edt_dia);
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = text.getText().toString();
                if (des.isEmpty()) {
                    text.setError(getString(R.string.error_empty_field));
                    text.requestFocus();
                    return;
                }
                if (Constants.Validator.isNetworkConnected(HomeAdmin.this)) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    hideSoftKeyboard();
                    addService(des);

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu); //your file name
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_refresh:
                showOptionDialog("Do you want to Logout ?", new Helper.OnChoiceListener() {
                    @Override
                    public void onChoose(boolean isPositive) {
                        if (isPositive) {
                            user.logout();
                            Intent intent = new Intent(getApplicationContext(), Login_register.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            // goHome();


                        }
                    }
                });


                return true;
            case R.id.send_email:
                selectSaloon();
                return true;
            case R.id.send_notification:
                SendMessageNotification();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void selectSaloon() {
        ArrayList<BookingModel> listing = new ArrayList<>();
        ArrayList<String> listing1 = new ArrayList<String>();
        for (int i = 0; i < requestModelArrayList.size(); i++) {
            String name = requestModelArrayList.get(i).getName();
            listing.add(requestModelArrayList.get(i));
            listing1.add(name);

        }

        final boolean[] sel1 = new boolean[requestModelArrayList.size()];

        CharSequence[] dayList = listing1.toArray(new CharSequence[listing.size()]);

        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setTitle("Select Parlours");
        alt_bld.setMultiChoiceItems(dayList, sel1,
                new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which, boolean isChecked) {
                        selecteditem = which;


                    }
                }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ListView list = ((AlertDialog) dialog).getListView();
//                Integer selected = (Integer)list.getItemAtPosition();
                String s = (String) list.getAdapter().getItem(selecteditem);
                BookingModel checkedItem = requestModelArrayList.get(selecteditem);
                subService_ID = checkedItem.getId();
                Log.d("checkedItem", checkedItem.getId() + " --- ");
                Log.d("checkedItem", s + " --- ");
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < list.getCount(); i++) {
                    boolean checked = list.isItemChecked(i);
                    if (checked) {
                        if (sb.length() > 0) sb.append(",");
                        selectedIds = String.valueOf(sb.append(requestModelArrayList.get(i).getId()));
                    }
                }
                sendEmail();

            }
        }).setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = alt_bld.create();
        alert.show();

    }

    private void SendMessageNotification() {
        final Dialog dialog = new Dialog(HomeAdmin.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dilog_send_notification);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        final EditText text = (EditText) dialog.findViewById(R.id.edt_dia);

        text.setSelection(text.getText().length());

        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = text.getText().toString();
                if (des.isEmpty()) {
                    text.setError(getString(R.string.error_empty_field));
                    text.requestFocus();
                    return;
                }
                dialog.dismiss();
                if (Constants.Validator.isNetworkConnected(HomeAdmin.this)) {
                    sendNotification(des);

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void sendEmail() {
        final Dialog dialog = new Dialog(HomeAdmin.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_send_email);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        final EditText text = (EditText) dialog.findViewById(R.id.edt_dia);

        text.setSelection(text.getText().length());

        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = text.getText().toString();
                if (des.isEmpty()) {
                    text.setError(getString(R.string.error_empty_field));
                    text.requestFocus();
                    return;
                }
                dialog.dismiss();
                if (Constants.Validator.isNetworkConnected(HomeAdmin.this)) {
                    sendMail(des);

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void sendMail(String description) {
        try {
            description = URLEncoder.encode(description, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String finalDescription = description;
        makeRequest(true, WebServices.SendEmail, "id=" + selectedIds + "&message=" + description, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message);
                    } else {

                        showInformationDialog(message);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        });
    }

    private void sendNotification(String description) {
        showLoading();
        try {
            description = URLEncoder.encode(description, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String finalDescription = description;
        makeRequest(true, WebServices.SendNotification, "message=" + description, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message);
                    } else {
                        showInformationDialog(message);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });
    }

    public StringRequest addService(String description) {
        showLoading();
        try {
            description = URLEncoder.encode(description, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        jsonArrayRequest = new StringRequest(AddService + "s_title=" + description, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("service response", "---  " + response);
                hideLoading();
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message);
                    } else {

                        showInformationDialog(message);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoading();

                    }
                }) {
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, parseCacheHeaders(response));
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        reqDetail.add(jsonArrayRequest);
        return jsonArrayRequest;
    }

}

package com.parlour.adminparlour.classes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.adminparlour.Adapters.RequestAdapterr;
import com.parlour.adminparlour.Models.RequestModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.BaseActivity;
import com.parlour.adminparlour.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RequestList extends BaseActivity {
    RecyclerView _rvRequestList;
    RequestAdapterr requestAdapter;
    ArrayList<RequestModel> requestModelArrayList = new ArrayList<>();
    private StringRequest jsonArrayRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _rvRequestList = (RecyclerView) findViewById(R.id.catrecycleview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RequestList.this, LinearLayoutManager.VERTICAL, false);
        _rvRequestList.setLayoutManager(linearLayoutManager);

        requestAdapter = new RequestAdapterr(RequestList.this, requestModelArrayList,new RequestAdapterr.onViewMoreListner() {
            @Override
            public void onview(String catlistingmodelName, String catlistingmodelId,String image, String details,String email, String addressline1, String addressline2, String city, String landline, String phone, String listing_status, String listing_type, String start_time, String end_time, String work_days,String distance) {
                addDialog(catlistingmodelName,catlistingmodelId,image,details,email,addressline1,addressline2,city,landline,phone,listing_status,listing_type,start_time,end_time,work_days,distance);

            }
        });
        _rvRequestList.setAdapter(requestAdapter);

        if (Constants.Validator.isNetworkConnected(RequestList.this)) {
            getRequestList();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }

    public void getRequestList() {
        makeRequest(true, WebServices.RequestList, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                RequestModel requestModel = null;
                JSONArray jsonArray = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");

                    String status = jsonObject1.getString("status");
                    String message = jsonObject1.getString("message");
                    if (status.equalsIgnoreCase("valid")) {
                        requestModelArrayList.clear();

                        jsonArray = jsonObject1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject json = null;
                            try {
                                requestModel = new RequestModel();
                                json = jsonArray.getJSONObject(i);
                                requestModel.setId(json.getString("saloon_id"));
                                requestModel.setName(json.getString("Title"));
                                requestModel.setImage(json.getString("feature_Image"));
                                requestModel.setEmail(json.getString("email"));
                                requestModel.setDerc(json.getString("details"));
                                requestModel.setAddressline1(json.getString("address_line1"));
                                requestModel.setAddressline2(json.getString("address_line2"));
                                requestModel.setCity(json.getString("city"));
                                requestModel.setPhone(json.getString("phone"));
                                requestModel.setLandline(json.getString("landline"));
                                requestModel.setListing_status(json.getString("listing_status"));
                                requestModel.setListing_type(json.getString("listing_type"));
                                requestModel.setLatitude(json.getString("latitude"));
                                requestModel.setLongitude(json.getString("longitude"));
                                requestModel.setStart_time(json.getString("start_time"));
                                requestModel.setEnd_time(json.getString("end_time"));
                                requestModel.setWork_days(json.getString("work_days"));



                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                            requestModelArrayList.add(requestModel);
                        }

                    } else {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void approveReq(String id) {
        makeRequest(true, WebServices.ApproveReq, "saloon_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("valid")) {
                        showInformationDialog(jsonObject.getString("message"));
                        getRequestList();
                    } else {
                        showInformationDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
            }
        });
    }

    public void declineReq(String id) {
        makeRequest(true, WebServices.DeclineReq, "saloon_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("valid")) {
                        showInformationDialog(jsonObject.getString("message"));
                        getRequestList();
                    } else {
                        showInformationDialog(jsonObject.getString("message"));

                    }

                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
            }
        });
    }
    private void addDialog(String catlistingmodelName, final String catlistingmodelId,String image,String details, String email, String addressline1, String addressline2, String city, String landline, String phone, String listing_status, String listing_type, String start_time, String end_time, String work_days,String distance) {

        final Dialog dialog = new Dialog(RequestList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_edit);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final TextView _title = (TextView) dialog.findViewById(R.id.edt_dia);
        final TextView _details = (TextView) dialog.findViewById(R.id.details);
        final TextView _email1 = (TextView) dialog.findViewById(R.id.email);
        final TextView _phone1 = (TextView) dialog.findViewById(R.id.phone);
        final TextView _address = (TextView) dialog.findViewById(R.id.address);
        final TextView _listing_status = (TextView) dialog.findViewById(R.id.listingStatus);
        final TextView _stime = (TextView) dialog.findViewById(R.id.stime);
        final TextView _workdays = (TextView) dialog.findViewById(R.id.workDays);
        final TextView _distance = (TextView) dialog.findViewById(R.id.distance);

        final ImageView featureImage = (ImageView) dialog.findViewById(R.id.chooseImage);
        String fullUrl = WebServices.DomainNameImage +image;

        utils.setImagePicass(fullUrl,RequestList.this,featureImage);

        _title.setText(utils.capFirstChar(catlistingmodelName));
        _details.setText(details);
        _email1.setText("Email: "+email);
        _phone1.setText("Tel: "+landline+" , "+"Mob: "+phone);
        _address.setText("Address: "+addressline1+ addressline2+" , "+city);
        _stime.setText("Opening Time: "+start_time+"  Closing Time: "+end_time);
        _workdays.setText(work_days);
        _distance.setText(distance);
         if(listing_status.equals("0"))
         {
             _listing_status.setText("User Listing is pending");

         }
         else
             if(listing_status.equals("1"))

         {
             _listing_status.setText("Parlour approved");

         }
        else
            if(listing_status.equals("2"))

             {
                 _listing_status.setText("Listing Declined");

             }

        // text.setText(service_name);
        TextView yes = (TextView) dialog.findViewById(R.id.btn_yes);
        TextView no = (TextView) dialog.findViewById(R.id.btn_no);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.btn_cancel);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (Constants.Validator.isNetworkConnected(RequestList.this)) {
                    hideSoftKeyboard();
                    approveReq(catlistingmodelId);

                    //addService(service_id, service_name, des, sp, ep, ot);
                    // Toast.makeText(AddService.this, ""+des+sp+ep+"VVVV"+item, Toast.LENGTH_LONG).show();


                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (Constants.Validator.isNetworkConnected(RequestList.this)) {
                    hideSoftKeyboard();
                    declineReq(catlistingmodelId);
                    //addService(service_id, service_name, des, sp, ep, ot);
                    // Toast.makeText(AddService.this, ""+des+sp+ep+"VVVV"+item, Toast.LENGTH_LONG).show();


                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


}

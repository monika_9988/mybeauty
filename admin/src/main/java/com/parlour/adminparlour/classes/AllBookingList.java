package com.parlour.adminparlour.classes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.toolbox.StringRequest;
import com.parlour.adminparlour.Adapters.BookingAdapterr;
import com.parlour.adminparlour.Models.BookingRequestModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.BaseActivity;
import com.parlour.adminparlour.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/14/2017.
 */

public class AllBookingList extends BaseActivity {
    Context activity;
    RecyclerView rvbooking;
    ArrayList<BookingRequestModel> requestModelArrayList = new ArrayList<>();
    private BookingAdapterr requestAdapter;
    private StringRequest jsonArrayRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookinglist);
        activity = AllBookingList.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvbooking = (RecyclerView) findViewById(R.id.booking);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AllBookingList.this, LinearLayoutManager.VERTICAL, false);
        rvbooking.setLayoutManager(linearLayoutManager);

        requestAdapter = new BookingAdapterr(activity, requestModelArrayList);
        rvbooking.setAdapter(requestAdapter);

        if (Constants.Validator.isNetworkConnected(activity)) {
            getBookingList();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }

    public void getBookingList() {

        makeRequest(true, WebServices.getBookingNumber, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                JSONArray jsonArray = null;
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String message = jsonObj.getString("message");
                    String status = jsonObj.getString("status");
                    requestModelArrayList.clear();

                    if (status.equals("valid")) {
                        jsonArray = jsonObj.getJSONArray("rows");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("arrayyyyyy", "" + i);
                            BookingRequestModel bookingModel = new BookingRequestModel();
                            bookingModel.setId(jsonArray.getJSONObject(i).getString("saloon_id"));

                            bookingModel.setName(jsonArray.getJSONObject(i).getString("saloon_title"));
                            bookingModel.setTot_booking(jsonArray.getJSONObject(i).getString("total"));
                            bookingModel.setToday_booking(jsonArray.getJSONObject(i).getString("today"));
                            // bookingModel.setStatus(jsonArray.getJSONObject(i).getString("booking_status"));
                            //bookingModel.setDateTime(jsonArray.getJSONObject(i).getString("a_date") + " " + jsonArray.getJSONObject(i).getString("a_time"));
                            requestModelArrayList.add(bookingModel);
                        }
                        requestAdapter.notifyDataSetChanged();

                    } else {

                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();
                }

            }
        } );
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

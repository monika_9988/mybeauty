package com.parlour.adminparlour.classes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.adminparlour.Adapters.ApprovedAdapterr;
import com.parlour.adminparlour.Models.BookingModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.BaseActivity;
import com.parlour.adminparlour.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Lakhwinder on 8/14/2017.
 */

public class AllAproved extends BaseActivity {
    Context activity;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    RecyclerView rvbooking;
    ArrayList<BookingModel> requestModelArrayList = new ArrayList<>();
    private ApprovedAdapterr requestAdapter;
    private StringRequest jsonArrayRequest;
    private int mYear;
    private int mMonth, mDay;
    private TextView enddtae;
    private String dateToSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allapprovedist);
        activity = AllAproved.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvbooking = (RecyclerView) findViewById(R.id.approved);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AllAproved.this, LinearLayoutManager.VERTICAL, false);
        rvbooking.setLayoutManager(linearLayoutManager);

        requestAdapter = new ApprovedAdapterr(activity, requestModelArrayList, new ApprovedAdapterr.ondeleteListener() {
            @Override
            public void ondelete(final String id, final int i) {
                if (Constants.Validator.isNetworkConnected(activity)) {
                    showOptionDialog("If you disable parlour,It will never show to user", new Helper.OnChoiceListener() {
                        @Override
                        public void onChoose(boolean isPositive) {
                            if (isPositive) {
                                enableDisableSaloon(id, i);

                            } else {

                            }
                        }
                    });
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        }, new ApprovedAdapterr.onapproveListener() {
            @Override
            public void onapprove(String id, int i) {
                if (Constants.Validator.isNetworkConnected(activity)) {
                    enableDisableSaloon(id, i);
                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        }, new ApprovedAdapterr.onUpdate() {
            @Override
            public void onupd(BookingModel catlistingmodel) {
                dialogupdate(catlistingmodel);
            }
        }, new ApprovedAdapterr.onImageClick() {
            @Override
            public void onimageclick(String imgurl) {
                openImageDialog(imgurl);
            }
        });
        rvbooking.setAdapter(requestAdapter);


        if (Constants.Validator.isNetworkConnected(activity)) {
            getApprovedList();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }

    }


    private void openImageDialog(String imgurl) {
        final Dialog dialog = new Dialog(AllAproved.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_open_image);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        final ImageView image = (ImageView) dialog.findViewById(R.id.largeImage);


        ImageView cancel_action = (ImageView) dialog.findViewById(R.id.cancel);
        utils.setImagePicass(imgurl, AllAproved.this, image);
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    private StringRequest enableDisableSaloon(String id, int i) {
        String msg = "";
        if (i == 0) {
            msg = "Parlour Enabled";
        } else {
            msg = "Parlour Disabled";
        }

        final String finalMsg = msg;
        makeRequest(true, WebServices.EnableDisableparlour, "saloon_id=" + id + "&statusVal=" + i, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("valid")) {
                        showInformationDialog(finalMsg);
                        getApprovedList();
                    } else {
                        showInformationDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    hideLoading();

                    e.printStackTrace();
                }

            }
        });
        return jsonArrayRequest;
    }


    public void getApprovedList() {

        makeRequest(true, WebServices.AllAprrovedList,"", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                BookingModel requestModel = null;
                JSONArray jsonArray = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");

                    String status = jsonObject1.getString("status");
                    String message = jsonObject1.getString("message");
                    requestModelArrayList.clear();

                    if (status.equalsIgnoreCase("valid")) {
                        jsonArray = jsonObject1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject json = null;
                            try {
                                requestModel = new BookingModel();
                                json = jsonArray.getJSONObject(i);
                                requestModel.setId(json.getString("saloon_id"));
                                requestModel.setName(json.getString("Title"));
                                requestModel.setEmail(json.getString("email"));
                                requestModel.setPhone(json.getString("phone"));
                                requestModel.setLandline(json.getString("landline"));
                                requestModel.setStatus(json.getString("status"));
                                requestModel.setImage(json.getString("feature_Image"));
                                requestModel.setEndtime(json.getString("renew_date"));
                                requestModel.setPosition(json.getString("position"));

                            } catch (JSONException e) {
                                hideLoading();
                                e.printStackTrace();
                            }
                            requestModelArrayList.add(requestModel);
                        }

                    } else {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });
                    }
                } catch (JSONException e) {
                    hideLoading();
                    e.printStackTrace();
                }
                requestAdapter.notifyDataSetChanged();

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dialogupdate(final BookingModel model) {
        final Dialog dialog = new Dialog(AllAproved.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_editlistingstatus);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        final EditText text = (EditText) dialog.findViewById(R.id.edt_dia);
        enddtae = (TextView) dialog.findViewById(R.id.book_time);
        TextView name = (TextView) dialog.findViewById(R.id.txt_name);
        try {
            enddtae.setText(utils.getFormattedDate2(model.getEndtime()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        text.setText(model.getPosition());
        text.setSelection(text.getText().length());
        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        dateToSend = model.getEndtime();
        enddtae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdate(model.getEndtime());
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = text.getText().toString();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (des.isEmpty()) {
                    text.setError(getString(R.string.error_empty_field));
                    text.requestFocus();
                    return;
                }
                dialog.dismiss();
                if (Constants.Validator.isNetworkConnected(AllAproved.this)) {
                    updatemethod(des,model.getId());

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
    private void updatemethod(String description, final String id) {

        makeRequest(true, WebServices.UPdate, "saloon_id=" + id + "&renew_date=" + dateToSend + "&position=" + description, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getApprovedList();
                            }
                        });
                    } else {

                        showInformationDialog(message);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });

    }

    public void getdate(final String endtime) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        long selectedtime = calendar.getTimeInMillis();

//                        if (utils.compareDates(selectedtime, end)) {
//                            showInformationDialog("Selected date must be greater than End-Date");
//                        } else {
                            enddtae.setText(dayOfMonth + "-" + MONTHS[(monthOfYear)] + " " + year);
                            dateToSend = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
//                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

}

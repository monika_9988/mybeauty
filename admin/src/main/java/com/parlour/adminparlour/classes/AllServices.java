package com.parlour.adminparlour.classes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.toolbox.StringRequest;
import com.parlour.adminparlour.Adapters.ServiceAdapterr;
import com.parlour.adminparlour.Models.ServiceModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.BaseActivity;
import com.parlour.adminparlour.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/14/2017.
 */

public class AllServices extends BaseActivity {
    RecyclerView _rvservice;
    private ServiceAdapterr serviceAdapter;
    ArrayList<ServiceModel> serviceModelArrayList = new ArrayList<>();
    private StringRequest jsonArrayRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allservices);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _rvservice = (RecyclerView) findViewById(R.id.allservices);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AllServices.this, LinearLayoutManager.VERTICAL, false);
        _rvservice.setLayoutManager(linearLayoutManager);

        serviceAdapter = new ServiceAdapterr(AllServices.this, serviceModelArrayList, new ServiceAdapterr.ondeleteListener() {
            @Override
            public void ondelete(final String sid) {
                showOptionDialog("Do you really want to Delete ?", new Helper.OnChoiceListener() {
                    @Override
                    public void onChoose(boolean isPositive) {
                        if (isPositive) {
                            if (Constants.Validator.isNetworkConnected(AllServices.this)) {
                                declineReq(sid);

                            } else {
                                showInformationDialog(getString(R.string.checkinternet));
                            }
                        } else {

                        }
                    }
                });


            }
        }, new ServiceAdapterr.onapproveListener() {
            @Override
            public void onapprove(String name, String id) {
                openEditDialog(name, id);
            }
        });
        _rvservice.setAdapter(serviceAdapter);

        if (Constants.Validator.isNetworkConnected(AllServices.this)) {
            getservicelist();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }

    }



    private void openEditDialog(final String name, final String id) {

        final Dialog dialog = new Dialog(AllServices.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_service);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        final EditText text = (EditText) dialog.findViewById(R.id.edt_dia);
        text.setText(name);
        text.setSelection(text.getText().length());

        Button yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button no = (Button) dialog.findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = text.getText().toString();
                if (des.isEmpty()) {
                    text.setError(getString(R.string.error_empty_field));
                    text.requestFocus();
                    return;
                }
                dialog.dismiss();
                if (Constants.Validator.isNetworkConnected(AllServices.this)) {
                    updateServices(des, id);

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    public void getservicelist() {

        makeRequest(true, WebServices.GetServices, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                ServiceModel requestModel = null;
                JSONArray jsonArray = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");

                    String status = jsonObject1.getString("status");
                    String message = jsonObject1.getString("message");
                    if (status.equalsIgnoreCase("valid")) {
                        serviceModelArrayList.clear();
                        jsonArray = jsonObject1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject json = null;
                            try {
                                requestModel = new ServiceModel();
                                json = jsonArray.getJSONObject(i);
                                requestModel.setId(json.getString("service_id"));
                                requestModel.setName(json.getString("s_title"));


                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                            serviceModelArrayList.add(requestModel);
                        }
                        serviceAdapter.notifyDataSetChanged();

                    } else {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateServices(String trim, String id) {
        try {
            trim = URLEncoder.encode(trim, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        showLoading();
        makeRequest(true, WebServices.UPdateservice, "service_id=" + id + "&s_title=" + trim, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("valid")) {
                        showInformationDialog(jsonObject.getString("message"), new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                getservicelist();
                            }
                        });
                    } else {
                        showInformationDialog(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void declineReq(String id) {
        makeRequest(true, WebServices.DeleteServices, "service_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (status.equalsIgnoreCase("valid")) {
                        showInformationDialog(message);
                        getservicelist();
                    } else {

                        showInformationDialog(message);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideLoading();

                }
            }
        });
    }

}

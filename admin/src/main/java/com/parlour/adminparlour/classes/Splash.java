package com.parlour.adminparlour.classes;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import com.parlour.adminparlour.R;
import com.parlour.adminparlour.generic.BaseActivity;

public class Splash extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        final ProgressBar mProgress = (ProgressBar) findViewById(R.id.splash_screen_progress_bar);
        mProgress.showContextMenu();
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                mProgress.destroyDrawingCache();
                // This method will be executed once the timer is over
                // Start your app main activity

                if (user.isLoggedIn()){
                    Intent i = new Intent(Splash.this, HomeAdmin.class);
                    startActivity(i);
                    finish();

                }else{
                    Intent i = new Intent(Splash.this, Login_register.class);
                    startActivity(i);
                    finish();

                }


                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
package com.parlour.adminparlour.classes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.parlour.adminparlour.Adapters.RegisteredAdapterr;
import com.parlour.adminparlour.Models.registeredUserModel;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.BaseActivity;
import com.parlour.adminparlour.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lakhwinder on 8/14/2017.
 */

public class AllRegUsers extends BaseActivity {
    Context activity;
RecyclerView rvbooking;
    ArrayList<registeredUserModel> requestModelArrayList = new ArrayList<>();
    private RegisteredAdapterr requestAdapter;
    private StringRequest jsonArrayRequest;
    TextView _total;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reglist);
        activity = AllRegUsers.this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        _total = (TextView) findViewById(R.id.totalUsers);

        rvbooking = (RecyclerView) findViewById(R.id.reg);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AllRegUsers.this, LinearLayoutManager.VERTICAL, false);
        rvbooking.setLayoutManager(linearLayoutManager);
        requestAdapter = new RegisteredAdapterr(activity, requestModelArrayList);

        rvbooking.setAdapter(requestAdapter);

        if (Constants.Validator.isNetworkConnected(activity)) {
            getRegisteredList();

        } else {
            showInformationDialog(getString(R.string.checkinternet));
        }
    }


    public void getRegisteredList() {

        makeRequest(true, WebServices.AllREgisteredUsers, "", new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                registeredUserModel requestModel = null;
                JSONArray jsonArray = null;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");

                    String status = jsonObject1.getString("status");
                    String message = jsonObject1.getString("message");
                    String total = jsonObject1.getString("total");
                    if (status.equalsIgnoreCase("valid")) {
                        jsonArray = jsonObject1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject json = null;
                            try {
                                requestModel = new registeredUserModel();
                                json = jsonArray.getJSONObject(i);
                                requestModel.setId(json.getString("id"));
                                requestModel.setName(json.getString("name"));
                                requestModel.setEmail(json.getString("email"));
                                requestModel.setPhone(json.getString("phone"));
                                requestModel.setDerc(json.getString("profession"));


                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                            _total.setText("Total Registered Users : "+total);
                            requestModelArrayList.add(requestModel);
                        }
                          requestAdapter.notifyDataSetChanged();
                    } else {
                        showInformationDialog(message, new Helper.OnButtonClickListener() {
                            @Override
                            public void onClick() {
                                finish();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

package com.parlour.adminparlour.classes;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.parlour.adminparlour.R;
import com.parlour.adminparlour.config.Constants;
import com.parlour.adminparlour.config.WebServices;
import com.parlour.adminparlour.generic.BaseActivity;
import com.parlour.adminparlour.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Login_register extends BaseActivity {
    Button _signin;
    private CheckBox _ch_rememberme;
    private EditText _email;
    private EditText _password;
    private String emailStr;
    private String passwordStr;
    private StringRequest jsonArrayRequest;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        linearLayout = (LinearLayout) findViewById(R.id.ll);
        _ch_rememberme = (CheckBox) findViewById(R.id.login_rememberme);
        _email = (EditText) findViewById(R.id.emailLogin);
        _password = (EditText) findViewById(R.id.passLogin);
        _signin = (Button) findViewById(R.id.signin);
        String userEmail = user.getString(Constants.Email);
        String userPass = user.getString(Constants.Password);
        boolean check = user.getBoolean(Constants.Ischecked, false);

        setupUI(linearLayout);

        if (check) {
            _ch_rememberme.setChecked(true);
            _email.setText(userEmail);
//            _password.setText(userPass);
            _email.setSelection(_email.getText().length());
        }

        _signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailStr = _email.getText().toString().trim();
                passwordStr = _password.getText().toString();
                if (emailStr.isEmpty()) {
                    _email.setError(getString(R.string.error_empty_field));
                    _email.requestFocus();
                    _password.clearFocus();

                    return;
                }
                if (!Constants.Validator.isValidEmail(emailStr)) {
                    _email.setError("Please enter a valid email.");
                    _email.requestFocus();
                    _password.clearFocus();
                    return;
                }
                if (passwordStr.isEmpty()) {
                    _password.setError(getString(R.string.error_empty_field));
                    _password.requestFocus();
                    _email.clearFocus();
                    return;
                }
                if (Constants.Validator.isNetworkConnected(Login_register.this)) {
                    hideSoftKeyboard();
                    login();
                    //startActivity(new Intent(Login_register.this, HomeAdmin.class));

                } else {
                    showInformationDialog(getString(R.string.checkinternet));
                }
            }
        });
        _ch_rememberme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                      @Override
                                                      public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                                                          String email = _email.getText().toString().trim();
                                                          String pass = _password.getText().toString().trim();
                                                          if (isChecked) {
                                                              user.setBoolean(Constants.Ischecked, true);
                                                              user.setString(Constants.Email, email);
//                                                              user.setString(Constants.Password, pass);
                                                          } else {
                                                              user.setBoolean(Constants.Ischecked, false);
                                                              user.setString(Constants.Email, "");
//                                                              user.setString(Constants.Password, "");
                                                              _email.setText("");
                                                              _password.setText("");
                                                          }
                                                      }

                                                  }

        );
    }

    public void login() {

        makeRequest(true,WebServices.LoginAdmin, "email=" + emailStr + "&password=" + passwordStr, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("result");
                    if (jsonObject1.getString("status").equalsIgnoreCase("valid")) {
                        JSONArray jsonArray = jsonObject1.getJSONArray("resarray");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            user.setUserId(jsonArray.getJSONObject(i).getString("id"));
                            user.setUserType(jsonArray.getJSONObject(i).getString("type"));
                            user.setName(jsonArray.getJSONObject(i).getString("name"));
                            user.loggedIn(true);
                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                            savedetails(refreshedToken, user.getUserId());
                            showInformationDialog(jsonObject1.getString("message"), new Helper.OnButtonClickListener() {
                                @Override
                                public void onClick() {
                                    Intent intent = new Intent(Login_register.this, HomeAdmin.class);
                                    startActivity(intent);
                                    finish();


                                }

                            });

                        }
                    } else {
                        showInformationDialog(jsonObject1.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void savedetails(final String token, String id) {

        String android_id = Settings.Secure.getString(Login_register.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        makeRequest(false, WebServices.DeviceRegister, "device_token=" + token + "&id=" + android_id+ "&admin_id=" + id, new onRequestComplete() {
            @Override
            public void onComplete(String response) {
                Log.d("MyFirebaseIIDService response", "---  " + response);
                Log.d("MyFirebaseIIDService response   id ", "---  " + user.getUserId());
                Log.d("MyFirebaseIIDService response   token ", "---  " + token);
            }
        });
    }


}

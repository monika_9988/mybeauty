package com.parlour.adminparlour.utils;

import android.support.v4.BuildConfig;
import android.util.Log;

/**
 * Created by Gurvinder rajpal on 09-12-16.
 */
public class Logger {
    public static void logResponse(String responseCode, Object response, String requestDump, String responseDump) {
        LogValueE("newSoapRequest", "responseCode " + responseCode);
        LogValueE("newSoapRequest", "response " + response);
        LogValueE("newSoapRequest", "requestDump " + requestDump);
        LogValueE("newSoapRequest", "responseDump " + responseDump);
    }

    private static void LogValueE(String name, String s) {
        if (BuildConfig.DEBUG) {
            Log.e(name, s);
        }
    }

    public static void logE(String s) {
        LogValueE("mycapital", "log: " + s);
    }
}

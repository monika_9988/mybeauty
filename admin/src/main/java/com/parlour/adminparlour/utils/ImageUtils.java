package com.parlour.adminparlour.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.IOException;

/**
 * Created by Gurvinder rajpal on 07-10-16.
 */

public class ImageUtils {
    /**
     * Create bitmap from given path and compress image according to device.
     *
     * @param path path of the image
     * @return bitmap
     */
    public static Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 320;

            // Find the correct scale value. It should be the power of
            // 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE
                    && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bmp = rotateImage(path, BitmapFactory.decodeFile(path, o2));
            return bmp;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }
    /**
     * Rotate the image if needed based on information provided in exif Header
     *
     * @param pathToImage  image path
     * @param sourceBitmap bitmap
     * @return rotated image
     */
    private static Bitmap rotateImage(String pathToImage, Bitmap sourceBitmap) {

        // 1. figure out the amount of degrees
        int rotation = 0;
        try {
            rotation = getImageRotation(pathToImage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 2. rotate matrix by postconcatination
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);

        // 3. create Bitmap from rotated matrix
        // Bitmap sourceBitmap = BitmapFactory.decodeFile(pathToImage);
        return Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(),
                sourceBitmap.getHeight(), matrix, true);
    }

    private static int getImageRotation(String photoPath) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;

            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;

        }
        return 0;
    }

}

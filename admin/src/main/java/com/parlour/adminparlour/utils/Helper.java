package com.parlour.adminparlour.utils;


public class Helper {

    public interface OnChoiceListener {
        void onChoose(boolean isPositive);
    }

    public interface OnItemChooseListener {
        void onChoose(Object item);
    }

    public interface OnButtonClickListener {
        void onClick();
    }

    public interface OnRequestCompleteListener<T> {
        void onComplete(T object);
    }

}

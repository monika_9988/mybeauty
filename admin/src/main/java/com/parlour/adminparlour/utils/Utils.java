package com.parlour.adminparlour.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import com.parlour.adminparlour.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {
    private static Utils mInstance;
    private static Context mContext;
    private boolean twitterLoggedInAlready;
    private boolean shareOnTwitter;


    public static Utils getInstance() {
        if (mInstance == null) {
            mInstance = new Utils();
        } else {
            return mInstance;
        }
        return mInstance;
    }

    public int dpToPx(int dp, Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public void setImagePicass(String url, Context context, ImageView image) {
        if (url.isEmpty()) {
        } else {
            Picasso.with(context)
                    .load(url)
                    .resize(dpToPx(180, context), dpToPx(180, context))
                    .networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE)
                    .placeholder(android.R.drawable.gallery_thumb).centerCrop()
                    .error(android.R.drawable.gallery_thumb).into(image);
        }
    }
    public boolean compareDates(long selecteddate, String endtime) {
        Calendar c = Calendar.getInstance();
        long currenttime = c.getTimeInMillis();
        long etime= Long.parseLong(endtime);
        if (selecteddate > etime) {
            return true;
        } else {
            return false;
        }
    }
    public static String upperFirstChar(String s) {

        String upperString = "";
        if (s != null && s.length() > 0) {
            upperString = s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return upperString;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

    public static void init(Context mContext) {
        Utils.mContext = mContext;
    }

    private static double ConvertMetersToKm(double m) {
        return (m / 1000);
    }

    private static double ConvertMetersToMiles(double meters) {
        return (meters / 1609.344);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @return A float value to represent dp equivalent to px value
     */
    public float convertPixelsToDp(float px) {
        Resources resources = mContext.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public float convertDpToPixel(float dp) {
        Resources resources = mContext.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

//    public long getMillisFromString(String dateStr, String format) {h*9
//        //yyyy-MM-dd HH:mm:ss
//        SimpleDateFormat df = new SimpleDateFormat(format);
//        Date date = null;
//        try {
//            date = df.parse(dateStr);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return date.getTime();
//    }

    /**
     * Check the service is currently running or not on android system.
     *
     * @param serviceClass class name
     * @return true/false
     */
    public boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    //millis
    public long getMillisFromUTCString(String dateStr, String format, boolean convertToLocal) {
        //yyyy-MM-dd HH:mm:ss     0000-00-00 00:00:00
        SimpleDateFormat df = new SimpleDateFormat(format);
        if (convertToLocal) df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date == null) {
            return 00000;
        }
        return date.getTime();
    }

    public static long getTimeInMillisFromString(String dateTime, String format) {

        return getTimeInMillisFromString(dateTime, format, false);
    }

    public static long getTimeInMillisFromString(String dateTime, String format, boolean convertToLocal) {
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.ENGLISH);

        if (convertToLocal) {
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        Date date = null;
        try {
            date = df.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            date = new Date();
        }
        return date.getTime();

    }

    public String convertLocalTimeToUTC(String dateStr, String inputFormat) {
        long localTime = getMillisFromUTCString(dateStr, inputFormat, false);
        SimpleDateFormat sdf = new SimpleDateFormat(inputFormat);
        // Convert Local Time to UTC (Works Fine)
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(localTime);
    }

    public String convertLocalTimeToUTC(long millis, String outputFormat) {

        SimpleDateFormat sdf = new SimpleDateFormat(outputFormat);
        // Convert Local Time to UTC (Works Fine)
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(millis);
    }


    public static double cmToInch(double cm) {
        return cm * 0.393701;
    }

    public static double inchToCm(double inch) {
        return inch * 2.54;
    }

    public static double kmToMiles(double km) {
        return km * 0.621371;
    }

    public static double milesToKm(double miles) {
        return miles * 1.60934;
    }

    public static double kgToLbs(double kg) {
        return kg * 2.20462;
    }

    public static double lbsToKg(double lbs) {
        return lbs * 0.453592;
    }

    public String getFormattedString(long time, String outputFormat) {
        SimpleDateFormat df = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        return df.format(time);
    }

    public String getFormattedString(long time, String outputFormat, String timeZone) {
        SimpleDateFormat df = new SimpleDateFormat(outputFormat);
        if (timeZone != null) {
            df.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        return df.format(time);
    }

    public String getFormattedDate(String dateStr) {
        //"2017-01-05 03:50:23
        //0000-00-00 00:00:00
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        //  df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //MM/DD/YY
        SimpleDateFormat dayf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ENGLISH);
//        SimpleDateFormat df1 = new SimpleDateFormat("dd'" + getDayNumberSuffix(Integer.parseInt(dayf.format(date))) + "' MMM, yyyy",Locale.ENGLISH);
        dayf.setTimeZone(TimeZone.getDefault());
        String formattedDate = dayf.format(date);
        return formattedDate;
    }

    public String getFormattedDate2(String dateStr) {
        //"2017-01-05 03:50:23
        //0000-00-00 00:00:00
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        //  df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //MM/DD/YY
        SimpleDateFormat dayf = new SimpleDateFormat("dd-MMM-yyyy ", Locale.ENGLISH);
//        SimpleDateFormat df1 = new SimpleDateFormat("dd'" + getDayNumberSuffix(Integer.parseInt(dayf.format(date))) + "' MMM, yyyy",Locale.ENGLISH);
        dayf.setTimeZone(TimeZone.getDefault());
        String formattedDate = dayf.format(date);
        return formattedDate;
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public String getRemainingTime(String time, boolean b) {
        // it comes out like this 2013-08-31 15:55:22 so adjust the date format
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (b) {
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        Date date = null;
        long epoch = 0;
        try {
            date = df.parse(time);
            epoch = date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        CharSequence timePassedString = DateUtils.getRelativeTimeSpanString(epoch, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        return timePassedString.toString();
    }

    public String getRemainingTime_format(long time, boolean b) {
        // it comes out like this 2013-08-31 15:55:22 so adjust the date format
        SimpleDateFormat df1 = new SimpleDateFormat("MMM dd,yyyy");
        df1.setTimeZone(TimeZone.getDefault());
        String formattedDate = df1.format(time);
        return formattedDate;
    }

    public String getRemainingTime(long time, boolean b) {
        // it comes out like this 2013-08-31 15:55:22 so adjust the date format
        long epoch = 0;
        epoch = time;
        if (epoch > System.currentTimeMillis()) {
            epoch = System.currentTimeMillis() - 2000;
        }
        CharSequence timePassedString = DateUtils.getRelativeTimeSpanString(epoch, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        return timePassedString.toString();
    }

    public String formatChatTime(long time) {
        // it comes out like this 2013-08-31 15:55:22 so adjust the date format

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(time);
    }

    public String getPastTimeString(long time) {
        long mMillies = time / 1000;
        long seconds = mMillies % 60;
        long minutes = (mMillies / 60) % 60;
        long hours = (mMillies / (60 * 60)) % 24;
        long days = (mMillies / (60 * 60 * 24)) % 7;

        String commenttimeago;
        if (minutes > 0) {
            if (hours > 0) {
                if (days > 0) {
                    if (days > 1)
                        commenttimeago = days + " days ago";
                    else commenttimeago = days + " day ago";
                } else {
                    if (hours > 1)
                        commenttimeago = hours + " hours ago";
                    else commenttimeago = hours + " hour ago";
                }
            } else {
                if (minutes > 1) {
                    commenttimeago = minutes + " minutes ago";
                } else {
                    commenttimeago = minutes + " minute ago";
                }
            }
        } else {
//            commenttimeago = seconds + " secs ago";
            commenttimeago = "0 minute ago";
        }

        return commenttimeago;
    }

    /**
     * Calculates distance between two location points.
     *
     * @param lat1       latitude of first point
     * @param lng1       longitude of first point
     * @param lat2       latitude of second point
     * @param lng2       longitude of second point
     * @param unitOutput required output in miles(unitOutput=mile),meters(unitOutput=m) and km(unitOutput=km) use @see hangar.nightway.utils.Utils.Unit com.{@CO Double}
     * @return give distance between given points in {@link Double}
     */
    public double getdistFrom(double lat1, double lng1, double lat2, double lng2, String unitOutput) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);
        double miles = dist * 0.00062137119;
        if (unitOutput.equalsIgnoreCase("mile")) {
            return Double.valueOf(String.format("%.2f", miles));
        }
        if (unitOutput.equalsIgnoreCase("m")) {
            return Double.valueOf(String.format("%.2f", dist));
        }
        if (unitOutput.equalsIgnoreCase("km")) {
            return Double.valueOf(String.format("%.2f", ConvertMetersToKm(dist)));
        }
        return miles;
    }

    public String capFirstChar(String s) {
        if (s != null && s.length() > 0) {
            return s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return "";

    }


    public String getCurrencySymbol(String countryCode) throws JSONException {
        String symbol = "";
//        String symbol = Constants.euro;
//        String currencyCodeJson = getJsonFromFile(Constants.CURRENCY_CODE_FILE);
//        String currencyDetailsJson = getJsonFromFile(Constants.CURRENCY_DETAIL_FILE);
//        if (!countryCode.equalsIgnoreCase(Constants.COUNTRY_INDIA)) {
//            JSONObject jsonObject = new JSONObject(currencyCodeJson);
//            String currencyCode = jsonObject.getString(countryCode);
//            JSONObject jObject = new JSONObject(currencyDetailsJson);
//            symbol = jObject.getJSONObject(currencyCode).getString("symbol_native");
//        } else {
//            symbol = Constants.CURRENCY_CODE_INDIA;
//        }
        return symbol;
    }

    public String getJsonFromFile(String fileName) {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

//    public String saveTempProfileImageLocation(File compressedImageFile, String type) {
//        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + BuildConfig.APPLICATION_ID + "/files/", System.currentTimeMillis() + "_image." + type);
//        moveFile(compressedImageFile.getAbsolutePath(), file.getAbsolutePath());
//        return file.getAbsolutePath();
//    }

    private void moveFile(String inputPath, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.getParentFile().mkdirs();
            }


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath).delete();


        } catch (FileNotFoundException fnfe1) {
            Log.e("compressedImageFile", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("compressedImageFile", e.getMessage());
        }

    }

    public String getYouTubeIdFromUrl(String videoUrl) {
        try {
            return videoUrl.substring(videoUrl.lastIndexOf("/") + 1, videoUrl.length());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String addPostFix(String str, String count) {
        int countInt = 0;
        try {
            countInt = Integer.valueOf(count);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return addPostFix(str, countInt);
    }

    public static String addPostFix(String str, int count) {
        if (count > 1) {
            return String.format(Locale.ENGLISH, "%ss", str);
        } else
            return str;
    }

    public static String getYouTubeId(String url) {

        if (url != null) {
            String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

            Pattern compiledPattern = Pattern.compile(pattern);
            Matcher matcher = compiledPattern.matcher(url);

            if (matcher.find()) {
                return matcher.group();
            }
        }
        return null;
    }


    public static void SaveImage(Bitmap finalBitmap, OnImageSavedListener imageSavedListener) {

        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/" + mContext.getResources().getString(R.string.app_name));
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        long time = System.currentTimeMillis();
        String fname = "Image-" + time + ".jpg";
        File file = new File(myDir, fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (imageSavedListener != null) {
            imageSavedListener.onSave(file);
        }

    }

    public static long getCurrentDateMillis() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public interface OnImageSavedListener {
        void onSave(File path);
    }
}
